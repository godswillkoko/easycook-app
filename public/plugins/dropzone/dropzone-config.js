var photo_counter = 0;
Dropzone.options.realDropzone = {

    uploadMultiple: false,
    parallelUploads: 100,
    maxFilesize: 5,
    previewsContainer: '#dropzonePreview',
    previewTemplate: document.querySelector('#preview-template').innerHTML,
    addRemoveLinks: true,
    dictRemoveFile: 'Remove',
    dictFileTooBig: 'Image is bigger than 5MB',
    maxFiles: 15,
    dictMaxFilesExceeded: 'Only a total of 15 images are allowed!',
    acceptedFiles: '.jpeg,.bmp,.png,.jpg,.gif',
    // Prevents Dropzone from uploading dropped files immediately
    autoProcessQueue: false,

    // The setting up of the dropzone
    init:function() {

        var submitButton = document.querySelector("#submit-all")
            realDropzone = this; // closure

        submitButton.addEventListener("click", function() {
            // e.preventDefault();
            // e.stopPropagation();
            realDropzone.processQueue(); // Tell Dropzone to process all queued files.
        });

        // $.get('/sao/properties/images/2/edit', function(data) {

        //     $.each(data, function(key,file){
                 
        //         var mockFile = { name: value.name, size: value.size };
                 
        //         realDropzone.options.addedfile.call(realDropzone, mockFile);
 
        //         realDropzone.options.thumbnail.call(realDropzone, mockFile, "/property-images/full_size/"+value.name);                     
        //     });             
        // });

        this.on("removedfile", function(file) {

            $.ajax({
                type: 'POST',
                url: '/sao/property/upload/delete',
                data: {id: file.name},
                dataType: 'html',
                success: function(data){
                    var rep = JSON.parse(data);
                    if(rep.code == 200)
                    {
                        photo_counter--;
                        $("#photoCounter").text( "(" + photo_counter + ")");
                    }
                },
            });
        });
    },
    error: function(file, response) {
        if($.type(response) === "string")
            var message = response; //dropzone sends it's own error messages in string
        else
            var message = response.message;
        file.previewElement.classList.add("dz-error");
        _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i];
            _results.push(node.textContent = message);
        }
        return _results;
    },
    success: function(file,done) {
        photo_counter++;
        $("#photoCounter").text( "(" + photo_counter + ")");
    }
}