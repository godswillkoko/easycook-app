<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
	use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
        'user_id',
        'order_id',
        'total',
        'comment',
        'payment_method_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Payments belongsTo user
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Payment belongsTo order
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Payment belongsTo payment method
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function payment_method()
    {
        return $this->belongsTo(PaymentMethod::class);
    }
}
