<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{
    use SoftDeletes;
    
	/**
     * Listen for attributes events
     *
     * @return void
     * @author G-Factor
     **/
    public static function boot()
    {
        parent::boot();

        static::creating(function($attribute) {
            $attribute->slug = str_slug($attribute->name);
        });

        static::updating(function($attribute) {
            $attribute->slug = str_slug($attribute->name);
        });
    }

	/**
     * Get route key for this model
     *
     * @return void
     * @author G-Facotr
     **/
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Products belongsToMany products
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
