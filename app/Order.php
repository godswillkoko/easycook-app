<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use Notifiable;
    use SoftDeletes;

    /**
     * Listen for orders events
     *
     * @return void
     * @author G-Factor
     **/
    public static function boot()
    {
        parent::boot();
        static::creating(function($order) {
            $order->invoice_no = uniqid();
        });
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'user_id',
		'invoice_no',
        'comments', //Customer comment
        'date_scheduled',
        'delivery_status',
        'total',
        'note', //Admin comment
		'user_address_id',
		'payment_method_id',
		'delivery_method_id',
        'delivery_time_slot_id',
		'payment_status',
		'order_status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     * @author G-Factor
     */
    protected $dates = [
        'date_scheduled',
        'deleted_at'
    ];


    /**
     * Orders belongsTo User
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Orders belongsTo user address
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function user_address()
    {
        return $this->belongsTo(UserAddress::class);
    }

    /**
     * Orders belongsTo payment method
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function payment_method()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    /**
     * Orders belongsTo delivery method
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function delivery_method()
    {
        return $this->belongsTo(DeliveryMethod::class);
    }

    /**
     * Orders belongsTo delivery time slot
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function delivery_time_slot()
    {
        return $this->belongsTo(DeliveryTimeSlot::class);
    }

    /**
     * Orders hasMany order_recipes
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * @author G-Factor
     */
    public function order_recipes()
    {
        return $this->hasMany(OrderRecipe::class);
    }

    /**
     * Orders belongsToMany Products
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function recipes()
    {
        return $this->belongsToMany(Recipe::class)->withPivot('quantity');
    }
}