<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Extra extends Model
{
    use SoftDeletes;
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
        'recipe_id',
        'extra_name',
        'extra_display_name',
        'extra_price'
    ];

    /**
     * Hidden attributes
     *
     * @return void
     * @author 
     **/
    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Extra belongsTo recipe
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function recipe()
    {
        return $this->belongsTo(Recipe::class);
    }

    /**
     * Cart belongsToMany recipe_extras
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function cart_extras()
    {
        return $this->belongsToMany(Extra::class);
    }
}
