<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderRecipe extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'recipe_id',
        'order_id',
        'size_id',
        'recipe_quantity'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * OrderRecipe belongsTo order
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * OrderRecipe belongsTo recipe
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function recipe()
    {
        return $this->belongsTo(Recipe::class);
    }

    /**
     * OrderRecipe belongsTo size
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function size()
    {
        return $this->belongsTo(Size::class);
    }
}
