<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'country_id',
        'code',
        'status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * State belongsTo country
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * State hasMany user address
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * @author G-Factor
     */
    public function user_addresses()
    {
        return $this->hasMany(UserAddress::class);
    }
}
