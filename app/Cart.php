<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
        'user_id',
        'recipe_id',
        'quantity',
        'size_id',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id'
    ];

    /**
     * Cart belongsTo User
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Cart belongsTo Recipe
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function recipe()
    {
        return $this->belongsTo(Recipe::class);
    }

    /**
     * Cart belongsTo Size
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    /**
     * Cart belongsToMany extras
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function cart_extras()
    {
        return $this->belongsToMany(Extra::class);
    }
}
