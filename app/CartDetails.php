<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartDetails extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'user_id',
        'comments',
        'date_scheduled',
		'user_address_id',
		'payment_method_id',
		'delivery_method_id',
        'delivery_time_slot_id'
    ];

    public $visible = [
        'id',
    	'comments',
        'date_scheduled',
		'user_address_id',
		'payment_method_id',
		'delivery_method_id',
        'delivery_time_slot_id'
    ];

    /**
     * Orders belongsTo User
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Orders belongsTo user address
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function user_address()
    {
        return $this->belongsTo(UserAddress::class);
    }

    /**
     * Orders belongsTo payment method
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function payment_method()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    /**
     * Orders belongsTo delivery method
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function delivery_method()
    {
        return $this->belongsTo(DeliveryMethod::class);
    }

    /**
     * Orders belongsTo delivery time slot
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function delivery_time_slot()
    {
        return $this->belongsTo(DeliveryTimeslot::class);
    }
}
