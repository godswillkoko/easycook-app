<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    /**
     * Boot the model.
     *
     * @return void
     * @author G-Factor
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($group) {
            $group->name = str_slug(strtolower($group->label));
        });
    }
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
	protected $fillable = [
		'name',
		'label',
		'minimum_amount_required',
		'theme_color',
		'description'
	];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     * @author G-Factor
     */
    public function getRouteKeyName()
    {
        return 'name';
    }
    
    /**
     * Group hasMany users
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * @author G-Factor
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
