<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    /**
     * Boot the model.
     *
     * @return void
     * @author G-Factor
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->name = str_slug(strtolower($user->label));
        });
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     * @author G-Factor
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
	protected $fillable = [ 'name', 'label' ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];
    
    /**
     * Each member belongsToMany a role
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Each member belongsToMany a role
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
