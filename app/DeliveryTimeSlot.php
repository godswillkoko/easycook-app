<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryTimeSlot extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'period',
        'from',
        'to',
        'day',
        'status'
    ];

    public $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Delivery timeslot hasMany orders
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * @author G-Factor
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

}
