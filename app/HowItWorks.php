<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HowItWorks extends Model
{
    use SoftDeletes;

    protected $table = 'how_it_works';
    /**
     * Listen for how it works events
     *
     * @return void
     * @author G-Factor
     **/
    public static function boot()
    {
        parent::boot();

        static::creating(function($hiw) {
            $hiw->slug = str_slug($hiw->title);
        });

        static::updating(function($hiw) {

            $hiw->slug = str_slug($hiw->title);
        });
    }

    /**
     * Get the route key for this model
     *
     * @return void
     * @author G-Factor
     **/
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'slug',
    	'title',
		'text',
        'status',
		'image_url'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];
}
