<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryMethod extends Model
{
    use SoftDeletes;

    /**
     * Listen for delivery method events
     *
     * @return void
     * @author G-Factor
     **/
    public static function boot()
    {
        parent::boot();

        static::creating(function($delivery_methods) {
            $delivery_methods->name = str_slug($delivery_methods->label);
        });

        static::updating(function($delivery_methods) {

            $delivery_methods->name = str_slug($delivery_methods->label);
        });
    }

    /**
     * Get the route key for this model
     *
     * @return void
     * @author G-Factor
     **/
    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'name',
		'label',
        'delivery_fee',
        'description',
		'status'
    ];

    public $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    // public function setDeliveryFeeAttribute($value){
    //     $this->attributes['delivery_fee'] = (int) $value;
    // }

    /**
     * Shipping method hasOne orders
     *
     * @return Illuminate\Database\Eloquent\Relations\hasOne
     * @author G-Factor
     */
    public function order()
    {
        return $this->hasOne(Order::class);
    }

}
