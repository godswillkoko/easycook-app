<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipe extends Model
{
    use SoftDeletes;

    /**
     * Listen for recipes events
     *
     * @return void
     * @author G-Factor
     **/
    public static function boot()
    {
        parent::boot();

        static::creating(function($recipe) {
            $recipe->slug = str_slug($recipe->name);
            $recipe->code = uniqid();
        });

        static::updating(function($recipe) {

            $recipe->slug = str_slug($recipe->name);
        });
    }

    /**
     * Get route key for this model
     *
     * @return void
     * @author G-Facotr
     **/
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Hidden attributes
     *
     * @return void
     * @author 
     **/
    protected $hidden = [
        'cost',
        'quantity',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
        'name',
        'slug',
        'category_id',
        'code',
        'stock_status',
        'cost',
        'old_price',
        'price',
        'weight',
        'delivery_fee',
        'quantity',
        'minimum_order',
        'require_shipping',
        'description',
        'chef',
        'color',
        'tips',
        'status',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    public function setOldPriceAttribute($value){
        $this->attributes['old_price'] = (int) $value;
    }

    public function setWeightAttribute($value){
        $this->attributes['weight'] = (int) $value;
    }

    /**
     * Recipes belongsToMany ingredients
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class);
    }

    /**
     * Recipes belongsToMany attributes
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }

    /**
     * Recipes belongsToMany orders
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    /**
     * Recipes belongsToMany carts
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function carts()
    {
        return $this->belongsToMany(Cart::class);
    }

    /**
     * Recipes hasMany sizes
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * @author G-Factor
     */
    public function sizes()
    {
        return $this->hasMany(Size::class);
    }

    /**
     * Recipe hasMany extras
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * @author G-Factor
     */
    public function extras()
    {
        return $this->hasMany(Extra::class);
    }

    /**
     * Recipe belongsTo category
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
