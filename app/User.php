<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * Boot the model.
     *
     * @return void
     * @author G-Factor
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->username = strtolower($user->name . uniqid());
            $user->token = str_random(30);
            $user->group_id = 1;
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'gender',
        'phone',
        'about_me',
        'newsletter',
        'verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Each member belongsToMany a role
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Each member belongsTo a group
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * Each member hasMany orders
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * @author G-Factor
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * Each member hasMany user addresses
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * @author G-Factor
     */
    public function user_addresses()
    {
        return $this->hasMany(UserAddress::class);
    }

    /**
     * Check if user contains role
     *
     * @return Response
     * @author G-Factor
     */
    public function hasRole($role) {

        if(is_string($role))
        {
            return $this->roles->contains('name', $role);
        }

        return !! $role->intersect($this->roles)->count();
    }

    /**
     * Check if user has group
     *
     * @return Response
     * @author G-Factor
     */
    public function hasGroup($group) {

        if(is_string($group))
        {
            return $this->groups->contains('name', $group);
        }

        return !! $group->intersect($this->groups)->count();
    }

    /**
     * Assign Role to user
     *
     * @return Response
     * @author G-Factor
     */
    public function assignRole($role) 
    {
        if(is_string($role))
        {
            return $this->roles()->save(
                Role::whereName($role)->firstOrFail()
            );
        }

        return $this->roles()->save($role);
    }

    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }

    /**
     * Assign Group to user
     *
     * @return Response
     * @author G-Factor
     */
    public function assignGroup($group) 
    {
        if(is_string($group))
        {
            return $this->groups()->save(
                Group::whereName($group)->firstOrFail()
            );
        }

        return $this->groups()->save($group);
    }

    public function removeGroup($group)
    {
        return $this->groups()->detach($group);
    }

    /**
     * Confirm the user.
     *
     * @return void
     * @author G-Factor
     */
    public function confirmEmail()
    {
        $this->verified = true;
        $this->token = null;

        $this->save();
    }
}
