<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ingredient extends Model
{
    use SoftDeletes;
    
    /**
     * Listen for ingredient events
     *
     * @return void
     * @author G-Factor
     **/
    public static function boot()
    {
        parent::boot();

        static::creating(function($ingredient) {
            $ingredient->slug = str_slug($ingredient->name);
            $ingredient->code = uniqid();
        });

        static::updating(function($ingredient) {

            $ingredient->slug = str_slug($ingredient->name);
        });
    }

    /**
     * Get route key for this model
     *
     * @return void
     * @author G-Facotr
     **/
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
        'name',
        'slug',
        'manufacturer',
        'code',
        'color',
        'stock_status',
        'cost',
        'old_price',
        'price',
        'vat',
        'sales_unit',
        'sales_unit_measurement_id',
        'weight',
        'height',
        'width',
        'delivery_fee',
        'quantity',
        'minimum_order',
        'require_shipping',
        'description',
        'image_url',
        'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'cost'
    ];

    public function setOldPriceAttribute($value){
        $this->attributes['old_price'] = (int) $value;
    }

    public function setVatAttribute($value){
        $this->attributes['vat'] = (int) $value;
    }

    public function setSalesUnitAttribute($value){
        $this->attributes['sales_unit'] = (int) $value;
    }

    public function setSalesUnitMeasurementIdAttribute($value){
        $this->attributes['sales_unit_measurement_id'] = (int) $value;
    }

    public function setWeightAttribute($value){
        $this->attributes['weight'] = (int) $value;
    }

    public function setHeightAttribute($value){
        $this->attributes['height'] = (int) $value;
    }

    public function setWidthAttribute($value){
        $this->attributes['width'] = (int) $value;
    }

    public function setDeliveryFeeAttribute($value){
        $this->attributes['delivery_fee'] = (int) $value;
    }

    /**
     * Ingredients belongsToMany attributes
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }

    /**
     * Ingredients belongsToMany recipes
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function recipes()
    {
        return $this->belongsToMany(Recipe::class);
    }

    /**
     * Ingredients belongsToMany easycooks
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function easycooks()
    {
        return $this->belongsToMany(Easycook::class);
    }

    /**
     * Ingredients belongsToMany Orders
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function orders()
    {
        return $this->belongsToMany(Ingredient::class)->withPivot('quantity');
    }

    /**
     * Ingredients belongsTo Measurements
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function sales_unit_measurement()
    {
        return $this->belongsTo(Measurement::class);
    }
}
