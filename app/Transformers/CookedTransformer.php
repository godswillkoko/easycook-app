<?php

/**
 * Created by G-Factor
 * Date: 01/22/2017
 * Time: 07::00 AM
 */

namespace App\Transformers;

/**
* 
*/
class CookedTransformer extends Transformer
{
	
	/*
	 * @return array
	 */
	public function transform($cooked)
	{
		return [
            'name' => $cooked['name'],
            'slug' => $cooked['slug'],
            'ingredients' => $cooked->ingredients,
            'manufacturer' => $cooked['manufacturer'],
            'code' => $cooked['code'],
            'color' => $cooked['color'],
            'stock_status' => $cooked['stock_status'],
            'cost' => $cooked['cost'],
            'old_price' => $cooked['old_price'],
            'price' => $cooked['price'],
            'vat' => $cooked['vat'],
            'sales_unit' => $cooked['sales_unit'],
            'sales_unit_measurement' => $cooked->sales_unit_measurement['label'],
            'weight' => $cooked['weight'],
            'weight_measurement' => $cooked->weight_measurement['label'],
            // 'height' => $cooked['height'],
            // 'height_measurement' => $cooked->height_measurement['label'],
            // 'width' => $cooked['width'],
            // 'width_measurement' => $cooked->width_measurement['label'],
            'added_delivery_fee' => $cooked['delivery_fee'],
            'available_quantity' => $cooked['quantity'],
            // 'require_shipping' => (boolean) $cooked['require_shipping'],
            'description' => $cooked->description,
            'minimum_order' => $cooked['minimum_order'],
            // 'status' => (boolean) $cooked['status'],
            'images' => $cooked->cooked_images
        ];
	}
}