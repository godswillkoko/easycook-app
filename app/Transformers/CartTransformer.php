<?php

/**
 * Created by G-Factor
 * Date: 02/28/2017
 * Time: 07::00 AM
 */

namespace App\Transformers;

/**
* 
*/
class CartTransformer extends Transformer
{
	
	/*
	 * @return array
	 */
	public function transform($cart)
	{
		return [
            'cart_id' => $cart['id'],
            'recipe' => $cart->recipe,
            'recipe_id' => $cart->recipe->id,
            'recipe_code' => $cart->recipe->code,
            'category' => $cart->recipe->category->label,
            'size_id' => $cart->size->id,
            'size_name' => $cart->size->size_name,
            'size_price' => $cart->size->size_price,
            'sizes' => $cart->recipe->sizes,
            'extras' => $cart->recipe->extras,
            'selectedExtras' => $cart->cart_extras->pluck('id'),
            'selectedExtrasName' => $cart->cart_extras->pluck('extra_name'),
            'quantity' => $cart['quantity'],
            'order_status' => (boolean)$cart['status'],
            'sub_total' => $cart->size->size_price * $cart['quantity'] + $cart->cart_extras->sum('extra_price')
        ];
	}
}