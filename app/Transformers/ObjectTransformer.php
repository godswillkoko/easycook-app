<?php
/**
 * Created by G-Factor
 * Date: 8/27/2016
 * Time: 1:32 AM
 */

namespace App\Transformers;


abstract class ObjectTransformer {
    /**
     * Transform a collection of items
     */
    public function transformObject($items)
    {
        return $this->transform($items);
    }

    public abstract function transform($item);
}