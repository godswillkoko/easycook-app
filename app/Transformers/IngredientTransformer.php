<?php
/**
 * Created by G-Factor
 * Date: 01/22/2017
 * Time: 1:34 AM
 */

namespace App\Transformers;


class IngredientTransformer extends Transformer {

    /**
     * @param $ingredient
     * @return array
     */
    public function transform($ingredient)
    {
        return [
            'name' => $ingredient['name'],
            'slug' => $ingredient['slug'],
            'manufacturer' => $ingredient['manufacturer'],
            'code' => $ingredient['code'],
            'color' => $ingredient['color'],
            'stock_status' => $ingredient['stock_status'],
            // 'cost' => $ingredient['cost'],
            'old_price' => $ingredient['old_price'],
            'price' => $ingredient['price'],
            'vat' => $ingredient['vat'],
            'sales_unit' => $ingredient['sales_unit'],
            'sales_unit_measurement' => $ingredient->sales_unit_measurement['label'],
            'weight' => $ingredient['weight'],
            'weight_measurement' => $ingredient->weight_measurement['label'],
            'height' => $ingredient['height'],
            'height_measurement' => $ingredient->height_measurement['label'],
            'width' => $ingredient['width'],
            'width_measurement' => $ingredient->width_measurement['label'],
            'added_delivery_fee' => $ingredient['delivery_fee'],
            'available_quantity' => $ingredient['quantity'],
            // 'require_shipping' => (boolean) $ingredient['require_shipping'],
            'description' => $ingredient->description,
            'minimum_order' => $ingredient['minimum_order'],
            // 'status' => (boolean) $ingredient['status'],
            'images' => $ingredient->ingredient_images,
            'attributes' => $ingredient->attributes
        ];
    }
}