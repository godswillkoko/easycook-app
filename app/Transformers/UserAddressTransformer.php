<?php

/**
 * Created by G-Factor
 * Date: 01/22/2017
 * Time: 07::00 AM
 */

namespace App\Transformers;

/**
* 
*/
class UserAddressTransformer extends Transformer
{
	
	/*
	 * @return array
	 */
	public function transform($user_address)
	{
		return [
			'id' => $user_address['id'],
            'address' => $user_address['address'],
            'region_id' => $user_address['region_id'],
            'region_name' => $user_address->region->name,
            'state_id' => $user_address['state'],
            'state_name' => $user_address->state->name,
            'country_id' => $user_address['country_id'],
            'country_name' => $user_address->country->name,
        ];
	}
}