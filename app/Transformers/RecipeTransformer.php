<?php
/**
 * Created by G-Factor
 * Date: 01/22/2017
 * Time: 3:34 AM
 */

namespace App\Transformers;


class RecipeTransformer extends Transformer {

    /**
     * @param $pro
     * @return array
     * @author G-Factor
     */
    public function transform($recipe)
    {
        return [
            'id' => $recipe['id'],
            'name' => $recipe['name'],
            'slug' => $recipe['slug'],
            'category' => $recipe->category->label,
            'ingredients' => $recipe->ingredients,
            'sizes' => $recipe->sizes,
            'extras' => $recipe->extras,
            'chef' => $recipe['chef'],
            'code' => $recipe['code'],
            'stock_status' => $recipe['stock_status'],
            // 'cost' => $recipe['cost'],
            'old_price' => $recipe['old_price'],
            'price' => $recipe['price'],
            'vat' => $recipe['vat'],
            'sales_unit' => $recipe['sales_unit'],
            'sales_unit_measurement' => $recipe->sales_unit_measurement['label'],
            'weight' => $recipe['weight'],
            'added_delivery_fee' => $recipe['delivery_fee'],
            'available_quantity' => $recipe['quantity'],
            'require_shipping' => (boolean) $recipe['require_shipping'],
            'description' => $recipe->description,
            'tips' => $recipe->tips,
            'minimum_order' => $recipe['minimum_order'],
            // 'status' => (boolean) $recipe['status'],
            'image' => $recipe['image_url']
        ];
    }    
}