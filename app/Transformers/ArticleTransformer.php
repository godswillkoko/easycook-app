<?php
/**
 * Created by G-Factor
 * Date: 01/22/2017
 * Time: 3:34 AM
 */

namespace App\Transformers;


class ArticleTransformer extends Transformer {

    /**
     * @param $pro
     * @return array
     * @author G-Factor
     */
    public function transform($article)
    {
        return [
            'title' => $article['title'],
            'slug' => $article['slug'],
            'text' => $article['text'],
            'image' => $article['image_url'],
            'home' => (boolean)$article['home']
        ];
    }    
}