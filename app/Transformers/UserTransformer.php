<?php

/**
 * Created by G-Factor
 * Date: 01/22/2017
 * Time: 07::00 AM
 */

namespace App\Transformers;

/**
* 
*/
class UserTransformer extends ObjectTransformer
{
	
	/*
	 * @return array
	 */
	public function transform($user)
	{
		return [
			'username' => $user['username'],
            'name' => $user['name'],
            'email' => $user['email'],
            'group' => $user->group->label,
            'gender' => $user['gender'],
            'phone' => $user['phone'],
            'about_me' => $user['about_me'],
            'newsletter' => $user['newsletter']
        ];
	}
}