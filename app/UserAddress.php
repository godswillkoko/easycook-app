<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'user_id',
		'address',
		'region_id',
		'state_id',
		'country_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * User address belongsTo user
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * User address belongsTo region
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    /**
     * User address belongsTo state
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    /**
     * User address belongsTo country
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
