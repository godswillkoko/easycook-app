<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Measurement extends Model
{
    use SoftDeletes;

    /**
     * Listen for measurement events
     *
     * @return void
     * @author G-Factor
     **/
    public static function boot()
    {
        parent::boot();

        static::creating(function($measurement) {
            $measurement->slug = str_slug($measurement->label);
        });

        static::updating(function($measurement) {
            $measurement->slug = str_slug($measurement->label);
        });
    }

    /**
     * Get the route key for this model
     *
     * @return void
     * @author G-Factor
     **/
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'slug',
		'label',
		'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Measurement belongsToMany products
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('unit');
    }

}
