<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;
    
	/**
     * Listen for article events
     *
     * @return void
     * @author G-Factor
     **/
    public static function boot()
    {
        parent::boot();

        static::creating(function($article) {
            $article->slug = str_slug($article->title);
        });

        static::updating(function($article) {

            $article->slug = str_slug($article->title);
        });
    }

    /**
     * Get the route key for this model
     *
     * @return void
     * @author G-Factor
     **/
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'slug',
    	'title',
		'text',
		'home',
        'status',
		'image_url'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];
}
