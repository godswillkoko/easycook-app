<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecipeRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\RecipeImage;
use App\Measurement;
use App\Ingredient;
use App\Attribute;
use App\Category;
use App\Recipe;
use App\Extra;
use App\Size;

class RecipeController extends Controller
{
    /**
     * Index and list of all store recipes
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$recipes = Recipe::latest()->paginate(20);

    	return view('backend.store.recipes.index', compact('recipes'));
    }

    /**
     * Create a new recipe
     *
     * @return void
     * @author G-Factor
     **/
    public function create()
    {
        $sizes = [];
        $extras = [];
        $selected_ingredient = [];
        $categories = Category::pluck('label', 'id');
        $ingredients = Ingredient::pluck('name', 'id');
        $measurements = Measurement::pluck('label', 'id');

    	return view('backend.store.recipes.create', compact('ingredients', 'measurements', 'selected_ingredient', 'extras', 'sizes', 'categories'));
    }

    /**
     * Store the recipe details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(RecipeRequest $request)
    {
        // Filter this array & remove indexes with empty values (if any)
        $en_inputs = array_filter($request->input('extra_name'));
        $ep_inputs = array_filter($request->input('extra_price'));
        $sb_inputs = array_filter($request->input('size_number'));
        $sn_inputs = array_filter($request->input('size_name'));
        $sp_inputs = array_filter($request->input('size_price'));

        // Remove the gaps & reset offset to 0
        $en_inputs = array_splice($en_inputs, 0);
        $ep_inputs = array_splice($ep_inputs, 0);
        $sb_inputs = array_splice($sb_inputs, 0);
        $sn_inputs = array_splice($sn_inputs, 0);
        $sp_inputs = array_splice($sp_inputs, 0);

        // Create the recipe
    	$recipe = Recipe::create($request->all());
        // Set price equal to the first index of the price in sizes fields
        $recipe->price = $sp_inputs[0];
        $recipe->save();

        // Store image (if any)
        if($request->hasFile('image')){
            
            $this->validate($request, [
                'image' => 'mimes:jpeg,bmp,png,jpg,gif'
            ]);

            $recipe->image_url = request()->file('image')->store('photos');
            $recipe->save();
        }

        // Sync the ingredients of this recipe
        if ($request->has('ingredient_list')) {
        	$this->syncIngredient($recipe, $request->input('ingredient_list'));
        }

        // Loop through & save the extras
        for ($i=0; $i < count($en_inputs); $i++) { 

            $extras = new Extra();
            $extras->recipe_id = $recipe->id;
            $extras->extra_name = $en_inputs[$i];
            $extras->extra_price = $ep_inputs[$i];
            $extras->extra_display_name = $en_inputs[$i] . ' - ₦' . $ep_inputs[$i];
            $recipe->extras()->save($extras);
        }

        // Loop through & save the sizes
        for ($i=0; $i < count($sn_inputs); $i++) { 

            $sizes = new Size();
            $sizes->recipe_id = $recipe->id;
            $sizes->size_number = $sb_inputs[$i];
            $sizes->size_name = $sn_inputs[$i];
            $sizes->size_price = $sp_inputs[$i];
            $recipe->sizes()->save($sizes);
        }

    	flash()->success("Recipe successfully added.");
    	return redirect('store/recipes');
    }

    /**
     * Edit store recipe details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit(Recipe $recipe)
    {
        $extras = Extra::whereRecipeId($recipe->id)->get();
        $sizes = Size::whereRecipeId($recipe->id)->get();
        $ingredients = Ingredient::pluck('name', 'id');
        $measurements = Measurement::pluck('label', 'id');
        $categories = Category::pluck('label', 'id');
        $selected_ingredient = $recipe->ingredients->pluck('id')->all();

    	return view('backend.store.recipes.edit', compact('recipe', 'extras', 'sizes', 'measurements', 'selected_ingredient', 'ingredients', 'categories'));
    }

    /**
     * Update recipe details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(RecipeRequest $request, Recipe $recipe)
    {
        // Filter this array & remove indexes with empty values (if any)
        $en_inputs = array_filter($request->input('extra_name'));
        $ep_inputs = array_filter($request->input('extra_price'));
        $sb_inputs = array_filter($request->input('size_number'));
        $sn_inputs = array_filter($request->input('size_name'));
        $sp_inputs = array_filter($request->input('size_price'));

        // Remove the gaps & reset offset to 0
        $en_inputs = array_splice($en_inputs, 0);
        $ep_inputs = array_splice($ep_inputs, 0);
        $sb_inputs = array_splice($sb_inputs, 0);
        $sn_inputs = array_splice($sn_inputs, 0);
        $sp_inputs = array_splice($sp_inputs, 0);

        // dd($sb_inputs, $sn_inputs, $sp_inputs);

        // Update the recipe
        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
            $recipe->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

    	$recipe->update($request->all());
        // Set price equal to the first index of the price in sizes fields
        $recipe->price = $sp_inputs[0];
        $recipe->save();

        // Save image (if any)
        if($request->hasFile('image')){
            
            $this->validate($request, [
                'image' => 'mimes:jpeg,bmp,png,jpg,gif'
            ]);

            // Delete already existing image if it exist
            if ($recipe->image_url){
                \Storage::delete([
                    $recipe->image_url
                ]);
            }

            $recipe->image_url = request()->file('image')->store('photos');
            $recipe->save();
        }

        // Sync ingredients
        if ($request->has('ingredient_list')) {
            $this->syncIngredient($recipe, $request->input('ingredient_list'));
        }

        // Delete existing extras if they exist before saving the new array
        // $get_extras = Extra::whereRecipeId($recipe->id)->get();
        // if($get_extras) {
        //     foreach ($get_extras as $ex) {
        //         $ex->delete();
        //     }
        // }

        // Loop through & save the extras
        for ($i=0; $i < count($en_inputs); $i++) { 

            Extra::whereRecipeId($recipe->id)->updateOrCreate([
                'recipe_id' => $recipe->id,
                'extra_name' => $en_inputs[$i],
                'extra_display_name' => $en_inputs[$i] . ' - ₦' . $ep_inputs[$i],
                'extra_price' => $ep_inputs[$i]
            ]);
        }

        // Delete existing sizes if they exist before saving the new array
        // $get_sizes = Size::whereRecipeId($recipe->id)->get();
        // if($get_sizes) {
        //     foreach ($get_sizes as $sz) {
        //         $sz->delete();
        //     }
        // }

        // Loop through & save the sizes
        for ($i=0; $i < count($sn_inputs); $i++) { 

            Size::whereRecipeId($recipe->id)->updateOrCreate([
                'recipe_id' => $recipe->id,
                'size_number' => $sb_inputs[$i],
                'size_name' => $sn_inputs[$i],
                'size_price' => $sp_inputs[$i]
            ]);
        }

    	flash()->success("Recipe update was successful.");
    	return redirect('store/recipes');
    }

    /**
     * Delete recipes details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy(Recipe $recipe)
    {
    	$recipe->delete();

    	flash()->success("Recipe successfully deleted");
    	return redirect()->back();
    }

    /**
     * Delete extras from the recipe
     *
     * @return Response
     * @author G-Factor
     **/
    public function deleteExtra($id)
    {
        $extra = Extra::whereId($id)->firstOrFail();
        $extra->delete();

        flash()->success("Recipe extra successfully deleted");
        return redirect()->back();
    }

    /**
     * Delete sizes from the recipe
     *
     * @return Response
     * @author G-Factor
     **/
    public function deleteSize($id)
    {
        $size = Size::whereId($id)->firstOrFail();
        $size->delete();
        dd($size);

        flash()->success("Recipe size successfully deleted");
        return redirect()->back();
    }

    /**
     * Sync up the list of ingredients in the db
     *
     * @param Recipe $recipe
     * @param array $ingredients
     * @author G-Factor
     **/
    private function syncIngredient(Recipe $recipe, array $ingredients)
    {
        $recipe->ingredients()->sync($ingredients);
    }
}
