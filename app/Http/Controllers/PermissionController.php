<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PermissionRequest;
use App\Http\Requests;
use App\Permission;

class PermissionController extends Controller
{    
    /**
     * List of all the permissions
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$permissions = Permission::latest()->paginate(20);

    	return view('backend.permissions.index', compact('permissions'));
    }

    /**
     * Save a new permission
     *
     * @param PermissionRequest $request
     * @return Response
     * @author G-Factor
     */
    public function store(PermissionRequest $request)
    {
    	$permission = Permission::create($request->all());

        flash()->success('Permission Created Successfully!');
    	return redirect('user/permissions');
    }

    /**
     * Edit existing permission
     *
     * @return Response
     * @author G-Factor
     */
    public function edit(Permission $permission)
    {
    	$permissions = Permission::latest()->get();

    	return view('backend.permissions.edit', compact('permission', 'permissions'));
    }

    /**
     * Update existing permission
     *
     * @return Response
     * @author G-Factor
     */
    public function update(Permission $permission, PermissionRequest $request)
    {
    	$permission->update($request->all());

        flash()->success('Permission Successfully Updated!');
    	return redirect('user/permissions');
    }

    /**
     * Delete permission
     *
     * @return Response
     * @author G-Factor
     */
    public function destroy(Permission $permission)
    {
    	$permission->delete();

        flash()->success('Permission Deleted Successfully!');
    	return redirect('user/permissions');
    }
}
