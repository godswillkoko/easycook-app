<?php

namespace App\Http\Controllers;

use App\Transformers\UserAddressTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\UserAddress;
use Validator;
use App\User;
use Response;
use Hash;

class AuthController extends ApiController
{
    protected $userTransformer;
    protected $userAddressTransformer;

    /**
     * @param UserTransformer $userTransformer
     * @author G-Factor
     */
    function __construct(UserTransformer $userTransformer, UserAddressTransformer $userAddressTransformer)
    {
        $this->userTransformer = $userTransformer;
        $this->userAddressTransformer = $userAddressTransformer;
    }

    /**
     * Get user profile
     * @return mixed
     * @author G-Factor
     */
    public function getUser(Request $request)
    {
        // return $request->user();
        return $this->respond([
            'data' => $this->userTransformer->transformObject($request->user())
        ]);
    }

    /**
     * Get specific user addresses
     *
     * @return response
     * @author G-factor
     **/
    // public function getAddress(Request $request)
    // {
    //     $address = UserAddress::whereUserId($request->user()->id)->firstOrFail();

    //     return $this->respond([
    //         'data' => $this->userAddressTransformer->transformObject($address)
    //     ]);
    // }

    /**
     * Get all the user addresses
     *
     * @return response
     * @author G-factor
     **/
    public function getAddresses(Request $request)
    {
        $addresses = UserAddress::whereUserId($request->user()->id)->get();

        return $this->respond([
            'data' => $this->userAddressTransformer->transformCollection($addresses->all())
        ]);
    }

    /**
     * Add new user address
     *
     * @return response
     * @author G-Factor
     **/
    public function createUserAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [            
            'address' => 'required',
            'region_id' => 'required',
            'state_id' => 'required',
            'country_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->respondUnprocessed($validator->messages()->all());
        }

        $user_address = new UserAddress;
        $user_address->user_id = $request->user()->id;
        $user_address->address = $request->input('address');
        $user_address->region_id = $request->input('region_id');
        $user_address->state_id = $request->input('state_id');
        $user_address->country_id = $request->input('country_id');
        $user_address->save();


        return $this->respondSuccess('Address successfully added.');
    }

    /**
     * Update the user details
     *
     * @return response
     * @author G-Factor
     **/
    public function updateUser(Request $request)
    {
        $user = User::findOrFail($request->user()->id);

        $validator = Validator::make($request->all(), [            
            'name' => 'required|max:255',
            'username' => 'unique:users',
            'gender' => 'required|in:Male,Female',
            'phone' => 'required|digits_between:7,15',
            // 'about_me' => 'string',
            'newsletter' => 'boolean'
        ]);

        if ($validator->fails()) {
            return $this->respondUnprocessed($validator->messages()->all());
        }

        $user->update($request->all());

        return $this->respondSuccess('Profile update was successful.');
    }

    /**
     * Update the user address
     *
     * @return response
     * @author G-Factor
     **/
    public function updateUserAddress(Request $request)
    {
        $address = UserAddress::whereUserId($request->user()->id)->firstOrFail();

        $validator = Validator::make($request->all(), [            
            'address' => 'required',
            'region_id' => 'required',
            'state_id' => 'required',
            'country_id' => 'required'
        ]);

        if($validator->fails()) {
            return $this->respondUnprocessed($validator->messages()->all());
        }

        $address->update($request->all());

        return $this->respondSuccess('Address update was successful.');
    }

    /**
     * Update user password
     *
     * @param  Request  $request
     * @return Response
     * @author G-Factor
     **/
    public function changePassword(Request $request)
    {
        $user = User::findOrFail($request->user()->id);

        /*
        * Validate all input fields
        */
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'password' => 'required|min:6|confirmed'
        ]);

        if($validator->fails()) {
            return $this->respondUnprocessed($validator->messages()->all());
        }

        if (Hash::check($request->input('current_password'), $user->password)) { 
           $user->fill([
                'password' => bcrypt($request->password)
            ])->save();

            return $this->respondSuccess('Password update successful. Please login again.');
        } else {
            return $this->respondUnprocessed('Incorrect current password.');
        }       
    }

    /**
     * Delete user address
     *
     * @return response
     * @author G-Factor
     **/
    public function deleteUserAddress(Request $request, $id)
    {
        $u_address = UserAddress::findOrFail($id);
        $u_address->delete();

        return $this->respondSuccess('Address successfully deleted.');
    }
}
