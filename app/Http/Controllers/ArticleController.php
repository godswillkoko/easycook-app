<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Article;

class ArticleController extends Controller
{
    /**
     * Index and list of all articles
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$articles = Article::latest()->paginate(20);

    	return view('backend.pages.articles.index', compact('articles'));
    }

    /**
     * Store the article details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(ArticleRequest $request)
    {
    	$article = Article::create($request->all());

    	// Store image (if any)
        if($request->hasFile('image')){
            
            $this->validate($request, [
                'image' => 'mimes:jpeg,bmp,png,jpg,gif,svg'
            ]);

            $article->image_url = request()->file('image')->store('photos');
            $article->save();
        }

    	flash()->success("Article successfully added.");
    	return redirect()->back();
    }

    /**
     * Edit article details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit(Article $article)
    {
    	return view('backend.pages.articles.edit', compact('article'));
    }

    /**
     * Update article details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(ArticleRequest $request, Article $article)
    {
        $checkboxes = array('status','home');
        foreach ($checkboxes as $cb) {
        $article->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

    	$article->update($request->all());

    	// Store image (if any)
        if($request->hasFile('image')){
            
            $this->validate($request, [
                'image' => 'mimes:jpeg,bmp,png,jpg,gif,svg'
            ]);

            $article->image_url = request()->file('image')->store('photos');
            $article->save();
        }

    	flash()->success("Article update was successful.");
    	return redirect('pages/articles');
    }

    /**
     * Delete article details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy(Article $article)
    {
    	$article->delete();

    	flash()->success("Article successfully deleted.");
    	return redirect()->back();
    }
}
