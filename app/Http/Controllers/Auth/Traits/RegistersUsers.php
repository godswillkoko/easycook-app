<?php

namespace App\Http\Controllers\Auth\Traits;

use App\Notifications\AccountActivationEmail;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Role;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $check_email = User::whereEmail($request->input('email'))->first();

        if ($check_email) {
            return $this->respondUnprocessed('Sorry, it appears this email is already in use.');
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);


        if ($validator->fails()) {
            return $this->respondUnprocessed('Please check form for possible errors!');
        }

        event(new Registered(
            $user = $this->create($request->all())
        ));

        // Assign default role to the user
        $role = Role::whereName('user')->first();
        $user->assignRole($role);
        // Send activation mail to the user
        $user->notify(new AccountActivationEmail($user));

        $this->guard()->login($user);

        // return $this->registered($request, $user)
        //     ?: redirect($this->redirectPath());

        // return redirect('/login');
        return $this->respondCreated("Thank you for registering. Please check you email box(s) for further instruction.");
    }

    /**
     * Activate a user's email address. Set 'active' => true
     *
     * @param  string $token
     * @return mixed
     * @author G-Factor
     */
    public function confirmEmail($token)
    {
        $user = User::whereToken($token)->firstOrFail();

        $this->guard()->login($user);

        $user->confirmEmail();

        flash()->success('Congrats. Your account has been confirmed. Enjoy.');
        return redirect('/');
    }
    
    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
