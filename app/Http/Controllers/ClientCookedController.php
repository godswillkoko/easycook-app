<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Transformers\CookedTransformer;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cooked;
use App\Country;
use Response;
use Input;

class ClientCookedController extends ApiController
{
    protected $cookedTransformer;

    /**
     * @param CookedTransformer $cookedTransformer
     * @author G-Factor
     */
    function __construct(CookedTransformer $cookedTransformer)
    {
        $this->cookedTransformer = $cookedTransformer;
    }

    /**
     * List all the cookeds
     * @return mixed
     * @author G-Factor
     */
    public function getCookeds()
    {
        $limit = Input::get('limit') ?: 12;
        $cookeds = Cooked::whereStatus(true)->with('cooked_images')->latest()->paginate($limit);

        return $this->respondWithPagination($cookeds, [
            'data' => $this->cookedTransformer->transformCollection($cookeds->all())
        ]);
    }
}
