<?php

namespace App\Http\Controllers;

use App\Http\Requests\IngredientRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Measurement;
use App\Ingredient;
use App\Attribute;

class IngredientController extends Controller
{
    /**
     * Index and list of all store ingredients
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$ingredients = Ingredient::latest()->paginate(20);

    	return view('backend.store.ingredients.index', compact('ingredients'));
    }

    /**
     * Create a new ingredient
     *
     * @return void
     * @author G-Factor
     **/
    public function create()
    {
        $measurements = Measurement::pluck('label', 'id')->prepend('Please select...', '');
        $attributes = Attribute::pluck('label', 'id');
        $selected_attribute = [];

    	return view('backend.store.ingredients.create', compact('categories', 'measurements', 'attributes', 'selected_attribute'));
    }

    /**
     * Store the ingredient details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(IngredientRequest $request)
    {
    	$ingredient = Ingredient::create($request->all());  

        if($request->hasFile('image')){
            
            $this->validate($request, [
                'image' => 'mimes:jpeg,bmp,png,jpg,gif'
            ]);

            $ingredient->image_url = request()->file('image')->store('photos');
            $ingredient->save();
        }      

    	if ($request->has('attribute_list')) {
            $this->syncAttributes($ingredient, $request->input('attribute_list'));
        }

    	flash()->success("Ingredient successfully added.");
    	return redirect('store/ingredients');
    }

    /**
     * Edit store ingredient details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit(Ingredient $ingredient)
    {
        $measurements = Measurement::pluck('label', 'id')->prepend('Please select...', '');
        $attributes = Attribute::pluck('label', 'id');
        $selected_attribute = $ingredient->attributes->pluck('id')->all();

    	return view('backend.store.ingredients.edit', compact('ingredient', 'measurements', 'categories', 'attributes', 'selected_attribute'));
    }

    /**
     * Update ingredients details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(IngredientRequest $request, Ingredient $ingredient)
    {
        if($request->hasFile('image')){
            
            $this->validate($request, [
                'image' => 'mimes:jpeg,bmp,png,jpg,gif'
            ]);

            // Delete already existing image if it exist
            if ($ingredient->image_url){
                \Storage::delete([
                    $ingredient->image_url
                ]);
            }

            $ingredient->image_url = request()->file('image')->store('photos');
            $ingredient->save();
        }

        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
        $ingredient->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

    	$ingredient->update($request->all());

        if ($request->has('attribute_list')) {
            $this->syncAttributes($ingredient, $request->input('attribute_list'));
        }

    	flash()->success("Ingredient update was successful.");
    	return redirect('store/ingredients');
    }

    /**
     * Delete ingredients details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy(Ingredient $ingredient)
    {
    	$ingredient->delete();

    	flash()->success("Ingredient successfully deleted");
    	return redirect()->back();
    }

    /**
     * Sync up the list of attributes in the db
     *
     * @param Ingredient $ingredient
     * @param array $attributes
     * @author G-Factor
     **/
    private function syncAttributes(Ingredient $ingredient, array $attributes)
    {
        $ingredient->attributes()->sync($attributes);
    }
}
