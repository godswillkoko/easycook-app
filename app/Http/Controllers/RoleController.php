<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Http\Requests;
use App\Role;
use App\Permission;

class RoleController extends Controller
{
    /**
     * List of all the roles
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$roles = Role::latest()->paginate(20);
        $permissions = Permission::pluck('label', 'id');

    	return view('backend.roles.index', compact('roles', 'permissions'));
    }

    /**
     * Save a new role
     *
     * @param RoleRequest $request
     * @return Response
     * @author G-Factor
     */
    public function store(RoleRequest $request)
    {
    	$role = Role::create($request->all());

    	$this->syncPermissions($role, $request->input('permission_list'));
    	
        flash()->success('Role Created Successfully!');
    	return redirect('user/roles');
    }

    /**
     * Edit existing role
     *
     * @return View
     * @author G-Factor
     */
    public function edit(Role $role)
    {
    	$roles = Role::pluck('label', 'id');

    	$permissions = Permission::pluck('label', 'id');

    	// Get a list of permission ids associated with the current role
    	$selectedPermission = $role->permissions->pluck('id')->all();

    	return view('backend.roles.edit', compact('role', 'roles', 'permissions', 'selectedPermission'));
    }

    /**
     * Update existing role
     *
     * @return Response
     * @author G-Factor
     */
    public function update(Role $role, RoleRequest $request)
    {
    	$role->update($request->all());

    	$this->syncPermissions($role, $request->input('permission_list'));

        flash()->success('Role Successfully Updated!');
    	return redirect('user/roles');
    }

    /**
     * Delete role
     *
     * @return Response
     * @author G-Factor
     */
    public function destroy(Role $role)
    {
    	$role->delete();

        flash()->success('Role Deleted Successfully!');
    	return redirect('user/roles');
    }

    /**
     * Sync up the list of permissions in the db
     *
     * @param Role $role
     * @param array $permissions
     * @author G-Factor
     **/
    private function syncPermissions(Role $role, array $permissions)
    {
    	$role->permissions()->sync($permissions);
    }
}
