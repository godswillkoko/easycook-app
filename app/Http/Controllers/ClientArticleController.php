<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Transformers\ArticleTransformer;
use Illuminate\Pagination\Paginator;
use App\Http\Request\ArticleRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Article;
use Response;

class ClientArticleController extends ApiController
{
    protected $articleTransformer;

    /**
     * @param RecipeTransformer $RecipeTransformer
     * @author G-Factor
     */
    function __construct(ArticleTransformer $articleTransformer)
    {
        $this->articleTransformer = $articleTransformer;
    }

    /**
     * List all the articles
     * @return mixed
     * @author G-Factor
     */
    public function getArticles()
    {
        $articles = Article::whereStatus(true)->latest()->get();

        return $this->respond([
            'data' => $this->articleTransformer->transformCollection($articles->all())
        ]);
    }

    /**
     * List all the home articles
     * @return mixed
     * @author G-Factor
     */
    public function getHomeArticles()
    {
        $articles = Article::whereStatus(true)->whereHome(true)->latest()->get();

        return $this->respond([
            'data' => $this->articleTransformer->transformCollection($articles->all())
        ]);
    }
}
