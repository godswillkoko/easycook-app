<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegionRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Region;
use App\State;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function index()
    {
        $regions = Region::paginate(20);
        $states = State::pluck('name', 'id');

        return view('backend.regions.index', compact('regions', 'states'));
    }

    /**
     * Store new region
     *
     * @param  \Illuminate\Http\Request  $request
     * @author G-Factor
     */
    public function store(RegionRequest $request)
    {
        Region::create($request->all());

        flash()->success('Region Created Successfully!');
        return redirect('settings/regions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function edit(Region $region)
    {
    	$states = State::pluck('name', 'id');

        return view('backend.regions.edit', compact('region', 'states'));
    }

    /**
     * Update the specified resource in db.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function update(Region $region, RegionRequest $request)
    {
        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
        $region->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

        $region->update($request->all());

        flash()->success('Region Successfully Updated!');
        return redirect('settings/regions');
    }

    /**
     * Remove the specified resource from db.
     *
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function destroy(Region $region)
    {
        $region->delete();

        flash()->success('Region Deleted Successfully!');
        return redirect('settings/regions');
    }
}
