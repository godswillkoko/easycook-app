<?php

namespace App\Http\Controllers;

use App\Http\Requests\AttributeRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Attribute;

class AttributeController extends Controller
{
    /**
     * Index and list of all store-attributes
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$attributes = Attribute::latest()->get();

    	return view('backend.store.attributes.index', compact('attributes'));
    }

    /**
     * Store the store-Attribute details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(AttributeRequest $request)
    {
    	Attribute::create($request->all());

    	flash()->success("Store attribute successfully added.");
    	return redirect()->back();
    }

    /**
     * Edit store-attribute details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit(Attribute $attribute)
    {
    	return view('backend.store.attributes.edit', compact('attribute'));
    }

    /**
     * Update store-attribute details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(AttributeRequest $request, Attribute $attribute)
    {
    	$attribute->update($request->all());

    	flash()->success("Store attribute update was successful.");
    	return redirect('store/attributes');
    }

    /**
     * Delete store-attribute details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy(Attribute $attribute)
    {
    	$Attribute->delete();

    	flash()->success("Store attribute successfully deleted");
    	return redirect()->back();
    }
}
