<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CountryRequest;
use App\Http\Requests;
use App\Country;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function index()
    {
        $countries = Country::paginate(20);

        return view('backend.countries.index', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function store(CountryRequest $request)
    {
        Country::create($request->all());

        flash()->success('Country Created Successfully!');
        return redirect('settings/countries');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function edit(Country $country)
    {

        return view('backend.countries.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function update(Country $country, CountryRequest $request)
    {
        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
        $country->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

        $country->update($request->all());

        flash()->success('Country Successfully Updated!');
        return redirect('settings/countries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function destroy(Country $country)
    {
        $country->delete();

        flash()->success('Country Deleted Successfully!');
        return redirect('settings/countries');
    }
}
