<?php

namespace App\Http\Controllers;

use App\Notifications\AccountActivationEmail;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Group;
use App\User;
use App\Role;
use Hash;

class UserController extends Controller
{
    /**
     * List of all the users
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$users = User::latest()->paginate(20);

    	return view('backend.users.index', compact('users'));
    }

    /**
     * Show user
     *
     * @return Response
     * @author G-Factor
     **/
    public function show($id)
    {
        $user = User::whereId($id)->firstOrFail();

        return view('backend.users.show', compact('user'));
    }

    /**
     * Create a new user
     *
     * @return Response
     * @author G-Factor
     */
    public function create()
    {
        $roles = Role::pluck('label', 'id');

        return view('backend.users.create', compact('roles'));
    }

    /**
     * Save a new user
     *
     * @param RoleRequest $request
     * @return Response
     * @author G-Factor
     */
    public function store(UserRequest $request)
    {
        $user = User::create($request->all());
        $user->password = bcrypt($request->input('password'));
        $user->save();

        $this->syncRoles($user, $request->input('role_id'));

        // Send activation mail to the user
        $user->notify(new AccountActivationEmail($user));
        
        flash()->success('User Created Successfully!');
        return redirect('users');
    }

    /**
     * Edit existing user
     *
     * @return View
     * @author G-Factor
     */
    public function edit(User $user)
    {
        $users = User::pluck('email', 'id');

        $roles = Role::pluck('label', 'id');
        $groups = Group::pluck('label', 'id');

        $selectedRole = $user->roles->pluck('id')->all();

        return view('backend.users.edit', compact('user', 'roles', 'users', 'selectedRole', 'groups'));
    }

    /**
     * Update existing user
     *
     * @return Response
     * @author G-Factor
     */
    public function update($id, Request $request)
    {
        $user = User::whereId($id)->firstOrFail();

        if($user->hasRole('superUser')){
            flash()->error('Sorry, this account cannot be edited!');
            return redirect('users');
        }

        $this->validate($request, [            
            'name' => 'required|max:255',
            // 'email' => 'email',
            'gender' => 'required|in:Male,Female',
            'phone' => 'required|digits_between:7,15',
            'about_me' => 'string',
            'verified' => 'required|boolean'
        ]);

        $user->fill([
            'name' => $request->input('name'),
            // 'email' => $user->email,
            'gender' => $request->input('gender'),
            'phone' => $request->input('phone'),
            'about_me' => $request->input('about_me'),
            'verified' => $request->input('verified')
        ])->save();
        
        // $user->update($request->all());

        if($request->has('password')) {
            $user->password = bcrypt($request->input('password'));
            $user->save();
        }

        $this->syncRoles($user, $request->input('role_id'));

        flash()->success('User Successfully Updated!');
        return redirect('users');
    }

    /**
     * Delete user
     *
     * @return response
     * @author G-Factor
     **/
    public function destroy(User $user)
    {
        if($user->hasRole('superUser')){
            flash()->error('Sorry, this account cannot be deleted!');
            return redirect('users');
        }

        $user->delete();

        flash()->success("User Successfully Deleted.");
        return redirect('users');
    }

    /**
     * Sync up the role in the db
     *
     * @param User $user
     * @param array $roles
     * @author G-Factor
     **/
    private function syncRoles(User $user, array $roles)
    {
        $user->roles()->sync($roles);
    }
}
