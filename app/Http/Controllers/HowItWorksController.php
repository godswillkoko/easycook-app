<?php

namespace App\Http\Controllers;

use App\Http\Requests\HowItWorksRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\HowItWorks;

class HowItWorksController extends Controller
{
    /**
     * Index and list of all how it works
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$howitworks = HowItWorks::latest()->paginate(20);

    	return view('backend.pages.hiws.index', compact('howitworks'));
    }

    /**
     * Store the how it works details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(HowItWorksRequest $request)
    {
    	$howitworks = HowItWorks::create($request->all());

    	// Store image (if any)
        if($request->hasFile('image')){
            
            $this->validate($request, [
                'image' => 'mimes:jpeg,bmp,png,jpg,gif,svg'
            ]);

            $howitworks->image_url = request()->file('image')->store('photos');
            $howitworks->save();
        }

    	flash()->success("Details successfully added.");
    	return redirect()->back();
    }

    /**
     * Edit hiw details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit($slug)
    {
        $howitworks = HowItWorks::whereSlug($slug)->first();

    	return view('backend.pages.hiws.edit', compact('howitworks'));
    }

    /**
     * Update hiw details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(HowItWorksRequest $request, $slug)
    {
        $howitworks = HowItWorks::whereSlug($slug)->first();

        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
        $howitworks->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

    	$howitworks->update($request->all());

    	// Store image (if any)
        if($request->hasFile('image')){
            
            $this->validate($request, [
                'image' => 'image'
            ]);

            $howitworks->image_url = request()->file('image')->store('photos');
            $howitworks->save();
        }

    	flash()->success("Details update was successful.");
    	return redirect('pages/howitworks');
    }

    /**
     * Delete hiw details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy($slug)
    {
        $howitworks = HowItWorks::whereSlug($slug)->firstOrFail();
    	$howitworks->delete();

    	flash()->success("Details successfully deleted.");
    	return redirect()->back();
    }
}
