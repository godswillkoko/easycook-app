<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeliveryTimeSlotRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\DeliveryTimeSlot;

class DeliveryTimeSlotController extends Controller
{
    /**
     * Index and list of all timeslots
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$timeslots = DeliveryTimeSlot::get();

    	return view('backend.settings.timeslots.index', compact('timeslots'));
    }

    /**
     * Store the timeslot details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(DeliveryTimeSlotRequest $request)
    {
    	DeliveryTimeSlot::create($request->all());

    	flash()->success("Delivery timeslot successfully added.");
    	return redirect()->back();
    }

    /**
     * Edit timeslot details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit($id)
    {
        $timeslot = DeliveryTimeSlot::find($id);
    	return view('backend.settings.timeslots.edit', compact('timeslot'));
    }

    /**
     * Update delivery time slot details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(DeliveryTimeSlotRequest $request, $id)
    {
        $timeslot = DeliveryTimeSlot::find($id);

        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
        $timeslot->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

    	$timeslot->update($request->all());

    	flash()->success("Delivery timeslot update was successful.");
    	return redirect('settings/delivery-timeslots');
    }

    /**
     * Delete delivery time slot details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy($id)
    {
        $timeslot = DeliveryTimeSlot::find($id);
    	$timeslot->delete();

    	flash()->success("Delivery timeslot successfully deleted");
    	return redirect()->back();
    }
}
