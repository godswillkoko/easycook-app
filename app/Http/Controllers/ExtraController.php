<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExtraRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Extra;

class ExtraController extends Controller
{
    /**
     * Index and list of all recipe extras
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$extras = Extra::get();

    	return view('backend.settings.extras.index', compact('extras'));
    }

    /**
     * Store the recipe extra details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(ExtraRequest $request)
    {
    	Extra::create($request->all());

    	flash()->success("Store extra successfully added.");
    	return redirect()->back();
    }

    /**
     * Edit recipe extra details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit(Extra $extra)
    {
    	return view('backend.settings.extras.edit', compact('extra'));
    }

    /**
     * Update recipe extra details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(ExtraRequest $request, extra $extra)
    {
    	$extra->update($request->all());

    	flash()->success("Store extra update was successful.");
    	return redirect('settings/extras');
    }

    /**
     * Delete recipe extra details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy(Extra $extra)
    {
    	$extra->delete();

    	flash()->success("Store extra successfully deleted");
    	return redirect()->back();
    }
}
