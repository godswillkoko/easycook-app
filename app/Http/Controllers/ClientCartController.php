<?php

namespace App\Http\Controllers;

use App\Transformers\CartTransformer;
use Illuminate\Http\Request;
use App\DeliveryTimeSlot;
use App\CartRecipeExtra;
use App\DeliveryMethod;
use App\PaymentMethod;
use App\CartDetails;
use App\CartRecipe;
use App\Cart;
use App\Size;
use DB;

class ClientCartController extends ApiController
{
    protected $cartTransformer;

    /**
     * @param CartTransformer $cartTransformer
     * @author G-Factor
     */
    function __construct(CartTransformer $cartTransformer)
    {
        $this->cartTransformer = $cartTransformer;
    }

    
    /**
     * Add recipe to users cart
     *
     * @return void
     * @author G-Factor
     **/
    public function addToCart(Request $request)
    {
        $extras = $request->input('extras');
        $recipe = $request->input('recipe');
        $quantity = $request->input('quantity');
        $size = $request->input('size');
        // Check if the user already has the recipe
        $cart = Cart::whereUserId($request->user()->id)
                    ->whereRecipeId($recipe)
                    ->first();

        if($cart) {
            // Add new recipe to cart
            $cart->quantity = $quantity;
            $cart->size_id = $size;
            $cart->status = false;
            $cart->save();

            // Sync the recipe extras
            if ($extras) {
                $cart->cart_extras()->sync($extras);
            }
        } else {
            $cart = new Cart();
            $cart->user_id = $request->user()->id;
            $cart->quantity = $quantity;
            $cart->recipe_id = $recipe;
            $cart->size_id = $size;
            $cart->status = false;
            $cart->save();

            // Sync the recipe extras
            if ($extras) {
                $cart->cart_extras()->sync($extras);
            }
        }
        return $this->respondSuccess('Successfully added to cart.');
    }

    /**
     * Get user cart
     *
     * @return Response
     * @author G-Factor
     **/
    public function getCart(Request $request)
    {
        $cart_recipes = Cart::whereUserId($request->user()->id)->latest()->get();

        // Delete items from cart if the admin has deleted the items
        foreach ($cart_recipes as $ct) {

            if(!$ct->recipe) {
                $ct->delete();
            }
        }

        // Calculate cart total (by size)
        $size_total = DB::table('carts')
                ->join('sizes', 'carts.size_id', '=', 'sizes.id')
                ->select(DB::raw('(CASE WHEN sizes.id = carts.size_id THEN SUM(sizes.size_price*carts.quantity) ELSE 0 END) AS size_total'))
                ->where('carts.user_id', $request->user()->id)
                ->groupBy('carts.id')
                ->get();
        // Calculate cart total (by extra(s))
        $extra_total = DB::table('extras')
                ->join('cart_extra', 'extras.id', '=', 'cart_extra.extra_id')
                ->join('carts', 'cart_extra.cart_id', '=', 'carts.id')
                ->select(DB::raw('(CASE WHEN carts.id = cart_extra.cart_id THEN SUM(extras.extra_price) ELSE 0 END) AS extra_total'))
                ->where('carts.user_id', $request->user()->id)
                ->groupBy('carts.id')
                ->get();

        if(!$cart_recipes) {
            return $this->respondNotFound("Your cart is empty.");
        }

        return $this->respond([
            'data' => $this->cartTransformer->transformCollection($cart_recipes->all()),
            'total' => $size_total->sum('size_total') + $extra_total->sum('extra_total')
        ]);
    }

    /**
     * Get cart details
     *
     * @return response
     * @author G-Factor
     **/
    public function getCartDetails(Request $request)
    {
        $cart_details = CartDetails::whereUserId($request->user()->id)->first();
        if (isset($cart_details)) {
            return $this->respond([
                'data' => $cart_details,
                'delivery_region' => $cart_details->user_address->region
            ]);
        } else {
            return $this->respond([
                'data' => null,
                'delivery_region' => null
            ]);
        }
    }

    /**
     * Remove recipe from users cart
     *
     * @return Response
     * @author G-Factor
     **/
    public function removeFromCart(Request $request, $id)
    {
        $cart_recipe = Cart::whereId($id)
                        ->whereUserId($request->user()->id)
                        ->first();
        $cart_recipe->delete($id);

        return $this->respondSuccess("Successfully removed.");
    }

    /**
     * Add details to the cart details table
     * Note the values are temporal & will be deleted later
     *
     * @return void
     * @author G-Factor
     **/
    public function cartDetails(Request $request)
    {
        // Check if the user details already already exist
        $cart_details = CartDetails::whereUserId($request->user()->id)->first();
        if($cart_details) {
            $cart_details->user_id = $request->user()->id;
            $cart_details->comments = $request->input('comments');
            $cart_details->date_scheduled = $request->input('date_scheduled');
            $cart_details->user_address_id = $request->input('user_address_id');
            $cart_details->payment_method_id = $request->input('payment_method_id');
            $cart_details->delivery_method_id = $request->input('delivery_method_id');
            $cart_details->delivery_time_slot_id = $request->input('delivery_time_slot_id');
            $cart_details->save();

            return $this->respondSuccess("Delivery details updated successfully.");
        } else {
            $c_details = new CartDetails;
            $c_details->user_id = $request->user()->id;
            $c_details->comments = $request->input('comments');
            $c_details->date_scheduled = $request->input('date_scheduled');
            $c_details->user_address_id = $request->input('user_address_id');
            $c_details->payment_method_id = $request->input('payment_method_id');
            $c_details->delivery_method_id = $request->input('delivery_method_id');
            $c_details->delivery_time_slot_id = $request->input('delivery_time_slot_id');
            $c_details->save();

            return $this->respondSuccess("Delivery details added successfully.");
        }
    }

    /**
     * Get all the delivery time slots
     *
     * @return Response
     * @author G-Factor
     **/
    public function getTimeSlots()
    {
        $timeslots = DeliveryTimeSlot::whereStatus(true)->get();

        if(count($timeslots) < 1) {
            return $this->respondNotFound("No delivery time slot at the moment. Please check back later.");
        }

        return $this->respond([
            'data' => $timeslots
        ]);
    }

    /**
     * Get all the enabled payment methods
     *
     * @return Response
     * @author G-Factor
     **/
    public function getPaymentMethods()
    {
        $pm = PaymentMethod::whereStatus(true)->get();

        if(count($pm) < 1) {
            return $this->respondNotFound("No payment method available at the moment. Please check back later.");
        }

        return $this->respond([
            'data' => $pm
        ]);
    }

    /**
     * Get all the enabled delivery methods
     *
     * @return Response
     * @author G-Factor
     **/
    public function getDeliveryMethods()
    {
        $del_method = DeliveryMethod::whereStatus(true)->get();

        if(count($del_method) < 1) {
            return $this->respondNotFound("Delivery method is not available at the moment. Please check back later.");
        }

        return $this->respond([
            'data' => $del_method
        ]);
    }

    /**
     * Move user cart recipe & details to order
     *
     * @return response
     * @author G-Factor
     **/
    public function placeOrder(Request $request)
    {
        $cart = Cart::whereUserId($request->user()->id)->get();
        $cart_details = CartDetails::whereUserId($request->user()->id)->first();
        // dd($cart, $cart_details);
        return $this->respondSuccess("Order is being processed.");
    }
}
