<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Transformers\RecipeTransformer;
use Illuminate\Pagination\Paginator;
use App\Http\Request\RecipeRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use App\Country;
use App\Recipe;
use Validator;
use App\Cart;
use Response;
use Input;

class ClientRecipeController extends ApiController
{
    protected $recipeTransformer;

    /**
     * @param RecipeTransformer $RecipeTransformer
     * @author G-Factor
     */
    function __construct(RecipeTransformer $recipeTransformer)
    {
        $this->recipeTransformer = $recipeTransformer;
    }

    /**
     * List all the categories
     * @return mixed
     * @author G-Factor
     */
    public function getCategories()
    {
        $categories = Category::whereStatus(true)->latest()->get();

        return $this->respond([
            'data' => $categories->all()
        ]);
    }

    /**
     * Search recipe
     *
     * @return response
     * @author G-Factor
     **/
    public function searchRecipes(Request $req)
    {
        $data = Recipe::select('name', 'slug')->where("name","LIKE","%{$req->input('name')}%")->get();
        
        return response([
            'data' => $data->all()
        ]);
    }

    /**
     * List all the recipes
     * @return mixed
     * @author G-Factor
     */
    public function getRecipes()
    {
        $limit = Input::get('limit') ?: 12;
        $recipes = Recipe::whereStatus(true)->latest()->get();

        return $this->respond([
            'data' => $this->recipeTransformer->transformCollection($recipes->all())
        ]);

        // return $this->respondWithPagination($recipes, [
        //     'data' => $this->recipeTransformer->transformCollection($recipes->all())
        // ]);
    }

    /**
     * List recipes by categories
     * @return mixed
     * @author G-Factor
     */
    public function getRecipesByCategory($cat)
    {
        $limit = Input::get('limit') ?: 12;
        $cat_recipes = Recipe::whereStatus(true)
                        ->whereCategoryId($cat)
                        ->latest()
                        ->get();

        return $this->respond([
            'data' => $this->recipeTransformer->transformCollection($cat_recipes->all())
        ]);
    }

    /**
     * Show a particular recipe
     *
     * @return Response
     * @author G-Factor
     **/
    public function getRecipe($slug)
    {
        $recipe = Recipe::whereSlug($slug)->first();

        if( !$recipe )
        {
            return $this->respondNotFound("Recipe not found.");
        }

        return $this->respond([
            'data' => $this->recipeTransformer->transform($recipe)
        ]);
    }
}
