<?php

namespace App\Http\Controllers;

use App\Http\Requests\SizeRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Size;

class SizeController extends Controller
{
    /**
     * Index and list of all item-sizes
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$sizes = Size::get();

    	return view('backend.settings.sizes.index', compact('sizes'));
    }

    /**
     * Store the item-size details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(SizeRequest $request)
    {
    	Size::create($request->all());

    	flash()->success("Store size successfully added.");
    	return redirect()->back();
    }

    /**
     * Edit item-size details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit(Size $size)
    {
    	return view('backend.settings.sizes.edit', compact('size'));
    }

    /**
     * Update item-size details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(SizeRequest $request, Size $size)
    {
    	$size->update($request->all());

    	flash()->success("Store size update was successful.");
    	return redirect('settings/sizes');
    }

    /**
     * Delete item-size details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy(Size $size)
    {
    	$size->delete();

    	flash()->success("Store size successfully deleted");
    	return redirect()->back();
    }
}
