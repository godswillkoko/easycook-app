<?php

namespace App\Http\Controllers;

use App\Notifications\OrderNotificationEmail;
use App\Http\Requests\OrderRequest;
use Illuminate\Http\Request;
use App\DeliveryMethod;
use App\PaymentMethod;
use App\UserAddress;
use App\OrderRecipe;
use App\Recipe;
use App\Order;
use App\User;
use App\Size;
use DB;

class OrderController extends Controller
{
    /**
     * Index and list of all orders
     *
     * @return View
     * @author G-Factor
     **/
    public function index()
    {
    	$orders = Order::latest()->get();

    	return view('backend.sales.orders.index', compact('orders'));
    }

    /**
     * Show order
     *
     * @return View
     * @author G-Factor
     **/
    public function show($id)
    {
        $orm_order = Order::find($id);

        $order = DB::table('orders')
            ->join('order_recipe', 'orders.id', '=', 'order_recipe.order_id')
            // ->join('recipes', 'order_recipe.product_id', '=', 'recipes.id')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('user_addresses', 'orders.user_address_id', '=', 'user_addresses.id')
            ->join('states', 'user_addresses.state_id', '=', 'states.id')
            ->join('countries', 'user_addresses.country_id', '=', 'countries.id')
            // Join delivery method & payment method later
            ->select('orders.*', 'users.name as user_fullname', 'users.phone', 'users.email', 'user_addresses.address', 'user_addresses.city', 'states.name as state', 'countries.name as country')
            ->where('orders.id', $id)
            ->first();

        return view('backend.sales.orders.show', compact('order', 'orm_order'));
    }

    /**
     * Create a new order
     *
     * @return View Response
     * @author G-Factor
     **/
    public function create()
    {
        $recipes = Recipe::pluck('name', 'id')->prepend('Please select...', '');
        $delivery_methods = DeliveryMethod::pluck('label', 'id');
        $payment_methods = PaymentMethod::pluck('label', 'id');
        $customers = User::pluck('name', 'id');
        $selectedAddress = null;
        $selectedRecipes = [];
        $selectedSizes = [];
        $addresses = [];
        $sizes = [];

    	return view('backend.sales.orders.create', compact('selectedRecipes', 'selectedSizes', 'delivery_methods', 'customers', 'payment_methods', 'selectedAddress', 'sizes', 'recipes', 'addresses'));
    }

    /**
     * Store the order details
     *
     * @return View Response
     * @author G-Factor
     **/
    public function store(OrderRequest $request)
    {
        // Filter this array & remove indexes with empty values (if any)
        $recipes = array_filter($request->input('recipe_id'));
        $sizes = array_filter($request->input('size_id'));
        $quantities = array_filter($request->input('recipe_quantity'));
        // Remove the gaps & reset offset to 0
        $recipes = array_splice($recipes, 0);
        $sizes = array_splice($sizes, 0);
        $quantities = array_splice($quantities, 0);
        // Create the order
        $order = Order::create($request->all());

        // Loop through & save the recipes
        for ($i=0; $i < count($recipes); $i++) {
            $order_recipe = new OrderRecipe();
            $order_recipe->recipe_id = $recipes[$i];
            $order_recipe->order_id = $order->id;
            $order_recipe->size_id = $sizes[$i];
            $order_recipe->recipe_quantity = $quantities[$i];
            $order->order_recipes()->save($order_recipe);
        }

        // Calculate the order grand total 
        $get_recipes = DB::table('recipes')
            ->join('order_recipes', 'recipes.id', '=', 'order_recipes.recipe_id')
            ->where('order_recipes.order_id', $order->id)
            ->select(
                'recipes.id AS recipe_id',
                DB::raw('(CASE WHEN recipes.id = order_recipes.recipe_id THEN SUM((recipes.price+recipes.delivery_fee)*order_recipes.recipe_quantity) ELSE 0 END) AS total_price'))
            ->groupBy('recipe_id')
            ->get();

        // Update the total column
        $total_price = $get_recipes->sum('total_price');
        $order->update(['total' => $total_price]);

        // Send notification ail to admin
        $order->notify(new OrderNotificationEmail($order));

    	flash()->success("Order successfully added.");
    	return redirect('sales/orders');
    }

    /**
     * Edit order details
     *
     * @return View
     * @author G-Factor
     **/
    public function edit(Order $order)
    {
        $customers = User::pluck('name', 'id');
        $recipes = Recipe::pluck('name', 'id')->prepend('Please select...', '');
        $payment_methods = PaymentMethod::pluck('label', 'id');
        $delivery_methods = DeliveryMethod::pluck('label', 'id');
        $selectedAddress = $order->user_address->id;
        $selectedRecipes = [];
        $selectedSizes = [];
        $addresses = UserAddress::pluck('address', 'id');
        $sizes = Size::pluck('size_name', 'id');

    	return view('backend.sales.orders.edit', compact('order', 'customers', 'recipes', 'selectedRecipes', 'delivery_methods', 'payment_methods', 'selectedAddress','sizes', 'selectedSizes', 'addresses'));
    }

    /**
     * Update orders details
     *
     * @return View
     * @author G-Factor
     **/
    public function update(OrderRequest $request, Order $order)
    {
        // Filter this array & remove indexes with empty values (if any)
        $recipes = array_filter($request->input('recipe_id'));
        $sizes = array_filter($request->input('size_id'));
        $quantities = array_filter($request->input('recipe_quantity'));
        // Remove the gaps & reset offset to 0
        $recipes = array_splice($recipes, 0);
        $sizes = array_splice($sizes, 0);
        $quantities = array_splice($quantities, 0);
        // Update the order
        $order->update($request->all());

        // Loop through & update the recipes
        for ($i=0; $i < count($recipes); $i++) { 

            OrderRecipe::whereOrderId($order->id)->updateOrCreate([
                'recipe_id' => $recipes[$i],
                'order_id' => $order->id,
                'size_id' => $sizes[$i],
                'recipe_quantity' => $quantities[$i]
            ]);
        }

        // Calculate the order grand total 
        $get_recipes = DB::table('recipes')
            ->join('order_recipes', 'recipes.id', '=', 'order_recipes.recipe_id')
            ->where('order_recipes.order_id', $order->id)
            ->select(
                'recipes.id AS recipe_id',
                DB::raw('(CASE WHEN recipes.id = order_recipes.recipe_id THEN SUM((recipes.price+recipes.delivery_fee)*order_recipes.recipe_quantity) ELSE 0 END) AS total_price'))
            ->groupBy('recipe_id')
            ->get();

        // Update the total column
        $total_price = $get_recipes->sum('total_price');
        $order->update(['total' => $total_price]);

    	flash()->success("Order update was successful.");
    	return redirect('sales/orders');
    }

    /**
     * Delete orders details
     *
     * @return View
     * @author G-Factor
     **/
    public function destroy(Order $order)
    {
    	$order->delete();

    	flash()->success("Order successfully deleted.");
    	return redirect()->back();
    }

    /**
     * Delete recipe from the order
     *
     * @return Response
     * @author G-Factor
     **/
    public function deleteRecipe($id)
    {
        $or = OrderRecipe::findOrFail($id);
        $or->delete();

        flash()->success("Order recipe successfully deleted.");
        return redirect()->back();
    }
}
