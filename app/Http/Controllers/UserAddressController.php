<?php

namespace App\Http\Controllers;

use App\Transformers\UserAddressTransformer;
use Illuminate\Http\Request;
use App\UserAddress;

class UserAddressController extends ApiController
{
    protected $userAddressTransformer;

    /**
     * @param UserAddressTransformer $userAddressTransformer
     * @author G-Factor
     */
    function __construct(UserAddressTransformer $userAddressTransformer)
    {
        $this->userAddressTransformer = $userAddressTransformer;
    }

    /**
     * Get all the user addresses
     *
     * @return response
     * @author G-factor
     **/
    public function index(Request $request)
    {
        $addresses = UserAddress::whereUserId($request->user()->id)->get();

        return $this->respond([
            'data' => $this->userAddressTransformer->transformCollection($addresses->all())
        ]);
    }

    /**
     * undocumented function
     *
     * @return response
     * @author G-factor
     **/
    public function show()
    {
    }

    /**
     * Update user address
     *
     * @return Response
     * @author G-Factor
     **/
    public function update()
    {
    }

    /**
     * Delete user address
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy()
    {
    }
}
