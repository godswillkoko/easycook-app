<?php

namespace App\Http\Controllers;

use App\Http\Requests\StateRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Country;
use App\State;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function index()
    {
        $states = State::paginate(20);
        $countries = Country::pluck('name', 'id');

        return view('backend.states.index', compact('states', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @author G-Factor
     */
    public function store(StateRequest $request)
    {
        State::create($request->all());

        flash()->success('State Created Successfully!');
        return redirect('settings/states');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function edit(State $state)
    {
    	$countries = Country::pluck('name', 'id');

        return view('backend.states.edit', compact('state', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function update(State $state, StateRequest $request)
    {
        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
        $state->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

        $state->update($request->all());

        flash()->success('State Successfully Updated!');
        return redirect('settings/states');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     * @author G-Factor
     */
    public function destroy(State $state)
    {
        $state->delete();

        flash()->success('State Deleted Successfully!');
        return redirect('settings/states');
    }
}
