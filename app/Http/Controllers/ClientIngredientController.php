<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Transformers\IngredientTransformer;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\IngredientImage;
use App\Ingredient;
use App\Country;
use Response;
use Input;

class ClientIngredientController extends ApiController
{
    protected $ingredientTransformer;

    /**
     * @param IngredientTransformer $ingredientTransformer
     * @author G-Factor
     */
    function __construct(IngredientTransformer $ingredientTransformer)
    {
        $this->ingredientTransformer = $ingredientTransformer;
    }

    /**
     * List all the ingredients
     * @return mixed
     * @author G-Factor
     */
    public function getIngredients()
    {
        $limit = Input::get('limit') ?: 12;
        $ingredients = Ingredient::whereStatus(true)->latest()->paginate($limit);

        return $this->respondWithPagination($ingredients, [
            'data' => $this->ingredientTransformer->transformCollection($ingredients->all())
        ]);
    }

    /**
     * Show a specific property
     * @param $slug
     * @return mixed
     * @author G-Factor
     */
    public function showProperty($slug)
    {
        $property = Property::whereSlug($slug)->first();

        if( !$property )
        {
            return $this->respondNotFound("Property not found.");
        }

        return $this->respond([
            'data' => $this->propertyTransformer->transform($property)
        ]);
    }


    /**
     * Search for a property
     *
     * @return Data
     * @author G-Factor
     **/
    public function search(Request $request)
    {
    	$types = PropertyType::pluck('label', 'name');
    	$countries = Country::pluck('name', 'name');
        $currencies = Currency::pluck('name', 'symbol');

        // Init ES host for searching
        $es = $this->esInit();
        
        $location = $request->input('location');
        $buyRent = $request->input('buyRent');
        $property_type = $request->input('property_type');
        $currency = $request->input('currency');

        if (!$request->input('price')) {
            $price = 10000000000;
        } else {
            $price = $request->input('price');
        }

        $per_page = $request->get('limit', 12);
        $from = ($request->get('page', 1) - 1) * $per_page;

        // Set search parameters
        $search_params = [
            'index' => $this->es_index,
            'type' => $this->es_type,
            'size' => $per_page,
            'from' => $from,
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [ 
                            'match' => ['buyRent' => $buyRent ]
                        ],
                        'should' => [
                            [ 'match' => [ 
                                'location' => [
                                    'query' => $location,
                                    'boost' => 3 
                                ]
                            ]],
                            [ 'match' => [ 
                                'state' => [
                                    'query' => $location,
                                    'boost' => 2
                                ]
                            ]],
                            [ 'match' => [ 
                                'country' => [
                                    'query' => $location,
                                    'boost' => 2 
                                ]
                            ]],
                            ['bool' => [
                                'should' => [
                                    [ 'term' => [ 
                                        'property_type' => $property_type,
                                        'boost' => 5
                                    ]],
                                    [ 'match' => [ 
                                        'currency' => [
                                            'query' => $currency,
                                            'boost' => 3
                                        ]
                                    ]]                            
                                ]
                            ]]                          
                        ],
                        'filter' => [                                
                            'range' => [
                                'price' => [ 'lte' => $price ]
                            ]
                        ]
                    ]
                ]                
            ]
        ];

        $search = $es->search($search_params);

		$es_hits = $search['hits'];
        $results = new LengthAwarePaginator(
            $es_hits['hits'],
            $es_hits['total'],
            $per_page,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );

        // The $search return type is an array, so we handle the loop in the front end
        // $limit = Input::get('limit') ?: 12;
        // $properties = Property::whereId($search['hits']['hits']['_id'])->latest()->paginate($limit);

        // return $this->respondWithPagination($properties, [
        //    $this->propertyTransformer->transformCollection($properties->all())
        // ]);

        return $this->respond($search);
    }
}
