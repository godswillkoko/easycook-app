<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GroupRequest;
use App\Http\Requests;
use App\Group;

class GroupController extends Controller
{
    /**
     * List of all the groups
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$groups = Group::latest()->paginate(20);

    	return view('backend.groups.index', compact('groups'));
    }

    /**
     * Save a new group
     *
     * @param GroupRequest $request
     * @return Response
     * @author G-Factor
     */
    public function store(GroupRequest $request)
    {

    	Group::create($request->all());

        flash()->success('Group Created Successfully!');
    	return redirect('user/groups');
    }

    /**
     * Edit existing group
     *
     * @return Response
     * @author G-Factor
     */
    public function edit(Group $group)
    {
    	$groups = Group::latest()->get();

    	return view('backend.groups.edit', compact('group', 'groups'));
    }

    /**
     * Update existing group
     *
     * @return Response
     * @author G-Factor
     */
    public function update(Group $group, GroupRequest $request)
    {
    	$group->update($request->all());

        flash()->success('Group Successfully Updated!');
    	return redirect('user/groups');
    }

    /**
     * Delete group
     *
     * @return Response
     * @author G-Factor
     */
    public function destroy(Group $group)
    {
    	$group->delete();

        flash()->success('Group Deleted Successfully!');
    	return redirect('user/groups');
    }
}
