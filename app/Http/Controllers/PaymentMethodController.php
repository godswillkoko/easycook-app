<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentMethodRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\PaymentMethod;

class PaymentMethodController extends Controller
{
    /**
     * Index and list of all payment_methods
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$payment_methods = PaymentMethod::get();

    	return view('backend.settings.payment-methods.index', compact('payment_methods'));
    }

    /**
     * Store the payment method details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(PaymentMethodRequest $request)
    {
    	PaymentMethod::create($request->all());

    	flash()->success("Payment method successfully added.");
    	return redirect()->back();
    }

    /**
     * Edit payment method details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit(PaymentMethod $payment_method)
    {
    	return view('backend.settings.payment-methods.edit', compact('payment_method'));
    }

    /**
     * Update payment method details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(PaymentMethodRequest $request, PaymentMethod $payment_method)
    {
        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
        $payment_method->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

    	$payment_method->update($request->all());

    	flash()->success("Payment method update was successful.");
    	return redirect('settings/payment-methods');
    }

    /**
     * Delete payment method details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy(PaymentMethod $payment_method)
    {
    	$payment_method->delete();

    	flash()->success("Payment method successfully deleted.");
    	return redirect()->back();
    }
}
