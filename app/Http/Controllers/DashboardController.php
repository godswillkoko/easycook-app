<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Ingredient;
use App\Recipe;
use App\Order;
use App\User;

class DashboardController extends Controller
{
    /**
     * Admin index page
     *
     * @return void
     * @author G-Factor
     **/
    public function index()
    {
        $ingredients = Ingredient::get();
        $all_recipes = Recipe::get();
        $enabled_recipes = Recipe::whereStatus(true)->get();
        $all_users = User::get();
        $orders = Order::get();
        $v_users = User::whereVerified(true)->get();

    	return view('backend.dashboard', compact('ingredients', 'all_users', 'all_recipes', 'enabled_recipes', 'v_users', 'orders'));
    }
}
