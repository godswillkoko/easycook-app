<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeliveryMethodRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\DeliveryMethod;

class DeliveryMethodController extends Controller
{
    /**
     * Index and list of all delivery methods
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$delivery_methods = DeliveryMethod::get();

    	return view('backend.settings.delivery-methods.index', compact('delivery_methods'));
    }

    /**
     * Store the delivery method details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(DeliveryMethodRequest $request)
    {
    	DeliveryMethod::create($request->all());

    	flash()->success("Delivery method successfully added.");
    	return redirect()->back();
    }

    /**
     * Edit delivery method details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit(DeliveryMethod $delivery_method)
    {
    	return view('backend.settings.delivery-methods.edit', compact('delivery_method'));
    }

    /**
     * Update delivery method details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(DeliveryMethodRequest $request, DeliveryMethod $delivery_method)
    {
        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
        $delivery_method->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }
    	$delivery_method->update($request->all());

    	flash()->success("Delivery method update was successful.");
    	return redirect('settings/delivery-methods');
    }

    /**
     * Delete delivery method details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy(DeliveryMethod $delivery_method)
    {
    	$delivery_method->delete();

    	flash()->success("Delivery method successfully deleted.");
    	return redirect()->back();
    }
}
