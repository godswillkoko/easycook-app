<?php

namespace App\Http\Controllers;

use App\Http\Requests\MeasurementRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Measurement;

class MeasurementController extends Controller
{
    /**
     * Index and list of all measurements
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$measurements = Measurement::get();

    	return view('backend.settings.measurements.index', compact('measurements'));
    }

    /**
     * Store the measurement details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(MeasurementRequest $request)
    {
    	Measurement::create($request->all());

    	flash()->success("Measurement successfully added.");
    	return redirect()->back();
    }

    /**
     * Edit measurement details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit(Measurement $measurement)
    {
    	return view('backend.settings.measurements.edit', compact('measurement'));
    }

    /**
     * Update measurement details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(MeasurementRequest $request, Measurement $measurement)
    {
        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
        $measurement->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

    	$measurement->update($request->all());

    	flash()->success("Measurement update was successful.");
    	return redirect('settings/measurements');
    }

    /**
     * Delete measurement details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy(Measurement $measurement)
    {
    	$measurement->delete();

    	flash()->success("Measurement successfully deleted");
    	return redirect()->back();
    }
}
