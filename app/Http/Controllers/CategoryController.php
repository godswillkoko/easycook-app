<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Index and list of all store-categories
     *
     * @return Response
     * @author G-Factor
     **/
    public function index()
    {
    	$categories = Category::latest()->paginate(20);

    	return view('backend.store.categories.index', compact('categories'));
    }

    /**
     * Store the store-category details
     *
     * @return Response
     * @author G-Factor
     **/
    public function store(CategoryRequest $request)
    {
    	$category = Category::create($request->all());

    	// Store image (if any)
        if($request->hasFile('image')){
            
            $this->validate($request, [
                'image' => 'mimes:jpeg,bmp,png,jpg,gif,svg'
            ]);

            $category->image_url = request()->file('image')->store('photos');
            $category->save();
        }

    	flash()->success("Store category successfully added.");
    	return redirect()->back();
    }

    /**
     * Edit store-category details
     *
     * @return Response
     * @author G-Factor
     **/
    public function edit(Category $category)
    {
    	return view('backend.store.categories.edit', compact('category'));
    }

    /**
     * Update store-category details
     *
     * @return Response
     * @author G-Factor
     **/
    public function update(CategoryRequest $request, category $category)
    {
        $checkboxes = array('status');
        foreach ($checkboxes as $cb) {
        $category->setAttribute($cb, ($request->has($cb)) ? 1 : 0);
        }

    	$category->update($request->all());

    	// Store image (if any)
        if($request->hasFile('image')){
            
            $this->validate($request, [
                'image' => 'mimes:jpeg,bmp,png,jpg,gif,svg'
            ]);

            $category->image_url = request()->file('image')->store('photos');
            $category->save();
        }

    	flash()->success("Store category update was successful.");
    	return redirect('store/categories');
    }

    /**
     * Delete store-category details
     *
     * @return Response
     * @author G-Factor
     **/
    public function destroy(Category $category)
    {
    	$category->delete();

    	flash()->success("Store category successfully deleted.");
    	return redirect()->back();
    }
}
