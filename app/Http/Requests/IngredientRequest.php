<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IngredientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2',
            'stock_status' => 'required',
            'quantity' => 'required|numeric',
            'cost' => 'required|numeric',
            'old_price' => 'numeric',
            'price' => 'numeric',
            'sales_unit' => 'numeric',
            'sales_unit_measurement_id' => 'numeric',
            'vat' => 'numeric',
            'weight' => 'numeric',
            'width' => 'numeric',
            'height' => 'numeric',
            'delivery_fee' => 'numeric',
            'minimum_order' => 'numeric|required',
            'description' => 'required|min:10',
            'status' => 'boolean'
        ];
    }
}
