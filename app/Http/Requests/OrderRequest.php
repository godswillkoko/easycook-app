<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'user_address_id' => 'required',
            'date_scheduled' => 'required|date',
            'delivery_status' => 'required',
            'payment_method_id' => 'required',
            'delivery_method_id' => 'required',
            'order_status' => 'required',
            'payment_status' => 'boolean'
        ];
    }
}
