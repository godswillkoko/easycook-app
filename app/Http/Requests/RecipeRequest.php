<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2',
            'stock_status' => 'required',
            'quantity' => 'required|numeric',
            'cost' => 'required|numeric',
            'old_price' => 'numeric',
            'price' => 'numeric',
            'sales_unit' => 'numeric',
            'sales_unit_measurement_id' => 'numeric',
            'vat' => 'numeric',
            'delivery_fee' => 'numeric',
            'minimum_order' => 'numeric|required',
            'description' => 'required|min:10',
            'require_shipping' => 'boolean'
        ];
    }
}
