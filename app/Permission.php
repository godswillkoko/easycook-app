<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use SoftDeletes;
    
    /**
     * Boot the model.
     *
     * @return void
     * @author G-Factor
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($permission) {
            $permission->name = str_slug(strtolower($permission->label));
        });

        static::updating(function ($permission) {
            $permission->name = str_slug(strtolower($permission->label));
        });
    }
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
	protected $fillable = [ 'name', 'label' ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     * @author G-Factor
     */
    public function getRouteKeyName()
    {
        return 'name';
    }
    
    /**
     * Each member belongsToMany a role
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsToMany
     * @author G-Factor
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
