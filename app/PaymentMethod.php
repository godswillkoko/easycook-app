<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
    use SoftDeletes;

    /**
     * Listen for payment method events
     *
     * @return void
     * @author G-Factor
     **/
    public static function boot()
    {
        parent::boot();

        static::creating(function($payment_methods) {
            $payment_methods->name = str_slug($payment_methods->label);
        });

        static::updating(function($payment_methods) {

            $payment_methods->name = str_slug($payment_methods->label);
        });
    }

    /**
     * Get the route key for this model
     *
     * @return void
     * @author G-Factor
     **/
    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'name',
		'label',
		'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Payment method hasOne orders
     *
     * @return Illuminate\Database\Eloquent\Relations\hasOne
     * @author G-Factor
     */
    public function order()
    {
        return $this->hasOne(Order::class);
    }

}
