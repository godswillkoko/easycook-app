<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
     * Listen for category events
     *
     * @return void
     * @author G-Factor
     **/
    public static function boot()
    {
        parent::boot();

        static::creating(function($category) {
            $category->slug = str_slug($category->label);
        });

        static::updating(function($category) {

            $category->slug = str_slug($category->label);
        });
    }

    /**
     * Get the route key for this model
     *
     * @return void
     * @author G-Factor
     **/
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
    	'slug',
		'label',
		'description',
		'image_url',
		'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Categories hasMany recipes
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     * @author G-Factor
     */
    public function recipes()
    {
        return $this->hasMany(Recipe::class);
    }

}
