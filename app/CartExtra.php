<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartExtra extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @author G-Factor
     */
    protected $fillable = [
        'user_id',
        'cart_id',
        'recipe_id',
        'extra_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id'
    ];

    public $timestamps = false;

    /**
     * CartRecipe belongsTo recipe
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function recipe()
    {
        return $this->belongsTo(Recipe::class);
    }

    /**
     * CartRecipe belongsTo cart
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    /**
     * CartRecipe belongsTo extra
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function extra()
    {
        return $this->belongsTo(Extra::class);
    }

    /**
     * CartRecipe belongsTo extra
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     * @author G-Factor
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
