@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Please correct the error(s) below.</strong>
        <br>
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif