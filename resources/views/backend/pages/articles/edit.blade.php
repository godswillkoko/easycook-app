@extends('backend.layouts.app')
@section('title') {{ $article->title }} @stop

@section('header')
	<link rel="stylesheet" href="{{ asset('backend/dist/summernote.css') }}">
@endsection

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $article->title }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($article, ['method' => 'PATCH', 'action' => ['ArticleController@update', $article->slug], 'files' => true]) }}
	                		{{ csrf_field() }}
			                <div class="form-group">
			                	{{ Form::label('title', 'Label:', ['class' => 'control-label']) }}
			                	{{ Form::text('title', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'What name would you like your customers to see?']) }}
			                </div>
			                <div>
			                	<div class="form-group">
						            {{ Form::label('image', 'Article Picture:') }}<br>
						            {{ Form::file('image') }}
						        </div>
			                	@if(isset($article->image_url))
							        <div class="images-container" id="deleteForm">
							            <div class="image-container form-group" data-toggle="dataTable" data-form="deleteForm">                        
							                {{--
							                <div class="controls">
							                    <form method="GET" action="{{ url('store/categories/image') }}/{{ $article->id }}" class="form-inline form-delete">
							                        {{ csrf_field() }}
							                        <a class="control-btn remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
							                            <i class="fa fa-trash-o "></i>
							                        </a>
							                    </form>
							                </div>
							                --}}
							                <a href="javacript:;" data-lity data-lity-target="{{ url('/') }}/{{ $article->image_url }}">
							                    <div class="image" style="background-image:url('{{ url('/') }}/{{ $article->image_url }}')"></div>
							                </a>
							            </div>
							        </div>
						        @endif
			                </div>
			                <div class="form-group">
			                	{{ Form::label('home', 'Show in home page?:') }}
							    {{ Form::checkbox('home', 1, old('status')) }}
							    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			                	{{ Form::label('status', 'Enable?:') }}
							    {{ Form::checkbox('status', 1, old('status')) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('text', 'Text:', ['class' => 'control-label']) }}
			                	{{ Form::textarea('text', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly tell your customer\'s more details about this article...']) }}
			                </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary">Save Details</button>
				            	<a href="{{ url('pages/articles') }}"><button type="button" class="btn btn-default">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop
@section('footer')
<script src="{{ asset('backend/dist/summernote.min.js') }}"></script>
<script>
	$(document).ready(function() {
        $('#wysiwug').summernote({
          height:300,
          minHeight: null,
		  maxHeight: null
        });
    });
</script>
@endsection