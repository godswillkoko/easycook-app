@extends('backend.layouts.app')

@section('title', 'Article')

@section('content')
<article class="content responsive-tables-page">
    <div class="title-block">
        <span class="pull-left">
        	<h1 class="title">Article</h1>
	        <p class="title-description"> List of all the articles</p>
	    </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-article-modal">
    		<button type="button" class="btn btn-info btn-sm">
    			<i class="fa fa-plus icon"></i> Create Article
    		</button>
    	</a>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12">
	            <div class="card">
	                <div class="card-block">
	                    <section class="example">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
	                                <thead class="flip-header">
	                                    <tr>
	                                        <th>Title</th>
	                                        <th>Homepage</th>
	                                        <th>Status</th>
	                                        <th class="text-center">Actions</th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                	@foreach($articles as $article)
	                                    <tr class="odd gradeX">
	                                        <td>{{ $article->title }}</td>
	                                        <td>
	                                        	@if($article->home == 0)
		                                        	Disabled
		                                        @else
		                                        	Enabled
		                                        @endif
	                                        </td>
	                                        <td>
	                                        	@if($article->status == 0)
		                                        	Disabled
		                                        @else
		                                        	Enabled
		                                        @endif
	                                        </td>
	                                        <td class="text-center">
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-right">
	                                        		<a class="edit" href="{{ action('ArticleController@edit', $article->slug) }}" title="Edit">
	                                                	<i class="fa fa-pencil"></i>
	                                                </a>
	                                        	</div>
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-left">
	                                        		{{ Form::model($article, ['method' => 'delete', 'action' => ['ArticleController@destroy', $article->slug], 'class' =>'form-inline form-delete']) }}
									                    {{ Form::hidden('id', $article->id) }}<a class="remove" href="javacript:void(0);" name="delete_modal" title="Delete">
		                                                	<i class="fa fa-trash-o "></i>
		                                                </a>
									                {{ Form::close() }} 
	                                        	</div>                                       
	                                        </td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                            <div class="text-center">{!! $articles->render() !!}</div>
	                        </div>
	                    </section>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
<!-- Add article modal -->
<div class="modal fade" id="add-article-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new short article</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'pages/articles', 'class' => 'form-horizontal', 'files' => true ]) }}
                	{{ csrf_field() }}
	                <div class="form-group">
	                	{{ Form::label('title', 'Title:', ['class' => 'control-label']) }}
	                	{{ Form::text('title', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Article title']) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('home', 'Show in home page?:') }}
					    {{ Form::checkbox('home', 1, old('status')) }}
					    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                	{{ Form::label('status', 'Enable?:') }}
					    {{ Form::checkbox('status', 1, old('status')) }}
	                </div>
	                <div class="form-group">
			            {{ Form::label('image', 'Article Picture:') }}<br>
			            {{ Form::file('image') }}
			        </div>
	                <div class="form-group">
	                	{{ Form::label('text', 'Description:', ['class' => 'control-label']) }}
	                	{{ Form::textarea('text', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly tell your customer\'s more details about this article...']) }}
	                </div>
	                <div class="modal-footer">
		            	<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
		            	<button type="submit" class="btn btn-primary btn-sm">Add article</button>
		            </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
	@include('includes.confirm-delete')
	<script src="{{ asset('backend/dist/summernote.min.js') }}"></script>
	<script>
		$(document).ready(function() {
	        $('#wysiwug').summernote({
	          height:300,
	          minHeight: null,
			  maxHeight: null
	        });
	    });
	</script>
@stop