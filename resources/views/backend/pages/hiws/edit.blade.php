@extends('backend.layouts.app')
@section('title') {{ $howitworks->title }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $howitworks->title }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($howitworks, ['method' => 'PATCH', 'action' => ['HowItWorksController@update', $howitworks->slug], 'files' => true]) }}
	                		{{ csrf_field() }}
			                <div class="form-group">
			                	{{ Form::label('title', 'Title:', ['class' => 'control-label']) }}
			                	{{ Form::text('title', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Title']) }}
			                </div>
			                <div>
			                	<div class="form-group">
						            {{ Form::label('image', 'Picture:') }}<br>
						            {{ Form::file('image') }}
						        </div>
			                	@if(isset($howitworks->image_url))
							        <div class="images-container" id="deleteForm">
							            <div class="image-container form-group" data-toggle="dataTable" data-form="deleteForm">
							                <a href="javacript:;" data-lity data-lity-target="{{ url('/') }}/{{ $howitworks->image_url }}">
							                    <div class="image" style="background-image:url('{{ url('/') }}/{{ $howitworks->image_url }}')"></div>
							                </a>
							            </div>
							        </div>
						        @endif
			                </div>
			                <div class="form-group">
			                	{{ Form::label('status', 'Enable?:') }}
							    {{ Form::checkbox('status', 1, old('status')) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('text', 'Text:', ['class' => 'control-label']) }}
			                	{{ Form::textarea('text', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly text...']) }}
			                </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary">Save Details</button>
				            	<a href="{{ url('pages/howitworks') }}"><button type="button" class="btn btn-default">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop