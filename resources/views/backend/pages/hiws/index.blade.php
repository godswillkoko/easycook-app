@extends('backend.layouts.app')

@section('title', 'How It Works')

@section('content')
<article class="content responsive-tables-page">
    <div class="title-block">
        <span class="pull-left">
        	<h1 class="title">How It Works</h1>
	        <p class="title-description"> List of all the how it works</p>
	    </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-hiw-modal">
    		<button type="button" class="btn btn-info btn-sm">
    			<i class="fa fa-plus icon"></i> Create How It Works
    		</button>
    	</a>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12">
	            <div class="card">
	                <div class="card-block">
	                    <section class="example">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
	                                <thead class="flip-header">
	                                    <tr>
	                                        <th>Title</th>
	                                        <th>Status</th>
	                                        <th class="text-center">Actions</th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                	@foreach($howitworks as $hiw)
	                                    <tr class="odd gradeX">
	                                        <td>{{ $hiw->title }}</td>
	                                        <td>
	                                        	@if($hiw->status == 0)
		                                        	Disabled
		                                        @else
		                                        	Enabled
		                                        @endif
	                                        </td>
	                                        <td class="text-center">
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-right">
	                                        		<a class="edit" href="{{ action('HowItWorksController@edit', $hiw->slug) }}" title="Edit">
	                                                	<i class="fa fa-pencil"></i>
	                                                </a>
	                                        	</div>
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-left">
	                                        		{{ Form::model($hiw, ['method' => 'delete', 'action' => ['HowItWorksController@destroy', $hiw->slug], 'class' =>'form-inline form-delete']) }}
									                    {{ Form::hidden('id', $hiw->id) }}<a class="remove" href="javacript:void(0);" name="delete_modal" title="Delete">
		                                                	<i class="fa fa-trash-o "></i>
		                                                </a>
									                {{ Form::close() }} 
	                                        	</div>                                       
	                                        </td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                            <div class="text-center">{!! $howitworks->render() !!}</div>
	                        </div>
	                    </section>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
<!-- Add hiw modal -->
<div class="modal fade" id="add-hiw-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new short hiw</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'pages/howitworks', 'class' => 'form-horizontal', 'files' => true ]) }}
                	{{ csrf_field() }}
	                <div class="form-group">
	                	{{ Form::label('title', 'Title:', ['class' => 'control-label']) }}
	                	{{ Form::text('title', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Title']) }}
	                </div>
	                <div class="form-group">
			            {{ Form::label('image', 'Picture:') }}<br>
			            {{ Form::file('image') }}
			        </div>
			        <div class="form-group">
	                	{{ Form::label('status', 'Enable?:') }}
					    {{ Form::checkbox('status', 1, old('status')) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('text', 'Text:', ['class' => 'control-label']) }}
	                	{{ Form::textarea('text', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly text...']) }}
	                </div>
	                <div class="modal-footer">
		            	<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
		            	<button type="submit" class="btn btn-primary btn-sm">ADD HIW</button>
		            </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
	@include('includes.confirm-delete')
@stop