@extends('backend.layouts.app')
@section('title', 'Permissions')

@section('content')
<article class="content static-tables-page">
    <div class="title-block">
    	<span class="pull-left">
        	<h1 class="title">Permissions</h1>
	        <p class="title-description"> List of all the permissions </p>
	    </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-permission-modal">
    		<button type="button" class="btn btn-primary-outline btn-sm">
    			<i class="fa fa-plus icon"></i> Create Permission
    		</button>
    	</a>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Label</th>
                                        <th>Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($permissions as $permission)
                                    <tr>
                                        <th scope="row">{{ $permission->id }}</th>
                                        <td>{{ $permission->label }}</td>
                                        <td>{{ $permission->name }}</td>
                                        <td class="tekxt-center">
                                        	<div class="col-xs-6 col-sm-6 col-md-6 l-right">
                                        		<a class="edit" href="{{ action('PermissionController@edit', $permission->name) }}">
                                                	<i class="fa fa-pencil"></i>
                                                </a>
                                        	</div>
                                        	<div class="col-xs-6 col-sm-6 col-md-6 l-left">
                                        		{{ Form::model($permission, ['method' => 'delete', 'action' => ['PermissionController@destroy', $permission->name], 'class' =>'form-inline form-delete']) }}
                                        			{{ Form::hidden('id', $permission->id) }}								                    
								                    <a class="remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
	                                                	<i class="fa fa-trash-o "></i>
	                                                </a>
								                {{ Form::close() }} 
                                        	</div>                                       
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">{!! $permissions->render() !!}</div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
<!-- Add permission modal -->
<div class="modal fade" id="add-permission-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new permission</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'user/permissions', 'class' => 'form-horizontal' ]) }}
                    {{ csrf_field() }}
	                <div class="form-group">
	                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
	                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Enter permission label...']) }}
	                </div>
	                <div class="modal-footer">
	                	<button type="submit" class="btn btn-primary-outline btn-sm">Add Permission</button>
		            	<button type="button" class="btn btn-warning-outline btn-sm" data-dismiss="modal">Close</button>
		            </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
	@include('includes.confirm-delete')
@stop