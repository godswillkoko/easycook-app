@extends('backend.layouts.app')

@section('title') {{ $permission->label }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $permission->label }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($permission, ['method' => 'PATCH', 'action' => ['PermissionController@update', $permission->name]]) }}

	                		{{ csrf_field() }}
	                		
			                <div class="form-group">
			                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
			                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Enter permission label...']) }}
			                </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary-outline">Save Details</button>
				            	<a href="{{ url('user/permissions') }}"><button type="button" class="btn btn-warning-outline">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop