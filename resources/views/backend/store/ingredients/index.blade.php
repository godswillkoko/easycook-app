@extends('backend.layouts.app')

@section('title', 'Store Ingredients')

@section('content')
<article class="content responsive-tables-page">
    <div class="title-block">
        <span class="pull-left">
        	<h1 class="title">Store Ingredients</h1>
	        <p class="title-description"> List of all the store ingredients<small>(eg: Curry, Tyme, Salt, Pepper, etc.)</small> </p>
	    </span>
        <a href="{{ url('store/ingredients/create')}}" class="pull-right">
    		<button type="button" class="btn btn-info btn-sm">
    			<i class="fa fa-plus icon"></i> Create Ingredient
    		</button>
    	</a>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12">
	            <div class="card">
	                <div class="card-block">
	                    <section class="example">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
	                                <thead class="flip-header">
	                                    <tr>
	                                        <th>Name</th>
	                                        <th>Stock Code</th>
	                                        <th>Stock Status</th>
	                                        <th>Cost(₦)</th>
	                                        <th>Price(₦)</th>
	                                        <th>Enabled</th>
	                                        <th class="text-center">Actions</th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                	@foreach($ingredients as $ing)
	                                    <tr class="odd gradeX">
	                                        <td>{{ $ing->name }}</td>
	                                        <td>{{ $ing->code }}</td>
	                                        <td>{{ $ing->stock_status }}</td>
	                                        <td>{{ number_format($ing->cost) }}</td>
	                                        <td>
	                                        	{{ number_format($ing->price) }}
	                                        	@if($ing->old_price)
	                                        		<br><span class="old-price">{{ number_format($ing->old_price) }}</span>
	                                        	@endif
	                                        </td>
	                                        <td>
	                                        	@if($ing->status == true ) Yes @else No @endif
                                        	</td>
	                                        <td class="text-center">
	                                        	<div class="col-xs-4 col-sm-4 col-md-4">
	                                        		<a class="edit" href="{{ action('IngredientController@edit', $ing->slug) }}">
	                                                	<i class="fa fa-pencil"></i>
	                                                </a>
	                                        	</div>
	                                        	<div class="col-xs-4 col-sm-4 col-md-4">
	                                        		{{ Form::model($ing, ['method' => 'delete', 'action' => ['IngredientController@destroy', $ing->slug], 'class' =>'form-inline form-delete']) }}
									                    {{ Form::hidden('id', $ing->id) }}        
									                    <a class="remove" href="javacript:void(0);" name="delete_modal">
		                                                	<i class="fa fa-trash-o "></i>
		                                                </a>
									                {{ Form::close() }} 
	                                        	</div>      
	                                        </td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                            <div class="text-center">{!! $ingredients->render() !!}</div>
	                        </div>
	                    </section>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop

@section('footer')
	@include('includes.confirm-delete')
@stop