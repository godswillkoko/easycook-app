@extends('backend.layouts.app')

@section('title') {{ $ingredient->name }} @stop

@section('header')
    {{ Html::style('plugins/lity/lity.min.css') }}
@stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $ingredient->name }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($ingredient, ['method' => 'PATCH', 'action' => ['IngredientController@update', $ingredient->slug], 'files' => true]) }}
			                @include('backend.store.ingredients._form')
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary btn-sm">Save Details</button>
				            	<a href="{{ url('store/ingredients') }}"><button type="button" class="btn btn-default btn-sm">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop

@section('footer')
    @include('includes.confirm-delete')
	@include('backend.store.ingredients._footer')
	{{ Html::script('plugins/lity/lity.min.js') }}
@stop