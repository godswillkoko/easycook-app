@extends('backend.layouts.app')

@section('title', 'Add Ingredient')

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Add New Ingredient</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::open(['url' => 'store/ingredients', 'class' => 'form-horizontal', 'files' => true ]) }}
		                	{{ csrf_field() }}
		                	
		                	@include('backend.store.ingredients._form')

			                <div class="row">
				                <div class="form-group col-xs-12 col-sm-12 col-md-12">
					            	<button type="submit" class="btn btn-primary btn-sm">Add Ingredient</button>
					            	<a href="{{ url('store/ingredients') }}"><button type="button" class="btn btn-default btn-sm">Cancel</button></a>
					            </div>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop

@section('footer')
	@include('backend.store.ingredients._footer')
@stop