<div class="row">
    <div class="form-group col-xs-12 col-sm-6 col-md-6">
        {{ Form::label('name', 'Ingredient Name:', ['class' => 'control-label']) }}
        {{ Form::text('name', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'What name would you like your customers to see?']) }}
    </div>

    <div class="form-group col-xs-12 col-sm-6 col-md-6">
    	{{ Form::label('attribute_list', 'Attributes:', ['class' => 'control-label']) }}<br>
    	{{ Form::select('attribute_list[]', $attributes, $selected_attribute, [
    		'multiple', 'class'=>'form-control js-example-responsive'
    		]) 
    	}}
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-4 col-md-4">
        {{ Form::label('quantity', 'Quantity Available:', ['class' => 'control-label']) }}
        {{ Form::text('quantity', null, ['class' => 'form-control boxed']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4">
    	{{ Form::label('stock_status', 'Stock Status:', ['class' => 'control-label']) }}<br>
    	{{ Form::select('stock_status', [
    		'In Stock' => 'In Stock', 'Pre-Order' => 'Pre-Order', 'Out Of Stock' => 'Out Of Stock', 
    		'2-3 Days' => '2-3 Days'], null, [ 'required', 'class'=>'form-control js-example-responsive'
    		]) 
    	}}
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4">
    	{{ Form::label('color', 'Color(if any):', ['class' => 'control-label']) }}
    	{{ Form::text('color', null, ['class' => 'form-control boxed']) }}
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-4 col-md-4">
    	{{ Form::label('cost', 'Cost:', ['class' => 'control-label']) }}
    	{{ Form::text('cost', null, ['class' => 'form-control boxed', 'required']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4">
    	{{ Form::label('old_price', 'Old Price(if any):', ['class' => 'control-label']) }}
    	{{ Form::text('old_price', null, ['class' => 'form-control boxed']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-4 col-md-4">
    	{{ Form::label('price', 'Price:', ['class' => 'control-label']) }}
    	{{ Form::text('price', null, ['class' => 'form-control boxed', 'required']) }}
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-6 col-md-3">
        {{ Form::label('manufacturer', 'Manufacturer(if any):', ['class' => 'control-label']) }}
        {{ Form::text('manufacturer', null, ['class' => 'form-control boxed']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-3">
        {{ Form::label('minimum_order', 'Minimum Order:', ['class' => 'control-label']) }}
        {{ Form::text('minimum_order', null, ['class' => 'form-control boxed']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-3">
        {{ Form::label('vat', 'Vat(if any):', ['class' => 'control-label']) }}
        {{ Form::text('vat', null, ['class' => 'form-control boxed']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-3">
        {{ Form::label('delivery_fee', 'Delivery Fee(if any):', ['class' => 'control-label']) }}
        {{ Form::text('delivery_fee', null, ['class' => 'form-control boxed']) }}
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12">        
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                {{ Form::label('sales_unit', 'Sales Unit:', ['class' => 'control-label']) }} 
                {{ Form::number('sales_unit', null, ['class' => 'form-control boxed unit-input', 'placeholder' => 'Enter sales unit..']) }}
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                {{ Form::label('sales_unit_measurement_id', 'Unit Class:', ['class' => 'control-label']) }}
                {{ Form::select('sales_unit_measurement_id', $measurements, null, ['class' => 'form-control boxed'])}}
            </div>
        </div>
    </div>        
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12">        
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                {{ Form::label('weight', 'Weight(Kg):', ['class' => 'control-label']) }}
                {{ Form::number('weight', null, ['class' => 'form-control boxed unit-input', 'placeholder' => 'Enter weight..']) }}
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                {{ Form::label('height', 'Height(Cm):', ['class' => 'control-label']) }} 
                {{ Form::number('height', null, ['class' => 'form-control boxed unit-input', 'placeholder' => 'Enter height...']) }}
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                {{ Form::label('width', 'Width(Cm):', ['class' => 'control-label']) }}
                {{ Form::number('width', null, ['class' => 'form-control boxed unit-input', 'placeholder' => 'Enter weight..']) }}
            </div>
        </div>
    </div>        
</div>
<div class="row">
    <div class="form-group col-xs-6 col-sm-6 col-md-6">
    	{{ Form::label('status', 'Enable:') }}
	    {{ Form::checkbox('status', 1, old('status')) }}
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{ Form::label('image', 'Ingredient Picture:') }}<br>
            {{ Form::file('image') }}
        </div>
        @if(isset($ingredient->image_url))
        <div class="images-container" id="deleteForm">
            <div class="image-container form-group" data-toggle="dataTable" data-form="deleteForm">                        
                {{--
                <div class="controls">
                    <form method="GET" action="{{ url('/charmbow/store/ingredients/image') }}/{{ $ingredient->id }}" class="form-inline form-delete">
                        {{ csrf_field() }}
                        <a class="control-btn remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
                            <i class="fa fa-trash-o "></i>
                        </a>
                    </form>
                </div>
                --}}
                <a href="javacript:;" data-lity data-lity-target="/{{ $ingredient->image_url }}">
                    <div class="image" style="background-image:url('/{{ $ingredient->image_url }}')"></div>
                </a>
            </div>
        </div>
        @endif
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12">
    	{{ Form::label('description', 'Description:', ['class' => 'control-label']) }}
    	{{ Form::textarea('description', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly tell your customer\'s more details about this ingredient...']) }}
    </div>
</div>