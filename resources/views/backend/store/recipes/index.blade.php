@extends('backend.layouts.app')

@section('title', 'Store Recipe/Meal')

@section('content')
<article class="content responsive-tables-page">
    <div class="title-block">
        <span class="pull-left">
        	<h1 class="title">Store Recipe/Meal</h1>
	        <p class="title-description"> List of all the store recipes/meals</p>
	    </span>
        <a href="{{ url('store/recipes/create')}}" class="pull-right">
    		<button type="button" class="btn btn-info btn-sm">
    			<i class="fa fa-plus icon"></i> Create Recipe
    		</button>
    	</a>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12">
	            <div class="card">
	                <div class="card-block">
	                    <section class="example">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
	                                <thead class="flip-header">
	                                    <tr>
	                                        <th>Name</th>
	                                        <th>Stock Code</th>
	                                        <th>Stock Status</th>
	                                        <th>Cost(₦)</th>
	                                        <th>Price(₦)</th>
	                                        <th>Enabled</th>
	                                        <th class="text-center">Actions</th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                	@foreach($recipes as $rec)
	                                    <tr class="odd gradeX">
	                                        <td>{{ $rec->name }}</td>
	                                        <td>{{ $rec->code }}</td>
	                                        <td>{{ $rec->stock_status }}</td>
	                                        <td>{{ number_format($rec->cost) }}</td>
	                                        <td>
	                                        	{{ number_format($rec->price) }}
	                                        	@if($rec->old_price)
	                                        		<br><span class="old-price">{{ number_format($rec->old_price) }}</span>
	                                        	@endif
	                                        </td>
	                                        <td>
	                                        	@if($rec->status == true ) Yes @else No @endif
                                        	</td>
	                                        <td class="text-center">
	                                        	<div class="col-xs-4 col-sm-4 col-md-4">
	                                        		<a class="edit" href="{{ action('RecipeController@edit', $rec->slug) }}">
	                                                	<i class="fa fa-pencil"></i>
	                                                </a>
	                                        	</div>
	                                        	<div class="col-xs-4 col-sm-4 col-md-4">
	                                        		{{ Form::model($rec, ['method' => 'delete', 'action' => ['RecipeController@destroy', $rec->slug], 'class' =>'form-inline form-delete']) }}
									                    {{ Form::hidden('id', $rec->id) }}        
									                    <a class="remove" href="javacript:void(0);" name="delete_modal">
		                                                	<i class="fa fa-trash-o "></i>
		                                                </a>
									                {{ Form::close() }} 
	                                        	</div>    
	                                        </td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                            <div class="text-center">{!! $recipes->render() !!}</div>
	                        </div>
	                    </section>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop

@section('footer')
	@include('includes.confirm-delete')
@stop