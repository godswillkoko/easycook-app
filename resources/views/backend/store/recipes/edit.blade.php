@extends('backend.layouts.app')

@section('title') {{ $recipe->name }} @stop

@section('header')
    {{ Html::style('plugins/lity/lity.min.css') }}
@stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $recipe->name }}</h1>
        <small class="error">Note: Please do not edit extras &amp; sizes, rather delete any any one you do not want &amp; add details as a new one.</small>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($recipe, ['method' => 'PATCH', 'action' => ['RecipeController@update', $recipe->slug], 'files' => true]) }}
	                		{{ csrf_field() }}
			                @include('backend.store.recipes._form')
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary btn-sm">Save Details</button>
				            	<a href="{{ url('store/recipes') }}"><button type="button" class="btn btn-default btn-sm">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop

@section('footer')
	{{ Html::script('plugins/lity/lity.min.js') }}
    @include('includes.confirm-delete')
	@include('backend.store.recipes._footer')
@stop