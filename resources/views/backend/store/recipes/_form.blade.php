<div class="row">
    <div class="form-group col-xs-12 col-sm-6 col-md-6">
        {{ Form::label('name', 'Recipe Name:', ['class' => 'control-label']) }}
        {{ Form::text('name', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'What name would you like your customers to see?']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-6">
        {{ Form::label('category_id', 'Category/Tribe:', ['class' => 'control-label']) }}
        {{ Form::select('category_id', $categories, null, ['class' => 'form-control boxed', 'required', 'id' => 'category', 'placeholder' => 'Select category/tribe']) }}
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12">
        {{ Form::label('ingredient_list', 'Food Ingredients:', ['class' => 'control-label']) }}<br>
        {{ Form::select('ingredient_list[]', $ingredients, $selected_ingredient, [
            'multiple', 'class'=>'form-control js-example-responsive', 'id' => 'ingredient']) 
        }}
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-6 col-md-6">
        {{ Form::label('quantity', 'Quantity Available:', ['class' => 'control-label']) }}
        {{ Form::number('quantity', null, ['class' => 'form-control boxed']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-6">
    	{{ Form::label('stock_status', 'Stock Status:', ['class' => 'control-label']) }}<br>
    	{{ Form::select('stock_status', [
    		'In Stock' => 'In Stock', 'Pre-Order' => 'Pre-Order', 'Out Of Stock' => 'Out Of Stock', 
    		'2-3 Days' => '2-3 Days'], null, [ 'required', 'class'=>'form-control js-example-responsive', 'id' => 'stock'
    		]) 
    	}}
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-6 col-md-6">
    	{{ Form::label('cost', 'Cost:', ['class' => 'control-label']) }}
    	{{ Form::number('cost', null, ['class' => 'form-control boxed', 'required']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-6">
    	{{ Form::label('old_price', 'Old Price(if any):', ['class' => 'control-label']) }}
    	{{ Form::number('old_price', null, ['class' => 'form-control boxed']) }}
    </div>
    {{--
    <div class="form-group col-xs-12 col-sm-4 col-md-4">
    	{{ Form::label('price', 'Price:', ['class' => 'control-label']) }}
    	{{ Form::text('price', null, ['class' => 'form-control boxed', 'required']) }}
    </div>
    --}}
</div>
<hr>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12">
        {{ Form::label('size', 'Available Sizes:', ['class' => 'control-label']) }}
        @foreach($sizes as $sz)
        <div class="row">
            <div class="col-xs-12 col-sm-2 col-md-2">
                {{ Form::number('size_number[]', $sz->size_number, ['class' => 'form-control boxed s-name-input', 'placeholder' => 'Enter size..', 'id' => 'size_name'])}}
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                {{ Form::text('size_name[]', $sz->size_name, ['class' => 'form-control boxed s-name-input', 'placeholder' => 'Enter size name..', 'id' => 'size_name'])}}
            </div>
            {{ Form::hidden('size_id', $sz->id)}}
            <div class="col-xs-12 col-sm-4 col-md-4">
                {{ Form::number('size_price[]', $sz->size_price, ['class' => 'form-control boxed size-input', 'placeholder' => 'Price for this size..', 'id' => 'size_price'])}}
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">       
                <a class="btn btn-xs btn-secondary deleteSize" data-id="{{ $sz->id }}" data-token="{{ csrf_token() }}">
                    <i class="fa fa-minus-circle "></i>
                </a>
            </div>
        </div>
        @endforeach
        <div class="s-field-wrapper row">
            <div class="col-xs-12 col-sm-2 col-md-2">
                {{ Form::number('size_number[]', null, ['class' => 'form-control boxed s-name-input', 'placeholder' => 'Enter size..', 'id' => 'size_name'])}}
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                {{ Form::text('size_name[]', null, ['class' => 'form-control boxed s-name-input', 'placeholder' => 'Enter size name..', 'id' => 'size_name'])}}
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                {{ Form::number('size_price[]', null, ['class' => 'form-control boxed size-input', 'placeholder' => 'Price for this size..', 'id' => 'size_price'])}}
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <a href="javascript:void(0);" class="s-add-button btn btn-xs btn-primary" title="Add extras">
                <i class="fa fa-plus-circle"></i></a>
            </div>
        </div>
        <div id="size"></div>
    </div>        
</div>
<hr>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12" id="deleteForm">
        {{ Form::label('extra', 'Available Extras:', ['class' => 'control-label']) }}
        @foreach($extras as $ex)
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                {{ Form::text('extra_name[]', $ex->extra_name, ['class' => 'form-control boxed e-name-input', 'placeholder' => 'Enter extra name..', 'id' => 'extra_name'])}}
            </div>
            {{ Form::hidden('extra_id', $ex->id)}}
            <div class="col-xs-12 col-sm-4 col-md-4">
                {{ Form::number('extra_price[]', $ex->extra_price, ['class' => 'form-control boxed extra-input', 'placeholder' => 'Price for this extra..', 'id' => 'extra_price'])}}
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <a class="btn btn-xs btn-secondary deleteExtra" data-id="{{ $ex->id }}" data-token="{{ csrf_token() }}">
                    <i class="fa fa-minus-circle "></i>
                </a>
            </div>
        </div>
        @endforeach
        <div class="e-field-wrapper row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                {{ Form::text('extra_name[]', null, ['class' => 'form-control boxed e-name-input', 'placeholder' => 'Enter extra name..', 'id' => 'extra_name'])}}
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                {{ Form::number('extra_price[]', null, ['class' => 'form-control boxed extra-input', 'placeholder' => 'Price for this extra..', 'id' => 'extra_price'])}}
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2">
                <a href="javascript:void(0);" class="e-add-button btn btn-xs btn-primary" title="Add extras">
                <i class="fa fa-plus-circle"></i></a>
            </div>
        </div>
        <div id="extra"></div>
    </div>        
</div>
<hr>
<div class="row">
    <div class="form-group col-xs-12 col-sm-6 col-md-4">
        {{ Form::label('chef', 'Chef name(if any):', ['class' => 'control-label']) }}
        {{ Form::text('chef', null, ['class' => 'form-control boxed']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-4">
        {{ Form::label('minimum_order', 'Minimum Order:', ['class' => 'control-label']) }}
        {{ Form::number('minimum_order', null, ['class' => 'form-control boxed']) }}
    </div>
    <div class="form-group col-xs-12 col-sm-6 col-md-4">
        {{ Form::label('weight', 'Weight(Kg):', ['class' => 'control-label']) }}
        {{ Form::number('weight', null, ['class' => 'form-control boxed unit-input', 'placeholder' => 'Enter weight..']) }}
    </div>
</div>
{{--
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12">        
        <div class="row">
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                {{ Form::label('sales_unit', 'Sales Unit:', ['class' => 'control-label']) }} 
                {{ Form::number('sales_unit', null, ['class' => 'form-control boxed unit-input', 'placeholder' => 'Enter sales unit..']) }}
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                {{ Form::label('sales_unit_measurement_id', 'Unit Class:', ['class' => 'control-label']) }}
                {{ Form::select('sales_unit_measurement_id', $measurements, null, ['class' => 'form-control boxed', 'id'=>'measure'])}}
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-4">
                {{ Form::label('delivery_fee', 'Delivery Fee(if any):', ['class' => 'control-label']) }}
                {{ Form::text('delivery_fee', null, ['class' => 'form-control boxed']) }}
            </div>
        </div>
    </div>        
</div>
--}}
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="form-group">
            {{ Form::label('image', 'Recipe Picture:') }}<br>
            {{ Form::file('image') }}
        </div>
        @if(isset($recipe->image_url))
        <div class="images-container" id="deleteForm">
            <div class="image-container form-group" data-toggle="dataTable" data-form="deleteForm">                        
                {{--
                <div class="controls">
                    <form method="GET" action="{{ url('/charmbow/store/recipes/image') }}/{{ $recipe->id }}" class="form-inline form-delete">
                        {{ csrf_field() }}
                        <a class="control-btn remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
                            <i class="fa fa-trash-o "></i>
                        </a>
                    </form>
                </div>
                --}}
                <a href="javacript:;" data-lity data-lity-target="{{ url('/') }}/{{ $recipe->image_url }}">
                    <div class="image" style="background-image:url('{{ url('/') }}/{{ $recipe->image_url }}')"></div>
                </a>
            </div>
        </div>
        @endif
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6">
        {{ Form::label('status', 'Enable:') }}
        {{ Form::checkbox('status', 1, old('status')) }}
    </div>
</div>
<hr>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12">
    	{{ Form::label('description', 'Description:', ['class' => 'control-label']) }}
    	{{ Form::textarea('description', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly tell your customer\'s more details about this recipe...']) }}
    </div>
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-12 col-md-12">
        {{ Form::label('tips', 'Extra Tips(if any):', ['class' => 'control-label']) }}
        {{ Form::textarea('tips', null, ['class' => 'form-control boxed', 'size' => '30x4', 'placeholder' => 'Are there some tips you would like to share?']) }}
    </div>
</div>