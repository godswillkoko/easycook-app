<script type="text/javascript">
	$('select').select2();

  	$(document).ready(function() {
		$('.e-add-button').click(function() {
	  	duplicateWrapper();
	  });
	  
	  var duplicateWrapper = function() {
	  	var clonedWrapper = $(".e-field-wrapper").clone();
	    clonedWrapper.removeClass("e-field-wrapper").addClass("e-recipe-wrapper");
	    
	    clonedWrapper.find(".e-name-input").change(function() {
	    	var that = this;
	    	$(that).closest(".e-recipe-wrapper").find(".extra-input").attr("extra_name", "extra_price[" + $(that).val() + "]");
	    });
	    
	    clonedWrapper.find(".e-add-button").click(function() {
	    	duplicateWrapper();
	    });
	    clonedWrapper.appendTo("#extra");
	  };
	  
	  duplicateWrapper();
	});

	$(document).ready(function() {
		$('.s-add-button').click(function() {
	  	duplicateWrapper();
	  });
	  
	  var duplicateWrapper = function() {
	  	var clonedWrapper = $(".s-field-wrapper").clone();
	    clonedWrapper.removeClass("s-field-wrapper").addClass("s-recipe-wrapper");
	    
	    clonedWrapper.find(".name-input").change(function() {
	    	var that = this;
	    	$(that).closest(".s-recipe-wrapper").find(".size-input").attr("size_name", "size_price[" + $(that).val() + "]");
	    });
	    
	    clonedWrapper.find(".s-add-button").click(function() {
	    	duplicateWrapper();
	    });
	    clonedWrapper.appendTo("#size");
	  };
	  
	  duplicateWrapper();
	});

	$(".deleteExtra").click(function(){
        var id = $(this).data("id");
        var token = $(this).data("token");
        $.ajax(
        {
            url: "/store/recipes/extra/delete/"+id,
            type: 'DELETE',
            dataType: "JSON",
            data: {
                "id": id,
                "_method": 'DELETE',
                "_token": token,
            },
            success: function ()
            {
                location.reload();
            }
        });

        location.reload();
    });

    $(".deleteSize").click(function(){
        var id = $(this).data("id");
        var token = $(this).data("token");
        $.ajax(
        {
            url: "/store/recipes/size/delete/"+id,
            type: 'DELETE',
            dataType: "JSON",
            data: {
                "id": id,
                "_method": 'DELETE',
                "_token": token,
            },
            success: function ()
            {
                location.reload();
            }
        });

        location.reload();
    });

</script>