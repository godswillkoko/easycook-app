@extends('backend.layouts.app')
@section('title') {{ $attribute->name }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $attribute->name }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($attribute, ['method' => 'PATCH', 'action' => ['CategoryController@update', $attribute->slug]]) }}
	                		{{ csrf_field() }}
			                <div class="form-group">
			                	{{ Form::label('name', 'Label:', ['class' => 'control-label']) }}
			                	{{ Form::text('name', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'What name would you like your customers to see?']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('status', 'Status:') }}
							    {{ Form::checkbox('status', 1, old('status')) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('description', 'Description:', ['class' => 'control-label']) }}
			                	{{ Form::textarea('description', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly tell your customer\'s more details about this attribute...']) }}
			                </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary">Save Details</button>
				            	<a href="{{ url('store/attributes') }}"><button type="button" class="btn btn-default">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop