@extends('backend.layouts.app')
@section('title') {{ $category->label }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $category->label }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($category, ['method' => 'PATCH', 'action' => ['CategoryController@update', $category->slug], 'files' => true]) }}
	                		{{ csrf_field() }}
			                <div class="form-group">
			                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
			                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'What name would you like your customers to see?']) }}
			                </div>
			                <div>
			                	<div class="form-group">
						            {{ Form::label('image', 'Category/Tribe Picture:') }}<br>
						            {{ Form::file('image') }}
						        </div>
			                	@if(isset($category->image_url))
							        <div class="images-container" id="deleteForm">
							            <div class="image-container form-group" data-toggle="dataTable" data-form="deleteForm">                        
							                {{--
							                <div class="controls">
							                    <form method="GET" action="{{ url('store/categories/image') }}/{{ $category->id }}" class="form-inline form-delete">
							                        {{ csrf_field() }}
							                        <a class="control-btn remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
							                            <i class="fa fa-trash-o "></i>
							                        </a>
							                    </form>
							                </div>
							                --}}
							                <a href="javacript:;" data-lity data-lity-target="{{ url('/') }}/{{ $category->image_url }}">
							                    <div class="image" style="background-image:url('{{ url('/') }}/{{ $category->image_url }}')"></div>
							                </a>
							            </div>
							        </div>
						        @endif
			                </div>
			                <div class="form-group">
			                	{{ Form::label('status', 'Status:') }}
							    {{ Form::checkbox('status', 1, old('status')) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('description', 'Description:', ['class' => 'control-label']) }}
			                	{{ Form::textarea('description', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly tell your customer\'s more details about this category...']) }}
			                </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary">Save Details</button>
				            	<a href="{{ url('store/categories') }}"><button type="button" class="btn btn-default">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop