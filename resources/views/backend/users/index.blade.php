@extends('backend.layouts.app')
@section('title', 'Users')

@section ('content')

<article class="content items-list-page">
    <div class="title-search-block">
        <div class="title-block">
            <h3 class="title"> Users </h3>
            <span class="pull-right">
                <a href="{{ url('users/create') }}" class="btn btn-primary-outline btn-sm">
                    Add New User
                </a>
            </span>
            <p class="title-description"> List of the users... </p>
        </div>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Group</th>
                                        <th>Phone</th>
                                        <th>Gender</th>
                                        <th>Reg.Date</th>
                                        <th>Email Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->group->label }}</td>
                                        <td>{{ $user->phone }}</td>
                                        <td>{{ $user->gender }}</td>
                                        <td>{{ $user->created_at->format('F j, Y') }}</td>
                                        <td>
                                            @if ($user->verified == 0)
                                                <button type="button" class="btn btn-sm btn-warning">Unconfirmed</button>
                                            @elseif ($user->suspended == 1)
                                                <button type="button" class="btn btn-sm btn-danger">Suspended</button>
                                            @elseif ($user->verified == 1)
                                                <button type="button" class="btn btn-sm btn-success">Confirmed</button>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="col-xs-6 col-sm-6 col-md-6 l-right">
                                                <a class="edit" href="{{ action('UserController@edit', $user->id) }}">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 l-left">
                                                {{ Form::model($user, ['method' => 'delete', 'action' => ['UserController@destroy', $user->id], 'class' =>'form-inline form-delete']) }}                                                   
                                                    <a class="remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
                                                        <i class="fa fa-trash-o "></i>
                                                    </a>
                                                {{ Form::close() }} 
                                            </div>                                       
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">{!! $users->render() !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@stop

@section('footer')
    @include('includes.confirm-delete')
@stop