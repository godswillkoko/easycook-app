@extends('backend.layouts.app')
@section('title') {{ $user->name }} @stop

@section('content')
    <article class="content item-editor-page">
        <div class="title-block">
            <h1 class="title">Edit: {{ $user->name }}</h1>
        </div>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                        {{ Form::model($user, ['method' => 'PATCH', 'action' => ['UserController@update', $user->id]]) }}
                            {{ csrf_field() }}
                            <div class="form-group">
                                {{ Form::label('name', 'Name:') }}
                                {{ Form::text('name', null, array('required', 'class'=>'form-control boxed')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('email', 'Email:') }}
                                {{ Form::email('email', null, array('required', 'class'=>'form-control boxed', 'disabled')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('phone', 'Phone:') }}
                                {{ Form::text('phone', null, array('class'=>'form-control boxed')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('gender', 'Gender:') }}
                                {{ Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], null, ['required', 'class'=>'form-control boxed']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('group_id', 'Group') }}                            
                                {{ Form::select('group_id', $groups, null, ['required', 'id' => 'group_id', 'class' => 'form-control boxed']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('role_id', 'Role') }}                            
                                {{ Form::select('role_id[]', $roles, $selectedRole, ['required', 'id' => 'role_id', 'class' => 'form-control boxed']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('password', 'Password') }}
                                {{ Form::password('password', ['class' => 'form-control boxed']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('password_confirmation', 'Confirm Password') }}
                                {{ Form::password('password_confirmation', ['class'=>'form-control boxed']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('about_me', 'About Me:') }}
                                {{ Form::textarea('about_me', null, ['class'=>'form-control boxed', 'size' => '30x5']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('verified', 'Verify User:') }}
                                {{ Form::select('verified', ['1' => 'Verify', '0' => 'Deny'], null, ['required', 'class'=>'form-control']) }}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary-outline">Save Details</button>
                                <a href="{{ url('users') }}"><button type="button" class="btn btn-warning-outline">Cancel</button></a>
                            </div>
                        {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@stop

@section('footer')
<script type="text/javascript">
    $('select').select2();
</script>
@endsection