<div class="col-xs-12 col-sm-6 col-md-6 form-group">
	{{ Form::label('name', 'Name:') }}
	{{ Form::text('name', null, array('required', 'class'=>'form-control')) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-6 form-group">
	{{ Form::label('email', 'Email:') }}
	{{ Form::email('email', null, array('required', 'class'=>'form-control')) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-6 form-group">
	{{ Form::label('username', 'Username:') }}
	{{ Form::text('username', null, array('required', 'class'=>'form-control')) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-6 form-group">
	{{ Form::label('phone', 'Phone:') }}
	{{ Form::text('phone', null, array('required', 'class'=>'form-control')) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-6 form-group">
	{{ Form::label('gender', 'Gender:') }}
	{{ Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], null, ['required', 'class'=>'form-control']) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-6 form-group">
    {{ Form::label('role_id', 'Role') }}                            
    {{ Form::select('role_id[]', $roles, $selectedRole, ['required', 'id' => 'role_id', 'class' => 'form-control']) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-6 form-group">
	{{ Form::label('password', 'Password') }}
	{{ Form::password('password', null, array('required', 'class'=>'form-control')) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-6 form-group">
	{{ Form::label('password_confirmation', 'Confirm Password') }}
	{{ Form::password('password_confirmation', null, array('required', 'class'=>'form-control')) }}
</div>
<div class="col-xs-12 col-sm-12 col-md-12 form-group">
	{{ Form::label('about_me', 'About Me:') }}
	{{ Form::textarea('about_me', null, ['required', 'class'=>'form-control']) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-6 form-group">
	{{ Form::label('verified', 'Verify User:') }}
	{{ Form::select('verified', ['1' => 'Verify', '0' => 'Deny'], null, ['required', 'class'=>'form-control']) }}
</div>
<div class="col-xs-12 col-sm-12 col-md-12 form-group">
	<div class="col-md-6">{{ Form::submit($submitButtonText, ['class' => 'bt btn-info form-control']) }}</div>
	<div class="col-md-6"><a href="{{ url('/users') }}" class="pull-right">Cancel</a></div>
</div>