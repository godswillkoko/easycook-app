@extends('backend.layouts.app')
@section('title') {{ $country->name }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $country->name }}</h1>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        {{ Form::model($country, ['method' => 'PATCH', 'action' => ['CountryController@update', $country->id]]) }}
                            <div class="form-group">
                                {{ Form::label('name', 'Name:') }}
                                {{ Form::text('name', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter country name...')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('iso_code_2', 'Iso_Code_2:') }}
                                {{ Form::text('iso_code_2', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter ISO Code(2 characters)...')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('iso_code_3', 'Iso_Code_3:') }}
                                {{ Form::text('iso_code_3', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter ISO Code(3 characters)...l')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('status', 'Status:') }}
                                {{ Form::checkbox('status', 1, old('status')) }}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary-outline">Save Details</button>
                                <a href="{{ url('settings/countries') }}"><button type="button" class="btn btn-warning-outline">Cancel</button></a>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@stop