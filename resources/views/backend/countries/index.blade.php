@extends('backend.layouts.app')
@section('title', 'Countries')

@section('content')

<article class="content static-tables-page">
    <div class="title-block">
        <span class="pull-left">
            <h1 class="title">Countries</h1>
            <p class="title-description"> List of all the countries... </p>
        </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-country-modal">
            <button type="button" class="btn btn-primary-outline btn-sm">
                <i class="fa fa-plus icon"></i> Add New Country
            </button>
        </a>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>ISO CODE 2</th>
                                        <th>ISO CODE 3</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($countries as $country)
                                    <tr>
                                        <th scope="row">{{ $country->id }}</th>
                                        <td>{{ $country->name }}</td>
                                        <td>{{ $country->iso_code_2 }}</td>
                                        <td>{{ $country->iso_code_3 }}</td>
                                        @if($country->status == 1)
                                            <td>Enabled</td>
                                        @else
                                            <td>Disabled</td>
                                        @endif
                                        <td class="text-center">
                                            <div class="col-xs-6 col-sm-6 col-md-6 l-right">
                                                <a class="edit" href="{{ action('CountryController@edit', $country->id) }}">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 l-left">
                                                {{ Form::model($country, ['method' => 'delete', 'action' => ['CountryController@destroy', $country->id], 'class' =>'form-inline form-delete']) }}                                              
                                                    <a class="remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
                                                        <i class="fa fa-trash-o "></i>
                                                    </a>
                                                {{ Form::close() }} 
                                            </div>                                       
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">{!! $countries->render() !!}</div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
<!-- Add country modal -->
<div class="modal fade" id="add-country-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new country</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'settings/countries', 'class' => 'form-horizontal' ]) }}
                    <div class="form-group">
                        {{ Form::label('name', 'Name:') }}
                        {{ Form::text('name', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter country name...')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('iso_code_2', 'Iso_Code_2:') }}
                        {{ Form::text('iso_code_2', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter ISO Code(2 characters)...')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('iso_code_3', 'Iso_Code_3:') }}
                        {{ Form::text('iso_code_3', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter ISO Code(3 characters)...l')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('status', 'Status:') }}
                        {{ Form::checkbox('status', 1, old('status')) }}
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary-outline btn-sm">Add country</button>
                        <button type="button" class="btn btn-warning-outline btn-sm" data-dismiss="modal">Close</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
    @include('includes.confirm-delete')
@stop