@extends('backend.layouts.app')
@section('title') {{ $role->label }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $role->label }}</h1>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        {{ Form::model($role, ['method' => 'PATCH', 'action' => ['RoleController@update', $role->name]]) }}
                            <div class="form-group">
                                {{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
                                {{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Enter role label...']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('permission_list', 'Permissions') }}                            
                                {{ Form::select('permission_list[]', $permissions, $selectedPermission, ['required', 'class' => 'form-control boxed', 'multiple']) }}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary-outline">Save Details</button>
                                <a href="{{ url('user/roles') }}"><button type="button" class="btn btn-warning-outline">Cancel</button></a>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@stop

@section('footer')
    <script type="text/javascript">
        $('select').select2();
    </script>
@stop