@extends('backend.layouts.app')
@section('title', 'Roles')

@section('content')

<article class="content static-tables-page">
    <div class="title-block">
        <span class="pull-left">
            <h1 class="title">Roles</h1>
            <p class="title-description"> List of all the roles </p>
        </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-role-modal">
            <button type="button" class="btn btn-primary-outline btn-sm">
                <i class="fa fa-plus icon"></i> Create Role
            </button>
        </a>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Label</th>
                                        <th>Name</th>
                                        <th>Permissions</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($roles as $role)
                                    <tr>
                                        <th scope="row">{{ $role->id }}</th>
                                        <td>{{ $role->label }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>
                                            @foreach($role->permissions as $rp)
                                                {{ $rp->label }}{{ $loop->remaining ? ' | ' : '' }}
                                            @endforeach
                                        </td>
                                        <td class="tekxt-center">
                                            <div class="col-xs-6 col-sm-6 col-md-6 l-right">
                                                <a class="edit" href="{{ action('RoleController@edit', $role->name) }}">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </div>
                                            <div class="col-xs-6 col-sm-6 col-md-6 l-left">
                                                {{ Form::model($role, ['method' => 'delete', 'action' => ['RoleController@destroy', $role->name], 'class' =>'form-inline form-delete']) }}
                                                    {{ Form::hidden('id', $role->id) }}                                                   
                                                    <a class="remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
                                                        <i class="fa fa-trash-o "></i>
                                                    </a>
                                                {{ Form::close() }} 
                                            </div>                                       
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">{!! $roles->render() !!}</div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
<!-- Add role modal -->
<div class="modal fade" id="add-role-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new role</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'user/roles', 'class' => 'form-horizontal' ]) }}
                    <div class="form-group">
                        {{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
                        {{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Enter role label...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('permission_list', 'Permissions') }}                            
                        {{ Form::select('permission_list[]', $permissions, null, ['required', 'class' => 'form-control boxed', 'multiple']) }}
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary-outline btn-sm">Add Role</button>
                        <button type="button" class="btn btn-warning-outline btn-sm" data-dismiss="modal">Close</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
    <script type="text/javascript">
        $('select').select2();
    </script>
    @include('includes.confirm-delete')
@stop