@extends('backend.layouts.app')
@section('title', 'Regions')

@section('content')

<article class="content static-tables-page">
    <div class="title-block">
        <span class="pull-left">
            <h1 class="title">Regions</h1>
            <p class="title-description"> List of all the regions... </p>
        </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-region-modal">
            <button type="button" class="btn btn-primary-outline btn-sm">
                <i class="fa fa-plus icon"></i> Add New Region
            </button>
        </a>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>State</th>
                                    <th>Delivery Fee</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($regions as $region)
                                <tr>
                                    <td>{{ $region->name }}</td>
                                    <td>{{ $region->state->name }}</td>
                                    <td>{{ number_format($region->region_shipping_fee) }}</td>
                                    @if($region->status == 1)
                                        <td>Enabled</td>
                                    @else
                                        <td>Disabled</td>
                                    @endif
                                    <td class="text-center">
                                        <div class="col-xs-6 col-sm-6 col-md-6 l-right">
                                            <a class="edit" href="{{ action('RegionController@edit', $region->id) }}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6 l-left">
                                            {{ Form::model($region, ['method' => 'delete', 'action' => ['RegionController@destroy', $region->id], 'class' =>'form-inline form-delete']) }}                                              
                                                <a class="remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
                                                    <i class="fa fa-trash-o "></i>
                                                </a>
                                            {{ Form::close() }} 
                                        </div>                                       
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">{!! $regions->render() !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
<!-- Add region modal -->
<div class="modal fade" id="add-region-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new region</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'settings/regions', 'class' => 'form-horizontal' ]) }}
                    <div class="form-group">
                        {{ Form::label('name', 'Name:') }}
                        {{ Form::text('name', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter region name...')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('state_id', 'State:') }}
                        {{ Form::select('state_id', $states, null, array('required', 'class'=>'form-control boxed')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('region_shipping_fee', 'Region shipping fee:') }}
                        {{ Form::number('region_shipping_fee', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Delivery fee for this region...')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('status', 'Status:') }}
                        {{ Form::checkbox('status', 1, old('status')) }}
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary-outline btn-sm">Add Region</button>
                        <button type="button" class="btn btn-warning-outline btn-sm" data-dismiss="modal">Close</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
    <script type="text/javascript">
        $('select').select2();
    </script>
    @include('includes.confirm-delete')
@stop