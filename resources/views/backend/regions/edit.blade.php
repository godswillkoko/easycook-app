@extends('backend.layouts.app')
@section('title') {{ $region->name }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $region->name }}</h1>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        {{ Form::model($region, ['method' => 'PATCH', 'action' => ['RegionController@update', $region->id]]) }}
                            <div class="form-group">
                                {{ Form::label('name', 'Name:') }}
                                {{ Form::text('name', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter region name...')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('state_id', 'State:') }}
                                {{ Form::select('state_id', $states, null, array('required', 'class'=>'form-control boxed')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('region_shipping_fee', 'Region shipping fee:') }}
                                {{ Form::number('region_shipping_fee', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Delivery fee for this region...')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('status', 'Status:') }}
                                {{ Form::checkbox('status', 1, old('status')) }}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary-outline">Save Details</button>
                                <a href="{{ url('settings/regions') }}"><button type="button" class="btn btn-warning-outline">Cancel</button></a>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@stop

@section('footer')
     <script type="text/javascript">
        $('select').select2();
    </script>
@endsection