<!-- Left side bar/menu bar -->
<aside class="sidebar">
    <div class="sidebar-container">
        <div class="sidebar-header">
            <div class="brand">
                <a href="{{ url('/') }}"> <img src="/backend/images/easycook-text-white.png"> </a>
            </div>
        </div>
        <nav class="menu">
            <ul class="nav metismenu" id="sidebar-menu">
                <li class="{{ request()->path() == '/' ? 'active' : 'n' }}">
                    <a href="{{ url('/') }}"> <i class="fa fa-home"></i> Dashboard </a>
                </li>
                <li>
                    <a href="#"> <i class="fa fa-th-large"></i> Store Manager <i class="fa arrow"></i> </a>
                    <ul>
                        <li class="{{ request()->path() == 'store/ingredients' ? 'active' : 'n' }}"> 
                            <a href="{{ url('store/ingredients') }}"> Ingredients </a> 
                        </li>

                        <li class="{{ request()->path() == 'store/recipes' ? 'active' : 'n' }}"> 
                            <a href="{{ url('store/recipes') }}"> Recipes </a> 
                        </li>
                        <li class="{{ request()->path() == 'store/categories' ? 'active' : 'n' }}"> 
                            <a href="{{ url('store/categories') }}"> Categories/Tribes </a> 
                        </li>
                    </ul>
                </li>
                <li>
                    <a href=""> <i class="fa fa-area-chart"></i> Sales <i class="fa arrow"></i> </a>
                    <ul>
                        <li> <a href="{{ url('sales/orders') }}"> Orders </a> </li>
                        <!-- <li> <a href="charts-morris.html"> Returns </a> </li> -->
                    </ul>
                </li>
                {{--
                <li>
                    <a href="forms.html"> <i class="fa fa-pencil-square-o"></i> Forms </a>
                </li>
                --}}
                <li>
                    <a href=""> <i class="fa fa-users"></i> User Management <i class="fa arrow"></i> </a>
                    <ul>
                        <li> <a href="{{ url('users') }}"> Customers </a> </li>
                        <li> <a href="{{ url('user/groups') }}"> Groups </a> </li>
                        <li> <a href="{{ url('user/roles') }}"> Roles </a> </li>
                        <li> <a href="{{ url('user/permissions') }}"> Permissions </a> </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;"><i class="fa fa-file-text"></i> Pages <i class="fa arrow"></i></a>
                    <ul>
                        <li><a href="{{ url('pages/articles') }}">Articles</a></li>
                        <li><a href="{{ url('pages/howitworks') }}">How It Works</a></li>
                    </ul>
                </li>
                <li>
                    <a href=""> <i class="fa fa-cogs"></i> Settings <i class="fa arrow"></i> </a>
                    <ul>
                        <li> <a href="{{ url('settings/payment-methods') }}"> Payment Methods </a> </li>
                        <li> <a href="{{ url('settings/delivery-methods') }}"> Delivery Methods </a> </li>
                        {{--<li> <a href="{{ url('settings/measurements') }}"> Measurements </a> </li>--}}
                        <li> <a href="{{ url('settings/delivery-timeslots') }}"> Delivery Time Slot </a> </li>
                        <hr>
                        <li> <a href="{{ url('settings/countries') }}"> Countries </a> </li>
                        <li> <a href="{{ url('settings/states') }}"> States </a> </li>
                        <li> <a href="{{ url('settings/regions') }}"> Regions </a> </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <footer class="sidebar-footer">
        <ul class="nav metismenu" id="customize-menu">
            <li>
                <a href="mailto:godswillkoko@gmail.com" target="_blank"><small>Biult For EasyCook By G-Factor</small></a>
            </li>
        </ul>
    </footer>
</aside>
<div class="sidebar-overlay" id="sidebar-overlay"></div>
<!-- /Left side bar/menu bar -->