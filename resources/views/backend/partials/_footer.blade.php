<footer class="footer">
    <div class="footer-block author">
        <ul>
            <li><small>&copy; Copyright <a href="{{ url('/') }}" target="_blank">EasyCook</a> All rights reserved.</small></li>
        </ul>
    </div>
</footer>