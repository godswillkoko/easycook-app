@extends('backend.layouts.app')

@section('title', 'Delivery Timeslot')

@section('content')
<article class="content responsive-tables-page">
    <div class="title-block">
        <span class="pull-left">
        	<h1 class="title">Delivery Timeslot</h1>
	        <p class="title-description"> List of all the delivery timeslots</p>
	    </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-timeslot-modal">
    		<button type="button" class="btn btn-info btn-sm">
    			<i class="fa fa-plus icon"></i> Create timeslot
    		</button>
    	</a>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12">
	            <div class="card">
	                <div class="card-block">
	                    <section class="example">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
	                                <thead class="flip-header">
	                                    <tr>
	                                        <th>Period</th>
	                                        <th>From</th>
	                                        <th>To</th>
	                                        <th>Day</th>
	                                        <th>Status</th>
	                                        <th class="text-center">Actions</th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                	@foreach($timeslots as $ts)
	                                    <tr class="odd gradeX">
	                                        <td>{{ $ts->period }}</td>
	                                        <td>{{ $ts->from }}</td>
	                                        <td>{{ $ts->to }}</td>
	                                        <td>{{ $ts->day }}</td>
	                                        <td>
	                                        	@if($ts->status == 0)
		                                        	Disabled
		                                        @else
		                                        	Enabled
		                                        @endif
	                                        </td>
	                                        <td class="text-center">
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-right">
	                                        		<a class="edit" href="{{ action('DeliveryTimeSlotController@edit', $ts->id) }}" title="Edit">
	                                                	<i class="fa fa-pencil"></i>
	                                                </a>
	                                        	</div>
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-left">
	                                        		{{ Form::model($ts, ['method' => 'delete', 'action' => ['DeliveryTimeSlotController@destroy', $ts->id], 'class' =>'form-inline form-delete']) }}
									                    {{ Form::hidden('id', $ts->id) }}								                    
									                    <a class="remove" href="javacript:void(0);" name="delete_modal" title="Delete">
		                                                	<i class="fa fa-trash-o "></i>
		                                                </a>
									                {{ Form::close() }} 
	                                        	</div>                                       
	                                        </td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                        </div>
	                    </section>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
<!-- Add timeslot modal -->
<div class="modal fade" id="add-timeslot-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new timeslot</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'settings/delivery-timeslots', 'class' => 'form-horizontal' ]) }}
                	{{ csrf_field() }}
	                <div class="form-group">
	                	{{ Form::label('period', 'Period:', ['class' => 'control-label']) }}
	                	{{ Form::select('period', ['Morning'=>'Morning', 'Afternoon'=>'Afternoon', 'Evening'=>'Evening'], null, ['class' => 'form-control js-example-responsive', 'required', 'placeholder' => 'Please select']) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('from', 'From:', ['class' => 'control-label']) }}
	                	{{ Form::time('from', null, ['class' => 'form-control boxed']) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('to', 'To:', ['class' => 'control-label']) }}
	                	{{ Form::time('to', null, ['class' => 'form-control boxed']) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('day', 'Day of the week:', ['class' => 'control-label']) }}
	                	{{ Form::select('day', ['Monday'=>'Monday', 'Tuesday'=>'Tuesday', 'Wednesday'=>'Wednesday', 'Thursday'=>'Thursday', 'Friday'=>'Friday', 'Saturday'=>'Saturday', 'Sunday'=>'Sunday'], null, ['class' => 'form-control js-example-responsive', 'required', 'placeholder' => 'Select day of the week...']) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('status', 'Enable?:') }}
					    {{ Form::checkbox('status', 1, old('status')) }}
	                </div>
	                <div class="modal-footer">
		            	<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
		            	<button type="submit" class="btn btn-primary btn-sm">Add Delivery Timeslot</button>
		            </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
	<script>
	  $('select').select2();
	</script>
	@include('includes.confirm-delete')
@stop