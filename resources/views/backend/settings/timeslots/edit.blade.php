@extends('backend.layouts.app')
@section('title') {{ $timeslot->period }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $timeslot->period }} {{ $timeslot->from }} - {{ $timeslot->to }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($timeslot, ['method' => 'PATCH', 'action' => ['DeliveryTimeSlotController@update', $timeslot->id]]) }}
	                		{{ csrf_field() }}
			                <div class="form-group">
			                	{{ Form::label('period', 'Period:', ['class' => 'control-label']) }}
			                	{{ Form::select('period', ['Morning'=>'Morning', 'Afternoon'=>'Afternoon', 'Evening'=>'Evening'], null, ['class' => 'form-control js-example-responsive', 'required', 'placeholder' => 'Please select']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('from', 'From:', ['class' => 'control-label']) }}
			                	{{ Form::time('from', null, ['class' => 'form-control boxed']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('to', 'To:', ['class' => 'control-label']) }}
			                	{{ Form::time('to', null, ['class' => 'form-control boxed']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('day', 'Day of the week:', ['class' => 'control-label']) }}
			                	{{ Form::select('day', ['Monday'=>'Monday', 'Tuesday'=>'Tuesday', 'Wednesday'=>'Wednesday', 'Thursday'=>'Thursday', 'Friday'=>'Friday', 'Saturday'=>'Saturday', 'Sunday'=>'Sunday'], null, ['class' => 'form-control js-example-responsive', 'required', 'placeholder' => 'Select day of the week...']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('status', 'Enable?:') }}
							    {{ Form::checkbox('status', 1, old('status')) }}
			                </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary">Save Details</button>
				            	<a href="{{ url('settings/delivery-timeslots') }}"><button type="button" class="btn btn-default">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop

@section('footer')
	<script>
	  $('select').select2();
	</script>
@endsection