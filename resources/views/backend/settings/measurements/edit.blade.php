@extends('backend.layouts.app')
@section('title') {{ $measurement->label }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $measurement->label }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($measurement, ['method' => 'PATCH', 'action' => ['MeasurementController@update', $measurement->name]]) }}
	                		{{ csrf_field() }}
			                <div class="form-group">
			                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
			                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'What name would you like your customers to see?']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('status', 'Status:') }}
							    {{ Form::checkbox('status', 1, old('status')) }}
			                </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary">Save Details</button>
				            	<a href="{{ url('settings/measurements') }}"><button type="button" class="btn btn-default">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop