@extends('backend.layouts.app')

@section('title', 'Delivery Methods')

@section('content')
<article class="content responsive-tables-page">
    <div class="title-block">
        <span class="pull-left">
        	<h1 class="title">Delivery Methods</h1>
	        <p class="title-description"> List of all the delivery methods</p>
	    </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-delivery-method-modal">
    		<button type="button" class="btn btn-info btn-sm">
    			<i class="fa fa-plus icon"></i> Create Delivery Method
    		</button>
    	</a>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12">
	            <div class="card">
	                <div class="card-block">
	                    <section class="example">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
	                                <thead class="flip-header">
	                                    <tr>
	                                        <th>Label</th>
	                                        <th>Delivery Fee</th>
	                                        <th>Status</th>
	                                        <th class="text-center">Actions</th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                	@foreach($delivery_methods as $sm)
	                                    <tr class="odd gradeX">
	                                        <td>{{ $sm->label }}</td>
	                                        <td>{{ $sm->delivery_fee }}</td>
	                                        <td>
	                                        	@if($sm->status == 0)
		                                        	Disabled
		                                        @else
		                                        	Enabled
		                                        @endif
	                                        </td>
	                                        <td class="text-center">
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-right">
	                                        		<a class="edit" href="{{ action('DeliveryMethodController@edit', $sm->name) }}" title="Edit">
	                                                	<i class="fa fa-pencil"></i>
	                                                </a>
	                                        	</div>
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-left">
	                                        		{{ Form::model($sm, ['method' => 'delete', 'action' => ['DeliveryMethodController@destroy', $sm->name], 'class' =>'form-inline form-delete']) }}
									                    {{ Form::hidden('id', $sm->id) }}								                    
									                    <a class="remove" href="javacript:void(0);" name="delete_modal" title="Delete">
		                                                	<i class="fa fa-trash-o "></i>
		                                                </a>
									                {{ Form::close() }} 
	                                        	</div>                                       
	                                        </td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                        </div>
	                    </section>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
<!-- Add delivery method modal -->
<div class="modal fade" id="add-delivery-method-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new delivery method</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'settings/delivery-methods', 'class' => 'form-horizontal' ]) }}
                	{{ csrf_field() }}
	                <div class="form-group">
	                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
	                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'What name would you like your customers to see?']) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('delivery_fee', 'Delivery Fee:', ['class' => 'control-label']) }}
	                	{{ Form::text('delivery_fee', null, ['class' => 'form-control boxed', 'required']) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('description', 'Short Description:', ['class' => 'control-label']) }}
	                	{{ Form::text('description', null, ['class' => 'form-control boxed', 'required']) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('status', 'Status:') }}
					    {{ Form::checkbox('status', 1, old('status')) }}
	                </div>
	                <div class="modal-footer">
		            	<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
		            	<button type="submit" class="btn btn-primary btn-sm">Add Delivery Method</button>
		            </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
	@include('includes.confirm-delete')
@stop