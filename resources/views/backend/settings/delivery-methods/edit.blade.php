@extends('backend.layouts.app')
@section('title') {{ $delivery_method->label }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $delivery_method->label }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($delivery_method, ['method' => 'PATCH', 'action' => ['DeliveryMethodController@update', $delivery_method->name]]) }}
	                		{{ csrf_field() }}
			                <div class="form-group">
			                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
			                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'What name would you like your customers to see?']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('delivery_fee', 'Shipping Fee:', ['class' => 'control-label']) }}
			                	{{ Form::text('delivery_fee', null, ['class' => 'form-control boxed', 'required']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('description', 'Short Description:', ['class' => 'control-label']) }}
			                	{{ Form::text('description', null, ['class' => 'form-control boxed', 'required']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('status', 'Status:') }}
							    {{ Form::checkbox('status', 1, old('status')) }}
			                </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary">Save Details</button>
				            	<a href="{{ url('settings/delivery-methods') }}"><button type="button" class="btn btn-default">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop