@extends('backend.layouts.app')

@section('title', 'Store recipe extras')

@section('content')
<article class="content responsive-tables-page">
    <div class="title-block">
        <span class="pull-left">
        	<h1 class="title">Store recipe extras</h1>
	        <p class="title-description"> List of all the store recipe extras</p>
	    </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-size-modal">
    		<button type="button" class="btn btn-info btn-sm">
    			<i class="fa fa-plus icon"></i> Create Recipe Extra
    		</button>
    	</a>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12">
	            <div class="card">
	                <div class="card-block">
	                    <section class="example">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
	                                <thead class="flip-header">
	                                    <tr>
	                                        <th>Label</th>
	                                        <th>Description</th>
	                                        <th>Status</th>
	                                        <th class="text-center">Actions</th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                	@foreach($extras as $extra)
	                                    <tr class="odd gradeX">
	                                        <td>{{ $extra->label }}</td>
	                                        <td>{{ $extra->description }}</td>
	                                        <td>
	                                        	@if($extra->status == 0)
		                                        	Disabled
		                                        @else
		                                        	Enabled
		                                        @endif
	                                        </td>
	                                        <td class="text-center">
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-right">
	                                        		<a class="edit" href="{{ action('ExtraController@edit', $extra->slug) }}" title="Edit">
	                                                	<i class="fa fa-pencil"></i>
	                                                </a>
	                                        	</div>
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-left">
	                                        		{{ Form::model($extra, ['method' => 'delete', 'action' => ['ExtraController@destroy', $extra->slug], 'class' =>'form-inline form-delete']) }}
									                    {{ Form::hidden('id', $extra->id) }}								                    
									                    <a class="remove" href="javacript:void(0);" name="delete_modal" title="Delete">
		                                                	<i class="fa fa-trash-o "></i>
		                                                </a>
									                {{ Form::close() }} 
	                                        	</div>                                       
	                                        </td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                        </div>
	                    </section>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
<!-- Add recipe-size modal -->
<div class="modal fade" id="add-size-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new recipe size</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'settings/extras', 'class' => 'form-horizontal' ]) }}
                	{{ csrf_field() }}
	                <div class="form-group">
	                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
	                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Eg: Extra Protein, Extra sauce, Extra chips, etc...']) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('description', 'Description:', ['class' => 'control-label']) }}
	                	{{ Form::textarea('description', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly description...']) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('status', 'Status:') }}
					    {{ Form::checkbox('status', 1, old('status')) }}
	                </div>
	                <div class="modal-footer">
		            	<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
		            	<button type="submit" class="btn btn-primary btn-sm">Add Recipe Extra</button>
		            </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
	@include('includes.confirm-delete')
@stop