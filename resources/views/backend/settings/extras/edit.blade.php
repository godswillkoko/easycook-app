@extends('backend.layouts.app')
@section('title') {{ $extra->label }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $extra->label }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($extra, ['method' => 'PATCH', 'action' => ['ExtraController@update', $extra->slug]]) }}
	                		{{ csrf_field() }}
			                <div class="form-group">
			                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
			                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Eg: Extra Protein, Extra sauce, Extra chips, etc...']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('description', 'Description:', ['class' => 'control-label']) }}
			                	{{ Form::textarea('description', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly description...']) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('status', 'Status:') }}
							    {{ Form::checkbox('status', 1, old('status')) }}
			                </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary">Save Details</button>
				            	<a href="{{ url('settings/extras') }}"><button type="button" class="btn btn-default">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop

@section('footer')
	<script type="text/javascript">
	  	$('select').select2();
	</script>
@stop