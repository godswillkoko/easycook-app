@extends('backend.layouts.app')
@section('title') {{ $state->name }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $state->name }}</h1>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        {{ Form::model($state, ['method' => 'PATCH', 'action' => ['StateController@update', $state->id]]) }}
                            <div class="form-group">
                                {{ Form::label('name', 'Name:') }}
                                {{ Form::text('name', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter state name...')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('country_id', 'Country:') }}
                                {{ Form::select('country_id', $countries, null, array('required', 'class'=>'form-control boxed')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('code', 'State Code:') }}
                                {{ Form::text('code', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter State Code(3 characters)...l')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('status', 'Status:') }}
                                {{ Form::checkbox('status', 1, old('status')) }}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary-outline">Save Details</button>
                                <a href="{{ url('settings/states') }}"><button type="button" class="btn btn-warning-outline">Cancel</button></a>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@stop