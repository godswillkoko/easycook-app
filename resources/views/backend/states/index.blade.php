@extends('backend.layouts.app')
@section('title', 'States')

@section('content')

<article class="content static-tables-page">
    <div class="title-block">
        <span class="pull-left">
            <h1 class="title">States</h1>
            <p class="title-description"> List of all the states... </p>
        </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-state-modal">
            <button type="button" class="btn btn-primary-outline btn-sm">
                <i class="fa fa-plus icon"></i> Add New State
            </button>
        </a>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Country</th>
                                    <th>CODE</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($states as $state)
                                <tr>
                                    <th scope="row">{{ $state->id }}</th>
                                    <td>{{ $state->name }}</td>
                                    <td>{{ $state->country->name }}</td>
                                    <td>{{ $state->code }}</td>
                                    @if($state->status == 1)
                                        <td>Enabled</td>
                                    @else
                                        <td>Disabled</td>
                                    @endif
                                    <td class="text-center">
                                        <div class="col-xs-6 col-sm-6 col-md-6 l-right">
                                            <a class="edit" href="{{ action('StateController@edit', $state->id) }}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6 l-left">
                                            {{ Form::model($state, ['method' => 'delete', 'action' => ['StateController@destroy', $state->id], 'class' =>'form-inline form-delete']) }}                                              
                                                <a class="remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
                                                    <i class="fa fa-trash-o "></i>
                                                </a>
                                            {{ Form::close() }} 
                                        </div>                                       
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">{!! $states->render() !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
<!-- Add state modal -->
<div class="modal fade" id="add-state-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new state</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'settings/states', 'class' => 'form-horizontal' ]) }}
                    <div class="form-group">
                        {{ Form::label('name', 'Name:') }}
                        {{ Form::text('name', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter state name...')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('country_id', 'Country:') }}
                        {{ Form::select('country_id', $countries, null, array('required', 'class'=>'form-control boxed')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('code', 'State Code:') }}
                        {{ Form::text('code', null, array('required', 'class'=>'form-control boxed', 'placeholder'=>'Enter State Code(3 characters)...l')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('status', 'Status:') }}
                        {{ Form::checkbox('status', 1, old('status')) }}
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary-outline btn-sm">Add state</button>
                        <button type="button" class="btn btn-warning-outline btn-sm" data-dismiss="modal">Close</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
    @include('includes.confirm-delete')
@stop