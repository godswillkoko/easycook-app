<!DOCTYPE html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>@yield('title') - EasyCook</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="{{ asset('backend/css/vendor.css') }}">
        <link rel="stylesheet" href="{{ asset('backend/css/app-orange.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
        @yield('header')
    </head>

    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
                @include('backend.partials._header')
                @include('backend.partials._left_menu')                
                @include('flash::message')
                @include('errors.form') 
                @yield('content')

                @include('backend.partials._footer')
            </div>
        </div>
        <script src="{{ asset('plugins/jquery/jquery-3.1.0.min.js') }}"></script>
        <script src="{{ asset('backend/js/vendor.js') }}"></script>
        <script src="{{ asset('backend/js/app.js') }}"></script>
        <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
        <!-- sliddeUp alert message -->
        <script>            
            $('div.alert').not('.alert-danger').delay(10000).slideUp(300);
            $('div.alert-warning').delay(100000).slideUp(300);
        </script>
        @yield('footer')
    </body>
</html>