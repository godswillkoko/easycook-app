@extends('backend.layouts.app')
@section('title', 'Groups')

@section('content')
<article class="content static-tables-page">
    <div class="title-block">
    	<span class="pull-left">
        	<h1 class="title">Groups</h1>
	        <p class="title-description"> List of all the groups </p>
	    </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-group-modal">
    		<button type="button" class="btn btn-primary-outline btn-sm">
    			<i class="fa fa-plus icon"></i> Create group
    		</button>
    	</a>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Label</th>
                                        <th>Color</th>
                                        <th>Qualification Amount(NGN)</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($groups as $group)
                                    <tr>
                                        <th scope="row">{{ $group->id }}</th>
                                        <td>{{ $group->label }}</td>
                                        <td>{{ $group->theme_color }}</td>
                                        <td>{{ number_format($group->minimum_amount_required) }}</td>
                                        <td>{{ $group->description }}</td>
                                        <td class="tekxt-center">
                                        	<div class="col-xs-6 col-sm-6 col-md-6 l-right">
                                        		<a class="edit" href="{{ action('GroupController@edit', $group->name) }}">
                                                	<i class="fa fa-pencil"></i>
                                                </a>
                                        	</div>
                                        	<div class="col-xs-6 col-sm-6 col-md-6 l-left">
                                        		{{ Form::model($group, ['method' => 'delete', 'action' => ['GroupController@destroy', $group->name], 'class' =>'form-inline form-delete']) }}
                                        			{{ Form::hidden('id', $group->id) }}								                    
								                    <a class="remove" href="javacript:void(0);" name="delete_modal" class="form-delete">
	                                                	<i class="fa fa-trash-o "></i>
	                                                </a>
								                {{ Form::close() }} 
                                        	</div>                                       
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="text-center">{!! $groups->render() !!}</div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
<!-- Add group modal -->
<div class="modal fade" id="add-group-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new group</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'user/groups', 'class' => 'form-horizontal' ]) }}
                    {{ csrf_field() }}
	                <div class="form-group">
	                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
	                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Enter group label...']) }}
	                </div>
                    <div class="form-group">
                        {{ Form::label('theme_color', 'Color:', ['class' => 'control-label']) }}
                        {{ Form::text('theme_color', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Group color...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('minimum_amount_required', 'Minimum Qualification Amount:', ['class' => 'control-label']) }}
                        {{ Form::number('minimum_amount_required', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Group color...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('description', 'Description:', ['class' => 'control-label']) }}
                        {{ Form::textarea('description', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly description...']) }}
                    </div>
	                <div class="modal-footer">
	                	<button type="submit" class="btn btn-primary-outline btn-sm">Add Group</button>
		            	<button type="button" class="btn btn-warning-outline btn-sm" data-dismiss="modal">Close</button>
		            </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
	@include('includes.confirm-delete')
@stop