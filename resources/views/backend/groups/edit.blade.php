@extends('backend.layouts.app')

@section('title') {{ $group->label }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $group->label }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($group, ['method' => 'PATCH', 'action' => ['GroupController@update', $group->name]]) }}

	                		{{ csrf_field() }}
	                		
			                <div class="form-group">
			                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
			                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Enter group label...']) }}
			                </div>
		                    <div class="form-group">
		                        {{ Form::label('theme_color', 'Color:', ['class' => 'control-label']) }}
		                        {{ Form::text('theme_color', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Group color...']) }}
		                    </div>
		                    <div class="form-group">
		                        {{ Form::label('minimum_amount_required', 'Minimum Qualification Amount:', ['class' => 'control-label']) }}
		                        {{ Form::number('minimum_amount_required', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'Group color...']) }}
		                    </div>
		                    <div class="form-group">
		                        {{ Form::label('description', 'Description:', ['class' => 'control-label']) }}
		                        {{ Form::textarea('description', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly description...']) }}
		                    </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary-outline">Save Details</button>
				            	<a href="{{ url('user/groups') }}"><button type="button" class="btn btn-warning-outline">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop