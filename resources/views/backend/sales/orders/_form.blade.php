<div class="col col-xs-12 col-sm-12 col-md-12 col-xl-12">
    <div class="card sameheight-item" data-exclude="xs">
        <div class="card-header card-header-sm bordered">
            <div class="header-block">
                <h3 class="title">Order</h3> </div>
            <ul class="nav nav-tabs pull-right" role="tablist">
                <li class="nav-item"> <a class="nav-link active" href="#details" role="tab" data-toggle="tab">Details</a> </li>
                <li class="nav-item"> <a class="nav-link" href="#recipes" role="tab" data-toggle="tab">Recipes</a> </li>
            </ul>
        </div>
        <div class="card-block">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active fade in" id="details">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-6 col-md-6">
                            {{ Form::label('user_id', 'Customers:', ['class' => 'control-label']) }}
                            {{ Form::select('user_id', $customers, null, [
                                'required', 'class'=>'form-control js-example-responsive', 'id' => 'customer', 'placeholder' => 'Select customer...']) }}
                        </div>
                        <div class="form-group col-xs-12 col-sm-6 col-md-6">
                            {{ Form::label('user_address_id', 'Customer Address:', ['class' => 'control-label']) }}
                            {{ Form::select('user_address_id', $addresses, $selectedAddress, [
                            'required', 'class'=>'form-control js-example-responsive', 'id' => 'user_address', 'placeholder' => 'Select customer first...']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4">
                            {{ Form::label('date_scheduled', 'Delivery Date:', ['class' => 'control-label'])}}
                            {{ Form::text('date_scheduled', null, ['class' => 'datepicker form-control boxed', 'data-provide' => 'datepicker']) }}
                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4">
                            {{ Form::label('delivery_status', 'Delivery Status:', ['class' => 'control-label'])}}
                            {{ Form::select('delivery_status', ['pending' => 'Pending', 'delivered' => 'Delivered', 'not delivered' => 'Not Delivered'], null, ['id'=>'delivery_stat']) }}
                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4">
                            {{ Form::label('order_status', 'Order Status:', ['class' => 'control-label'])}}
                            {{ Form::select('order_status', ['pending' => 'Pending', 'prcessed' => 'Processed', 'hold' => 'Hold', 'canceled' => 'Canceled', 'closed' => 'Closed'], null, ['id'=>'order_stat']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-4 col-md-4">
                            {{ Form::label('payment_method_id', 'Payment Method:', ['class' => 'control-label'])}}
                            {{ Form::select('payment_method_id', $payment_methods, null, ['required', 'class'=>'form-control js-example-responsive', 'id'=>'pay_method']) }}
                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4">
                            {{ Form::label('delivery_method_id', 'Delivery Method:', ['class' => 'control-label'])}}
                            {{ Form::select('delivery_method_id', $delivery_methods, null, ['required', 'class'=>'form-control js-example-responsive', 'id'=>'ship_method']) }}
                        </div>
                        <div class="form-group col-xs-12 col-sm-4 col-md-4">
                            {{ Form::label('payment_method_id', 'Payment Status(Check if paid):', ['class' => 'control-label'])}}<br>
                            {{ Form::checkbox('payment_status', 1, old('status')) }}
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-12 col-md-12">
                            {{ Form::label('comments', 'Customer Comment(if any):', ['class' => 'control-label']) }}
                            {{ Form::textarea('comments', null, ['class' => 'form-control boxed', 'size' => '30x4', 'placeholder' => 'Customers additional comment...']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-12 col-md-12">
                            {{ Form::label('note', 'Admin Comment(if any):', ['class' => 'control-label']) }}
                            {{ Form::textarea('note', null, ['class' => 'form-control boxed', 'size' => '30x4', 'placeholder' => 'Do you as am admin have any comment regarding this order?']) }}
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="recipes">
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-12 col-md-12">
                            {{ Form::label('recipes', 'Recipes:', ['class' => 'control-label']) }}
                            @if(isset($order))
                                @foreach($order->order_recipes as $or)
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                        {{ Form::select('recipe_id[]', $recipes, $or->recipe_id, ['class'=>'form-control boxed recipe-select o-select', 'id' => 'recipe1']) }}
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                        {{ Form::select('size_id[]', $sizes, $or->size_id, [
                                            'class'=>'form-control boxed o-select', 'id' => 'size1',
                                            'placeholder' => 'Select recipe first...'
                                            ])
                                        }}
                                    </div>
                                    <div class="col-xs-6 col-sm-2 col-md-2">
                                        {{ Form::number('recipe_quantity[]', $or->recipe_quantity, ['class' => 'form-control boxed quantity-input', 'placeholder' => 'Enter quantity...', 'id' => 'quantity1']) }}
                                    </div>
                                    <div class="col-xs-6 col-sm-2 col-md-2">
                                        <a class="btn btn-xs btn-secondary deleteRecipe" data-id="{{ $or->id }}" data-token="{{ csrf_token() }}">
                                            <i class="fa fa-minus-circle "></i>
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                            @endif
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::select('recipe_id[]', $recipes, $selectedRecipes, ['class'=>'form-control boxed recipe-select o-select', 'id' => 'recipe1']) }}
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::select('size_id[]', $sizes, $selectedSizes, [
                                        'class'=>'form-control boxed o-select', 'id' => 'size1',
                                        'placeholder' => 'Select recipe first...'
                                        ])
                                    }}
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::number('recipe_quantity[]', null, ['class' => 'form-control boxed quantity-input', 'placeholder' => 'Enter quantity...', 'id' => 'quantity1']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::select('recipe_id[]', $recipes, $selectedRecipes, ['class'=>'form-control boxed recipe-select o-select', 'id' => 'recipe2']) }}
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::select('size_id[]', $sizes, $selectedSizes, [
                                        'class'=>'form-control boxed o-select', 'id' => 'size2',
                                        'placeholder' => 'Select recipe first...'
                                        ])
                                    }}
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::number('recipe_quantity[]', null, ['class' => 'form-control boxed quantity-input', 'placeholder' => 'Enter quantity...', 'id' => 'quantity2']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::select('recipe_id[]', $recipes, $selectedRecipes, ['class'=>'form-control boxed recipe-select o-select', 'id' => 'recipe3']) }}
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::select('size_id[]', $sizes, $selectedSizes, [
                                        'class'=>'form-control boxed o-select', 'id' => 'size3',
                                        'placeholder' => 'Select recipe first...'
                                        ])
                                    }}
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::number('recipe_quantity[]', null, ['class' => 'form-control boxed quantity-input', 'placeholder' => 'Enter quantity...', 'id' => 'quantity3']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::select('recipe_id[]', $recipes, $selectedRecipes, ['class'=>'form-control boxed recipe-select o-select', 'id' => 'recipe4']) }}
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::select('size_id[]', $sizes, $selectedSizes, [
                                        'class'=>'form-control boxed o-select', 'id' => 'size4',
                                        'placeholder' => 'Select recipe first...'
                                        ])
                                    }}
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::number('recipe_quantity[]', null, ['class' => 'form-control boxed quantity-input', 'placeholder' => 'Enter quantity...', 'id' => 'quantity4']) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::select('recipe_id[]', $recipes, $selectedRecipes, ['class'=>'form-control boxed recipe-select o-select', 'id' => 'recipe5']) }}
                                </div>
                                <div class="form-group col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::select('size_id[]', $sizes, $selectedSizes, [
                                        'class'=>'form-control boxed o-select', 'id' => 'size5',
                                        'placeholder' => 'Select recipe first...'
                                        ])
                                    }}
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    {{ Form::number('recipe_quantity[]', null, ['class' => 'form-control boxed quantity-input', 'placeholder' => 'Enter quantity...', 'id' => 'quantity5']) }}
                                </div>
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>