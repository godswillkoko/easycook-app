@extends('backend.layouts.app')

@section('title') {{ $order->invoice_no }} @stop

@section('header')
	{{Html::style('backend/css/bootstrap-datepicker.min.css')}}
@endsection

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $order->invoice_no }}</h1>
        <small class="error">Note: Please do not edit recipe, rather delete any recipe &amp; add details as a new one.</small>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($order, ['method' => 'PATCH', 'action' => ['OrderController@update', $order->id]]) }}
	                		{{ csrf_field() }}
			                @include('backend.sales.orders._form')
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary">Save Details</button>
				            	<a href="{{ url('sales/orders') }}"><button type="button" class="btn btn-default">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop

@section('footer')
	@include('backend.sales.orders._footer')
@stop