@extends('backend.layouts.app')

@section('title', 'Orders')

@section('content')
<article class="content responsive-tables-page">
    <div class="title-block">
        <span class="pull-left">
        	<h1 class="title">Sales orders</h1>
	        <p class="title-description"> List of all the orders</p>
	    </span>
        <a href="{{ url('sales/orders/create')}}" class="pull-right">
    		<button type="button" class="btn btn-info btn-sm">
    			<i class="fa fa-plus icon"></i> Create Order
    		</button>
    	</a>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12">
	            <div class="card">
	                <div class="card-block">
	                    <section class="example">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
	                                <thead class="flip-header">
	                                    <tr>
	                                        <th>Invoice No</th>
	                                        <th>Customer</th>
	                                        <th>Delivery Status</th>
	                                        <th>Grand Total</th>
	                                        <th>Order Status</th>
	                                        <th>Order Time</th>
	                                        <th class="text-center">Actions</th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                	@foreach($orders as $order)
	                                    <tr class="odd gradeX">
	                                        <td>{{ $order->invoice_no }}</td>
	                                        <td>{{ $order->user->name }}</td>
	                                        <td>{{ $order->delivery_status }}</td>
	                                        <td>{{ number_format($order->total) }}</td>
	                                        <td>{{ $order->order_status }}</td>
	                                        <td>{{ $order->created_at->diffForHumans() }}</td>
	                                        <td class="text-center">
	                                        	<div class="col-xs-4 col-sm-4 col-md-4">
	                                        		<a class="show" href="#{{ action('OrderController@show', $order->id) }}" title="View">
	                                                	<i class="fa fa-eye"></i>
	                                                </a>
	                                        	</div>
	                                        	<div class="col-xs-4 col-sm-4 col-md-4">
	                                        		<a class="edit" href="{{ action('OrderController@edit', $order->id) }}" title="Edit">
	                                                	<i class="fa fa-pencil"></i>
	                                                </a>
	                                        	</div>
	                                        	<div class="col-xs-4 col-sm-4 col-md-4">
	                                        		{{ Form::model($order, ['method' => 'delete', 'action' => ['OrderController@destroy', $order->id], 'class' =>'form-inline form-delete']) }}
									                    {{ Form::hidden('id', $order->id) }}								                    
									                    <a class="remove" href="javacript:void(0);" name="delete_modal" title="Delete">
		                                                	<i class="fa fa-trash-o "></i>
		                                                </a>
									                {{ Form::close() }} 
	                                        	</div>                                       
	                                        </td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                        </div>
	                    </section>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop

@section('footer')
	@include('includes.confirm-delete')
@stop