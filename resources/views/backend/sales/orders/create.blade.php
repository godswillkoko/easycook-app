@extends('backend.layouts.app')

@section('title', 'Add Order')

@section('header')
	{{Html::style('backend/css/bootstrap-datepicker.min.css')}}
@endsection

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Add New Order</h1>
    </div>
	<section class="section">
        <div class="card">
            <div class="card-block">
            	{{ Form::open(['url' => 'sales/orders', 'class' => 'form-horizontal' ]) }}
                	{{ csrf_field() }}
                	
                	@include('backend.sales.orders._form')

	                <div class="row">
		                <div class="form-group col-xs-12 col-sm-12 col-md-12">
			            	<button type="submit" class="btn btn-primary">Place Order</button>
			            	<a href="{{ url('sales/orders') }}"><button type="button" class="btn btn-default">Cancel</button></a>
			            </div>
		            </div>
                {{ Form::close() }}
            </div>
        </div>
	</section>
</article>
@stop

@section('footer')
	@include('backend.sales.orders._footer')
@stop