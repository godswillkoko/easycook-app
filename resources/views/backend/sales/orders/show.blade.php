@extends('backend.layouts.app')

@section('title') Order - #{{ $order->id }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Order: #{{ $order->id }}</h1>
        <div class="pull-right">
        	<a href="{{ url('sales/orders') }}"><i class="fa fa-return"></i>Return to list</a>
        </div>
    </div>
	<section class="section">
		<div class="row">
		    <div class="col col-xs-12 col-sm-12 col-md-12 history-col">
	            <div class="card sameheight-item" data-exclude="xs">
	                <div class="card-header card-header-sm bordered">          
	                    <ul class="nav nav-tabs pull-left" role="tablist">
	                        <li class="nav-item">
	                        	<a class="nav-link active" href="#order" role="tab" data-toggle="tab">Order Details</a>
	                        </li>
	                        <li class="nav-item">
	                        	<a class="nav-link" href="#products" role="tab" data-toggle="tab">Products</a>
	                        </li>
	                        <li class="nav-item">
	                        	<a class="nav-link" href="#payments" role="tab" data-toggle="tab">Payment Details</a>
	                        </li>
	                        <li class="nav-item">
	                        	<a class="nav-link" href="#shippings" role="tab" data-toggle="tab">Shipping Details</a>
	                        </li>
	                    </ul>
	                </div>
	                <div class="card-block">
	                    <div class="tab-content">
	                        <div role="tabpanel" class="tab-pane active fade in" id="order">
		                        <div class="table-responsive">
		                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" data-form="deleteForm">
			                            <tbody>
				                        	<tr class="odd gradeX">
			                                    <th>Order ID</th>
			                                    <td>#{{ $order->id }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Invoice No.</th>
			                                    <td>{{ $order->invoice_no }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Customer</th>
			                                    <td class="capitalize">{{ $order->user_fullname }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Customer Group</th>
			                                    <td>{{ $orm_order->user->group->label }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Customer Email</th>
			                                    <td>{{ $order->email }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Customer Phone</th>
			                                    <td>{{ $order->phone }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Order Total Amount</th>
			                                    <td>{{ number_format($order->total) }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Created</th>
			                                    <td>{{ $orm_order->created_at->format('F j, Y') }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Updated</th>
			                                    <td>{{ $orm_order->updated_at->format('F j, Y') }}</td>
			                                </tr>
			                            </tbody>
			                        </table>
		                        </div>
	                        </div>
	                        <div role="tabpanel" class="tab-pane fade" id="products">
	                        	<div class="table-responsive">
		                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" data-form="deleteForm">
			                            <thead class="flip-header">
		                                    <tr>
		                                        <th>Product</th>
		                                        <th>Stock Code</th>
		                                        <th>Quantity</th>
		                                        <th>Unit Price</th>
		                                        <th>Vat</th>
		                                        <th>Delivery Fee</th>
		                                        <th>Total</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
		                                	@foreach($orm_order->products as $od)
		                                    <tr class="odd gradeX">
		                                        <td>{{ $od->name }}</td>
		                                        <td>{{ $od->code }}</td>
		                                        <td>{{ $od->pivot->quantity }}</td>	
		                                        <td>{{ number_format($od->price) }}</td>
		                                        <td>{{ number_format($od->vat) }}</td>
		                                        <td>{{ number_format($od->delivery_fee) }}</td>
		                                        <td>{{ number_format(($od->price + $od->vat + $od->delivery_fee)  * $od->pivot->quantity) }}</td>
		                                    </tr>
		                                    @endforeach
		                                    <tr>
		                                    	<td colspan="6"><b class="pull-right">Grand Total</b></td>
		                                    	<td><b>{{ number_format($order->total) }}</b></td>
		                                    </tr>
		                                </tbody>
			                        </table>
		                        </div>
	                        </div>
	                        <div role="tabpanel" class="tab-pane fade" id="payments">
	                        </div>
	                        <div role="tabpanel" class="tab-pane fade" id="shippings">
	                        	<div class="table-responsive">
		                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" data-form="deleteForm">
			                            <tbody>
				                        	<tr class="odd gradeX">
			                                    <th>Order ID</th>
			                                    <td>#{{ $order->id }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Customer</th>
			                                    <td class="capitalize">{{ $order->user_fullname }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Address</th>
			                                    <td>{{ $order->address }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>City</th>
			                                    <td>{{ $order->city }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>State</th>
			                                    <td>{{ $order->state }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Country</th>
			                                    <td>{{ $order->country }}</td>
			                                </tr>
			                                <tr class="odd gradeX">
			                                    <th>Delivery Date</th>
			                                    <td>{{ $orm_order->date_scheduled->format('F j, Y') }}</td>
			                                </tr>
			                            </tbody>
			                        </table>
		                        </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
        </div>
	</section>
</article>
@stop