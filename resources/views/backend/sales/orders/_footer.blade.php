{{Html::script('/backend/js/bootstrap-datepicker.min.js')}}
@include('includes.customer-address')

<script type="text/javascript">
  	$('select').select2();

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: '-3d'
    });

	/*var counter = 10;
	var limit = 3;
	function addInput(divName){
	    if (counter == limit)  {
	        alert("You have reached the limit of adding " + counter + " inputs");
	    } else {
	        var newdiv = document.createElement('div');
	        newdiv.innerHTML = '<div class="row">Recipe ' + (counter + 1) + ' <br><div class="form-group col-xs-12 col-sm-4 col-md-4">{{ Form::select("recipe_id[]", $recipes, $selectedRecipes, ["required", "class"=>"form-control boxed recipe-select o-select", "id"=>"recipe"]) }}</div><div class="form-group col-xs-12 col-sm-4 col-md-4">{{ Form::select("recipe_size_id[]", $sizes, $selectedSizes, ["required", "class"=>"form-control boxed o-select", "placeholder" => "Select recipe first...", "id"=>"size"]) }}</div><div class="col-xs-12 col-sm-4 col-md-4">{{ Form::number("recipe_quantity[]", null, ["class" => "form-control boxed quantity-input", "placeholder" => "Enter quantity...","id"=>"quantity"]) }}</div></div>';
	        document.getElementById(divName).appendChild(newdiv);
	        counter++;
	    }
	}*/

	$(".deleteRecipe").click(function(){
        var id = $(this).data("id");
        var token = $(this).data("token");
        $.ajax(
        {
            url: "/sales/orders/recipe/delete/"+id,
            type: 'DELETE',
            dataType: "JSON",
            data: {
                "id": id,
                "_method": 'DELETE',
                "_token": token,
            },
            success: function ()
            {
                location.reload();
            }
        });

        location.reload();
    });

	$('#recipe1').change(function(){
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size1');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + '(₦' + size.size_price + ')' + '</option>');
	        });
	    });
	});

	$('#recipe2').change(function(){
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size2');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + '(₦' + size.size_price + ')' + '</option>');
	        });
	    });
	});

	$('#recipe3').change(function(){
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size3');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + '(₦' + size.size_price + ')' + '</option>');
	        });
	    });
	});

	$('#recipe4').change(function(){
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size4');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + '(₦' + size.size_price + ')' + '</option>');
	        });
	    });
	});

	$('#recipe5').change(function(){
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size5');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + '(₦' + size.size_price + ')' + '</option>');
	        });
	    });
	});

	$('#recipe6').change(function(){
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size6');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + '(₦' + size.size_price + ')' + '</option>');
	        });
	    });
	});

	$('#recipe7').change(function(){
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size7');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + '(₦' + size.size_price + ')' + '</option>');
	        });
	    });
	});	

	$('#recipe8').change(function(){
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size8');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + '(₦' + size.size_price + ')' + '</option>');
	        });
	    });
	});

	$('#recipe9').change(function(){
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size9');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + '(₦' + size.size_price + ')' + '</option>');
	        });
	    });
	});

	$('#recipe10').change(function(){
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size10');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + '(₦' + size.size_price + ')' + '</option>');
	        });
	    });
	});
</script>