@extends('backend.layouts.app')

@section('title', 'Store Categories')

@section('content')
<article class="content responsive-tables-page">
    <div class="title-block">
        <span class="pull-left">
        	<h1 class="title">Store Categories</h1>
	        <p class="title-description"> List of all the store categories<small>(eg: coldroom, wines, easy market, easy cook, etc)</small> </p>
	    </span>
        <a href="javascript:;" class="pull-right" data-toggle="modal" data-target="#add-category-modal">
    		<button type="button" class="btn btn-info btn-sm">
    			<i class="fa fa-plus icon"></i> Create Store Category
    		</button>
    	</a>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12">
	            <div class="card">
	                <div class="card-block">
	                    <section class="example">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-bordered table-hover" data-toggle="dataTable" id="deleteForm">
	                                <thead class="flip-header">
	                                    <tr>
	                                        <th>Label</th>
	                                        <th>Description</th>
	                                        <th>Navigation</th>
	                                        <th>Status</th>
	                                        <th>Image</th>
	                                        <th class="text-center">Actions</th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                	@foreach($categories as $cat)
	                                    <tr class="odd gradeX">
	                                        <td>{{ $cat->label }}</td>
	                                        <td>{{ $cat->description }}</td>
	                                        <td>
	                                        	@if($cat->navigation == 0)
		                                        	No
		                                        @else
		                                        	Yes
		                                        @endif
	                                        </td>
	                                        <td>
	                                        	@if($cat->status == 0)
		                                        	Disabled
		                                        @else
		                                        	Enabled
		                                        @endif
	                                        </td>
	                                        <td>
	                                        	<a href="#" class="add-image" data-toggle="modal" data-target="#modal-media">
						                            <div class="image-container new">
						                                <div class="image"> <i class="fa fa-plus"></i> </div>
						                            </div>
						                        </a>
	                                        </td>
	                                        <td class="text-center">
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-right">
	                                        		<a class="edit" href="{{ action('CategoryController@edit', $cat->slug) }}">
	                                                	<i class="fa fa-pencil"></i>
	                                                </a>
	                                        	</div>
	                                        	<div class="col-xs-6 col-sm-6 col-md-6 text-left">
	                                        		{{ Form::model($cat, ['method' => 'delete', 'action' => ['CategoryController@destroy', $cat->slug], 'class' =>'form-inline form-delete']) }}
									                    {{ Form::hidden('id', $cat->id) }}								                    
									                    <a class="remove" href="javacript:void(0);" name="delete_modal">
		                                                	<i class="fa fa-trash-o "></i>
		                                                </a>
									                {{ Form::close() }} 
	                                        	</div>                                       
	                                        </td>
	                                    </tr>
	                                    @endforeach
	                                </tbody>
	                            </table>
	                        </div>
	                    </section>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
<!-- Add store-category modal -->
<div class="modal fade" id="add-category-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add a new store category</h4> </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'charmbow/store/categories', 'class' => 'form-horizontal' ]) }}
                	{{ csrf_field() }}
	                <div class="form-group">
	                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
	                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'What name would you like your customers to see?']) }}
	                </div>
	                <div class="form-group">
					    {{ Form::label('navigation', 'Navigation:') }}
					    {{ Form::checkbox('navigation', 1, old('navigation')) }}
					</div>
	                <div class="form-group">
	                	{{ Form::label('status', 'Status:') }}
					    {{ Form::checkbox('status', 1, old('status')) }}
	                </div>
	                <div class="form-group">
	                	{{ Form::label('description', 'Description:', ['class' => 'control-label']) }}
	                	{{ Form::textarea('description', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly tell your customer\'s more details about this category...']) }}
	                </div>
	                <div class="modal-footer">
		            	<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
		            	<button type="submit" class="btn btn-primary btn-sm">Add Store Category</button>
		            </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
	@include('includes.confirm-delete')
	@include('includes.add-image-modal')
@stop