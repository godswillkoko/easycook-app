@extends('backend.layouts.app')
@section('title') {{ $category->label }} @stop

@section('content')
<article class="content item-editor-page">
    <div class="title-block">
        <h1 class="title">Edit: {{ $category->label }}</h1>
    </div>
	<section class="section">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-block">
	                	{{ Form::model($category, ['method' => 'PATCH', 'action' => ['CategoryController@update', $category->slug]]) }}
	                		{{ csrf_field() }}
			                <div class="form-group">
			                	{{ Form::label('label', 'Label:', ['class' => 'control-label']) }}
			                	{{ Form::text('label', null, ['class' => 'form-control boxed', 'required', 'placeholder' => 'What name would you like your customers to see?']) }}
			                </div>
			                <div class="form-group">
							    {{ Form::label('navigation', 'Navigation:') }}
							    {{ Form::checkbox('navigation', 1, old('navigation')) }}
							</div>
			                <div class="form-group">
			                	{{ Form::label('status', 'Status:') }}
							    {{ Form::checkbox('status', 1, old('status')) }}
			                </div>
			                <div class="form-group">
			                	{{ Form::label('description', 'Description:', ['class' => 'control-label']) }}
			                	{{ Form::textarea('description', null, ['class' => 'form-control boxed', 'required', 'size' => '30x4', 'placeholder' => 'Briefly tell your customer\'s more details about this category...']) }}
			                </div>
			                <div class="form-group">
				            	<button type="submit" class="btn btn-primary">Save Details</button>
				            	<a href="{{ url('charmbow/store/categories') }}"><button type="button" class="btn btn-default">Cancel</button></a>
				            </div>
		                {{ Form::close() }}
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
</article>
@stop

@section('footer')
	<script type="text/javascript">
	  	$('select').select2();
	</script>
	@include('includes.country-state')
@stop