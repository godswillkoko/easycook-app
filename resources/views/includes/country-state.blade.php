<script>
	$('#country').change(function()
	{
	    $.get('/countries/' + this.value + '/states.json', function(states)
	    {
	        var $state = $('#state');

	        $state.find('option').remove().end();

	        $.each(states, function(index, state) {
	            $state.append('<option value="' + state.id + '">' + state.name + '</option>');
	        });
	    });
	});
</script>