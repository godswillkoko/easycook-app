<script>
	$('#recipe').change(function()
	{
	    $.get('/recipes/' + this.value + '/sizes.json', function(sizes)
	    {
	        var $size = $('#size');

	        $size.find('option').remove().end();

	        $.each(sizes, function(index, size) {
	            $size.append('<option value="' + size.id + '">' + size.size_name + ' (' + size.size_price + ')' + '</option>');
	        });
	    });
	});
</script>