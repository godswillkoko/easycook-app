<script>
	$('#customer').change(function()
	{
	    $.get('/customers/' + this.value + '/user_addresses.json', function(user_addresses)
	    {
	        var $user_address = $('#user_address');

	        $user_address.find('option').remove().end();

	        $.each(user_addresses, function(index, user_address) {
	            $user_address.append('<option value="' + user_address.id + '">' + user_address.address + ', ' + user_address.city + '</option>');
	        });
	    });
	});
</script>