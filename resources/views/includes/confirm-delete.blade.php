<!-- Confirm delete modal -->
<div class="modal fade" id="confirm">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
                <h4 class="modal-title"><i class="fa fa-warning"></i> Alert</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to do this?</p>
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-primary" id="delete-btn">Yes</button> 
            	<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button> 
            </div>
        </div>
    </div>
</div>
<script>
	$('#deleteForm').on('click', '.form-delete', function(e){
	    e.preventDefault();
	    var $form = $(this);
	    $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .on('click', '#delete-btn', function(){
            $form.submit();
        });
	});
</script>