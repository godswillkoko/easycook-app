<div class="modal fade" id="modal-media">
    <div class="modal-dialog modal-">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
            	</button>
                <h4 class="modal-title">Image Upload</h4>
            </div>
            <div class="modal-body modal-tab-container">
                <div class="tab-pane fade active in" id="upload" role="tabpanel">
                        <div class="upload-container">
                            <div id="dropzone">
                                <form action="/" method="POST" enctype="multipart/form-data" class="dropzone needsclick dz-clickable" id="demo-upload">
                                    <div class="dz-message-block">
                                        <div class="dz-message needsclick"> Drop file(s) here or click to upload. </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
                <button type="button" class="btn btn-primary">Insert Selected</button> 
            </div>
        </div>
    </div>
</div>