<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = array(
            array(
                'name' => 'default',
                'label' => 'Default',
                'minimum_amount_required' => 0,
                'theme_color' => 'blue'
            ),
            array(
                'name' => 'silver',
                'label' => 'Silver',
                'minimum_amount_required' => 50000,
                'theme_color' => 'silver'
            ),
            array(
                'name' => 'gold',
                'label' => 'Gold',
                'minimum_amount_required' => 100000,
                'theme_color' => 'gold'
            )
        );

        $users = array(
        	array(
        		'name' => 'Godswill Koko',
                'username' => 'g-factor',
        		'email' => 'godswillkoko@gmail.com',
                'password' => bcrypt('d1cap0'),
                'username' => 'g-factor',
                'gender' => 'male',
                'phone' => '08089763057',
                'group_id' => 3,
                'newsletter' => false,
                'verified' => true,
                'created_at' => Carbon::now()
        	),
        	array(
        		'name' => 'Jonathan Iyere',
                'username' => 'jo_iyere',
                'email' => 'jonathan.okojie@gmail.com',
                'password' => bcrypt('jv_decode'),
                'username' => 'sarah_oke',
                'gender' => 'male',
                'phone' => '08032930387',
                'group_id' => 1,
                'newsletter' => true,
                'verified' => true,
                'created_at' => Carbon::now()
        	)
        );

        $user_addresses = array(
            array(
                'user_id' => 1,
                'address' => 'No 1 Abike Close, Rd 14',
                'region_id' => 1,
                'state_id' => 2515,
                'country_id' => 156
            ),
            array(
                'user_id' => 2,
                'address' => 'No 7 Uwanna Street, Rd 12',
                'region_id' => 1,
                'state_id' => 2515,
                'country_id' => 156
            )
        );

        DB::table('groups')->insert($groups);
        DB::table('users')->insert($users);
        // DB::table('user_addresses')->insert($user_addresses);
    }
}
