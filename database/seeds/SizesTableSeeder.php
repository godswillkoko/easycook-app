<?php

use Illuminate\Database\Seeder;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sizes = array(
            array(
                'size' => 2,
                'slug' => '2-adults',
                'label' => '2 Adults',
                'description' => 'By our standard, this size is good enough for two adults...',
                'status' => true
            ),
            array(
                'size' => 3,
                'slug' => '3-adults',
                'label' => '3 Adults',
                'description' => 'By our standard, this size is good enough for three adults...',
                'status' => true
            ),
            array(
                'size' => 4,
                'slug' => '4-adults',
                'label' => '4 Adults',
                'description' => 'By our standard, this size is good enough for four adults...',
                'status' => true
            ),
        );

        DB::table('sizes')->insert($sizes);
    }
}
