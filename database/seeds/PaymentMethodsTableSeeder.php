<?php

use Illuminate\Database\Seeder;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment_methods = array(
            array(
                'name' => 'cash_on_delivery',
                'label' => 'Cash on Delivery',
                'status' => true
            ),
            array(
                'name' => 'bank_transfer',
                'label' => 'Bank Transfer',
                'status' => true
            ),
            array(
                'name' => 'cheque',
                'label' => 'Cheque',
                'status' => true
            ),
            array(
                'name' => 'bank_deposit',
                'label' => 'Bank Deposit',
                'status' => true
            )
        );

        DB::table('payment_methods')->insert($payment_methods);
    }
}
