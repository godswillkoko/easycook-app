<?php

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_role = array(
        	array(
        		'permission_id' => 1,
        		'role_id' => 1
        	),
        	array(
        		'permission_id' => 2,
        		'role_id' => 1
        	),
        	array(
        		'permission_id' => 3,
        		'role_id' => 1
        	),
        	array(
        		'permission_id' => 4,
        		'role_id' => 1
        	),
        	array(
        		'permission_id' => 1,
        		'role_id' => 4
        	),
        );

        $role_user = array(
            array('user_id' => 1, 'role_id' => 1),
            array('user_id' => 2, 'role_id' => 2),
        );

        DB::table('permission_role')->insert($permission_role);
        DB::table('role_user')->insert($role_user);
    }
}
