<?php

use Illuminate\Database\Seeder;

class MeasurementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $measurements = array(
            array(
                'slug' => 'per_litre',
                'label' => 'Per Litre',
                'status' => true
            ),
            array(
                'slug' => 'per_derica',
                'label' => 'Per Derica',
                'status' => true
            ),
            array(
                'slug' => 'cm',
                'label' => 'Cm',
                'status' => false
            ),
            array(
                'slug' => 'kg',
                'label' => 'Kg',
                'status' => true
            )
        );

        DB::table('measurements')->insert($measurements);
    }
}
