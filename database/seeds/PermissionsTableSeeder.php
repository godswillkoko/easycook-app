<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = array(
            array(
                'name' => 'canView',
                'label' => 'Can View'
            ),
            array(
                'name' => 'canCreate',
                'label' => 'Can Create'
            ),
            array(
                'name' => 'canEdit',
                'label' => 'Can Edit'
            ),
            array(
                'name' => 'canDelete',
                'label' => 'Can Delete'
            ),
        );

        DB::table('permissions')->insert($permissions);
    }
}
