<?php

use Illuminate\Database\Seeder;

class DeliveryMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $delivery_methods = array(
            array(
                'name' => 'pickup',
                'label' => 'Pickup',
                'delivery_fee' => 0,
                'description' => 'You will pick up the order yourself at (Easycook Office Address)',
                'status' => true
            ),
            array(
                'name' => 'delivery',
                'label' => 'Delivery',
                'delivery_fee' => 0,
                'description' => 'Your order will be delivered to te address you\'ve specified from the previous page.',
                'status' => true
            ),
            array(
                'name' => 'free_shipping',
                'label' => 'Free Shipping',
                'delivery_fee' => 0,
                'description' => 'None',
                'status' => true
            ),
            array(
                'name' => 'fedex',
                'label' => 'Fedex',
                'delivery_fee' => 2000,
                'description' => 'None',
                'status' => false
            )
        );

        DB::table('delivery_methods')->insert($delivery_methods);
    }
}
