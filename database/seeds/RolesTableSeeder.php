<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
        	array(
        		'name' => 'superUser',
        		'label' => 'Super User'
        	),
        	array(
        		'name' => 'admin',
        		'label' => 'Administrator'
        	),
        	array(
        		'name' => 'editor',
        		'label' => 'Editor'
        	),
        	array(
        		'name' => 'user',
        		'label' => 'User'
        	),
        );

        DB::table('roles')->insert($roles);
    }
}
