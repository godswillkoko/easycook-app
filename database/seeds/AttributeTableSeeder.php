<?php

use Illuminate\Database\Seeder;

class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = array(            
            array(
                'slug' => 'fresh',
                'label' => 'Fresh',
                'description' => 'This item is fresh. Add more description here if necessary...',
                'status' => true
            ),
            array(
                'slug' => 'chopped',
                'label' => 'Chopped',
                'description' => 'This item is chopped. Add more description here if necessary...',
                'status' => true
            ),
            array(
                'slug' => 'blended',
                'label' => 'Blended',
                'description' => 'This item is blended. Add more description here if necessary...',
                'status' => true
            )
        );

        $categories = array( 
            array(
                'slug' => 'local',
                'label' => 'Local',
                'description' => 'Local description here...',
                'status' => true
            ), 
            array(
                'slug' => 'foreign',
                'label' => 'Foreign',
                'description' => 'Foreign description here...',
                'status' => true
            ),         
            array(
                'slug' => 'igbo',
                'label' => 'Igbo',
                'description' => 'Igbo description here...',
                'status' => true
            ),
            array(
                'slug' => 'yoruba',
                'label' => 'Yoruba',
                'description' => 'Yoruba description here...',
                'status' => true
            ),
            array(
                'slug' => 'hausa',
                'label' => 'Hausa',
                'description' => 'Hausa description here...',
                'status' => true
            ),
            array(
                'slug' => 'efik',
                'label' => 'Efik',
                'description' => 'Efik(Calabar) description here...',
                'status' => true
            )
        );

        DB::table('attributes')->insert($attributes);
        DB::table('categories')->insert($categories);
    }
}
