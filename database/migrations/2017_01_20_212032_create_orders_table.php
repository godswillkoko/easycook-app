<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @author G-Factor
     */
    public function up()
    {

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('invoice_no')->unique();
            $table->integer('user_address_id')->unsigned(); //Address that the order will be delivered to
            $table->text('comments')->nullable(); //User's order comment/additional info
            $table->date('date_scheduled'); //The date the user wants the order delivered
            $table->string('delivery_status'); //Option = ['delivered', 'pending', 'not delivered']
            $table->double('total')->nullable();
            $table->text('note')->nullable(); //Admin comment
            $table->integer('payment_method_id')->unsigned();
            $table->integer('delivery_method_id')->unsigned();
            $table->integer('delivery_time_slot_id')->unsigned();
            $table->boolean('payment_status')->default(false); // 1 == 'paid', 0 = 'not paid'
            $table->string('order_status'); //Option = ['processed', 'pending' 'hold', 'canceled', 'refund', 'closed']
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('orders', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('user_address_id')->references('id')->on('user_addresses')
                ->onDelete('cascade');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
            $table->foreign('delivery_method_id')->references('id')->on('delivery_methods')->onDelete('cascade');
            $table->foreign('delivery_time_slot_id')->references('id')->on('delivery_time_slots')->onDelete('cascade');
        });

        Schema::table('payments', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
        });

        Schema::table('deliveries', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('delivery_method_id')->references('id')->on('delivery_methods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @author G-Factor
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('deliveries');
        Schema::dropIfExists('payments');
        Schema::dropIfExists('orders');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
