<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @author G-Factor
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('recipe_id')->unsigned();
            $table->integer('size_id')->unsigned();
            $table->integer('quantity');
            $table->boolean('status')->default(false);
            $table->timestamps();
        });

        Schema::table('carts', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
            $table->foreign('size_id')->references('id')->on('sizes')->onDelete('cascade');
        });

        Schema::create('cart_extra', function (Blueprint $table) {
            $table->integer('cart_id')->unsigned();
            $table->integer('extra_id')->unsigned();
            // $table->integer('recipe_id')->unsigned();

            $table->primary(['cart_id', 'extra_id']);
        });

        Schema::table('cart_extra', function(Blueprint $table) {
            $table->foreign('cart_id')->references('id')->on('carts')->onDelete('cascade');
            $table->foreign('extra_id')->references('id')->on('extras')->onDelete('cascade');
            // $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @author G-Factor
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('cart_extra');
        Schema::dropIfExists('carts');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
