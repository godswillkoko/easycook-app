<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaterecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @author G-Factor
     */
    public function up()
    {
        //Eg: Banga soap, Jollof rice, yam pourage etc
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->integer('category_id')->unsigned();
            $table->string('code');
            $table->string('stock_status'); //In Stock, Pre-Order, Out Of Stock, 2-3 Days
            $table->integer('quantity');
            $table->double('cost');
            $table->double('old_price')->default(0);
            $table->double('price')->default(0);
            $table->integer('delivery_fee')->default(0);
            $table->integer('minimum_order')->default(1);
            $table->integer('weight')->default(0);
            $table->text('description');
            $table->text('tips')->nullable();
            $table->string('chef')->nullable();
            $table->string('image_url')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });

        // Stuffs that the user can order more of
        Schema::create('extras', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('recipe_id')->unsigned();
            $table->string('extra_name');
            $table->string('extra_display_name');
            $table->double('extra_price');
            $table->timestamps();
            $table->softDeletes();
        });

        // Extra sizes that the user can order more of
        Schema::create('sizes', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('recipe_id')->unsigned();
            $table->integer('size_number');
            $table->string('size_name');
            $table->double('size_price');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ingredient_recipe', function (Blueprint $table) {
            $table->integer('recipe_id')->unsigned();
            $table->integer('ingredient_id')->unsigned();

            $table->primary(['ingredient_id', 'recipe_id']);
        });

        Schema::table('ingredient_recipe', function (Blueprint $table) {
            $table->foreign('ingredient_id')->references('id')->on('ingredients')->onDelete('cascade');
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
        });

        Schema::create('attribute_recipe', function(Blueprint $table) {
            $table->integer('attribute_id')->unsigned();
            $table->integer('recipe_id')->unsigned();

            $table->primary(['attribute_id', 'recipe_id']);
        });

        Schema::table('attribute_recipe', function(Blueprint $table) {
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
        });

        Schema::table('recipes', function(Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
        Schema::table('sizes', function(Blueprint $table) {
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
        });
        Schema::table('extras', function(Blueprint $table) {
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @author G-Factor
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('sizes');
        Schema::dropIfExists('extras');
        Schema::dropIfExists('ingredient_recipe');
        Schema::dropIfExists('attribute_recipe');
        Schema::dropIfExists('recipes');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
