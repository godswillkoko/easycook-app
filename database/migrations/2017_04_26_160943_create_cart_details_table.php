<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @author G-Factor
     */
    public function up()
    {
        Schema::create('cart_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->date('date_scheduled')->nullable();
            $table->integer('user_address_id')->unsigned()->nullable();
            $table->integer('payment_method_id')->unsigned()->nullable();
            $table->integer('delivery_method_id')->unsigned()->nullable();
            $table->integer('delivery_time_slot_id')->unsigned()->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('cart_details', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('user_address_id')->references('id')->on('user_addresses')
                ->onDelete('cascade');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
            $table->foreign('delivery_method_id')->references('id')->on('delivery_methods')->onDelete('cascade');
            $table->foreign('delivery_time_slot_id')->references('id')->on('delivery_time_slots')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @author G-Factor
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('cart_details');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
