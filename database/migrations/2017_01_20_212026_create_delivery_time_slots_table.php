<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryTimeSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_time_slots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('period'); // Morning, Afternoon, Evening
            $table->time('from'); // Eg: 08:00
            $table->time('to'); // Eg: 09:30
            $table->string('day'); // Eg: Monday, Tuesday, etc
            $table->boolean('status'); // Full / Available
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_time_slots');
    }
}
