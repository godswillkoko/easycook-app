<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @author G-Factor
     */
    public function up()
    {
        //Eg: tyme, salt, egusi, curry, pepper, oinion, etc
        Schema::create('ingredients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('code');
            $table->string('color')->nullable();
            $table->string('manufacturer')->nullable();
            $table->string('stock_status'); //In Stock, Pre-Order, Out Of Stock, 2-3 Days
            $table->integer('quantity');
            $table->double('cost');
            $table->double('old_price')->nullable();
            $table->double('price');
            $table->integer('sales_unit')->nullable();
            $table->integer('sales_unit_measurement_id')->unsigned()->nullable();
            $table->integer('vat')->nullable();
            $table->integer('delivery_fee')->nullable();
            $table->integer('minimum_order')->default(1);
            $table->integer('weight')->nullable();
            $table->integer('height')->nullable();
            $table->integer('width')->nullable();
            $table->boolean('require_shipping')->default(true);
            $table->text('description');
            $table->string('image_url')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('attribute_ingredient', function(Blueprint $table) {
            $table->integer('attribute_id')->unsigned();
            $table->integer('ingredient_id')->unsigned();

            $table->primary(['attribute_id', 'ingredient_id']);
        });

        Schema::table('attribute_ingredient', function(Blueprint $table) {
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->foreign('ingredient_id')->references('id')->on('ingredients')->onDelete('cascade');
        });

        Schema::table('ingredients', function(Blueprint $table) {
            $table->foreign('sales_unit_measurement_id')->references('id')->on('measurements')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @author G-Factor
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('attribute_ingredient');
        Schema::dropIfExists('ingredients');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
