<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Market::class, function (Faker\Generator $faker) {


	$countryIds = Country::lists('id');
	$stateIds = State::lists('id');

	foreach (range(1, 20) as $index) {
		
		DB::table('markets')->insert([
			'name' => $name = $faker->word,
			'slug' => str_slug(strtolower($name)),
			'location' => $faker->word(5),
			'country_id' => $faker->randomElement($countryIds),
			'state_id' => $faker->randomElement($stateIds),
			'description' => $faker->paragraph(1)
		]);
	}
});
