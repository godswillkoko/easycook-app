<?php

use App\UserAddress;
use App\Country;
use App\State;
use App\Size;

Route::group(['middleware' => 'admin', 'auth'], function () {

	Route::get('/', 'DashboardController@index');

    Route::resource('/users', 'UserController');
    Route::resource('/user/groups', 'GroupController');
    Route::resource('/user/roles', 'RoleController');
    Route::resource('/user/permissions', 'PermissionController');
    Route::resource('/pages/articles', 'ArticleController');
    Route::resource('/pages/howitworks', 'HowItWorksController');
    
	Route::resource('/store/categories', 'CategoryController');
    Route::resource('/store/attributes', 'AttributeController');
	Route::resource('/store/ingredients', 'IngredientController');
    Route::get('/store/ingredients/{slug}/images', [
        'uses' => 'IngredientController@imag e',
        'as'   => 'charmbow.store.ingredient.images'
    ]);
    Route::post('/store/ingredients/{slug}/images', [
        'uses' => 'IngredientController@addImage',
        'as'   => 'charmbow.store.ingredient.add.images'
    ]);

    Route::resource('/store/recipes', 'RecipeController');
    Route::get('/store/recipes/{slug}/images', [
        'uses' => 'RecipeController@image',
        'as'   => 'charmbow.store.easycook.images'
    ]);
    Route::post('/store/recipes/{slug}/images', [
        'uses' => 'RecipeController@addImage',
        'as'   => 'charmbow.store.easycook.add.images'
    ]);
    Route::get('/store/recipes/image/{id}', 'RecipeController@destroyImage');
    Route::delete('/store/recipes/extra/delete/{id}', 'RecipeController@deleteExtra');
    Route::delete('/store/recipes/size/delete/{id}', 'RecipeController@deleteSize');

    Route::resource('/sales/orders', 'OrderController');
    Route::delete('/sales/orders/recipe/delete/{id}', 'OrderController@deleteRecipe');
    // Route::resource('/sales/returns', 'ReturnController');

    Route::resource('/settings/payment-methods', 'PaymentMethodController');
    Route::resource('/settings/delivery-methods', 'DeliveryMethodController');
    Route::resource('/settings/measurements', 'MeasurementController');
    Route::resource('/settings/sizes', 'SizeController');
    Route::resource('/settings/extras', 'ExtraController');
    Route::resource('/settings/delivery-timeslots', 'DeliveryTimeSlotController');
    Route::resource('/settings/countries', 'CountryController');
    Route::resource('/settings/regions', 'RegionController');
    Route::resource('/settings/states', 'StateController');
});

// Route::group(['prefix' => 'account', 'middleware' => 'auth'], function() {

// 	Route::get('/', 'HomeController@index');
// });

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/register/confirm/{token}', 'Auth\RegisterController@confirmEmail');

Route::get('/all-countries/data.json', function() {
    return Country::pluck('name', 'id');
});

Route::get('/enabled-countries/data.json', function() {
    return Country::whereStatus(true)->pluck('name', 'id');
});

Route::get('/all-states/data.json', function() {
    return State::get();
});

Route::get('/countries/{id}/states.json', function($id) {
    return State::whereCountryId($id)->get();
});

Route::get('/recipes/{id}/sizes.json', function($id) {
    return Size::whereRecipeId($id)->get();
});

Route::get('/enabled-states/data.json', function() {
    return State::whereStatus(true)->pluck('name', 'id');
});

Route::get('/customers/{id}/user_addresses.json', function($id) {
    return UserAddress::whereUserId($id)->get();
});

Route::get('/photos/{filename}', function ($filename)
{
    $path = storage_path() . '/app/photos/' . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});