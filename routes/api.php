<?php

use Illuminate\Http\Request;
use App\Country;
// use JWTAuth;
use App\Region;
use App\State;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'], function(){

	Route::get('/countries', function() {
	    return Country::whereStatus(true)->get();
	});
	Route::get('/countries/{id}/states', function($id) {
	    return State::whereCountryId($id)->whereStatus(true)->get();
	});
	Route::get('/states/{id}/regions', function($id) {
	    return Region::whereStateId($id)->whereStatus(true)->get();
	});

	Route::post('/register', 'Auth\RegisterController@register');
	Route::post('/login', 'Auth\LoginController@authenticate');
	Route::get('/articles', 'ClientArticleController@getArticles');
	Route::get('/home-articles', 'ClientArticleController@getHomeArticles');
	Route::get('/how-it-works', 'ClientArticleController@getHiw');
	Route::get('/categories', 'ClientRecipeController@getCategories');
	Route::get('/recipes/category/{cat}', 'ClientRecipeController@getRecipesByCategory');
	Route::get('/recipes', 'ClientRecipeController@getRecipes');
	Route::get('/search-recipes', 'ClientRecipeController@searchRecipes');
	Route::get('/recipes/{recipe}', 'ClientRecipeController@getRecipe');

	Route::get('/delivery-timeslots', 'ClientCartController@getTimeSlots');
	Route::get('/payment-methods', 'ClientCartController@getPaymentMethods');
	Route::get('/delivery-methods', 'ClientCartController@getDeliveryMethods');

	Route::group(['middleware' => 'jwt.auth'], function(){

		Route::put('/password', 'AuthController@changePassword');

		Route::get('/user', 'AuthController@getUser');
		Route::put('/user', 'AuthController@updateUser');

		Route::get('/user-address', 'AuthController@getAddresses');
		Route::post('/user-address', 'AuthController@createUserAddress');
		Route::put('/user-address', 'AuthController@updateUserAddress');
		Route::delete('/user-address/{id}', 'AuthController@deleteUserAddress');

		Route::post('/add-to-cart', 'ClientCartController@addToCart');
		Route::get('/user-cart/', 'ClientCartController@getCart');
		Route::get('/user-cart-recipes/', 'ClientCartController@getCartRecipes');
		Route::delete('/remove-from-cart/{code}', 'ClientCartController@removeFromCart');
		Route::post('/cart-details', 'ClientCartController@cartDetails');
		Route::get('/cart-details', 'ClientCartController@getCartDetails');
		Route::post('/place-order', 'ClientCartController@placeOrder');
		// Route::post('/pay', 'ClientCartController@redirectToGateway')->name('pay');

		// Route::get('/user', function (Request $request) {
		// 	try {
		//         if (! $user = JWTAuth::parseToken()->authenticate()) {
		//             return response()->json(['user_not_found'], 404);
		//         }
		//     } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
		//         return response()->json(['token_expired'], $e->getStatusCode());
		//     } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
		//         return response()->json(['token_invalid'], $e->getStatusCode());
		//     } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
		//         return response()->json(['token_absent'], $e->getStatusCode());
		//     }
		//     // Token is valid and we have found the user via the sub claim
		//     return response()->json(compact('user'));
		// });
		
	});
});
